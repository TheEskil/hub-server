<?php

use Faker\Factory as Faker;
use App\Models\Feed;
use Illuminate\Database\Seeder;

/**
 * Class FeedsTableSeeder
 */
class FeedsTableSeeder extends Seeder {

    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        foreach( range(1, 10) as $feedIndex )
        {
            $feed = new Feed();
            $feed->fill([
                'title' => 'Feed #' . $feedIndex,
                'uri'   => 'http://' . $faker->domainName . '/rss/'
            ]);
            $feed->save();
        }
    }
}