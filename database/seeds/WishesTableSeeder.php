<?php

use Faker\Factory as Faker;
use App\Models\Wish;
use Illuminate\Database\Seeder;

/**
 * Class WishesTableSeeder
 */
class WishesTableSeeder extends Seeder {

    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        foreach( range(1, 10) as $index )
        {
            $wish = new Wish();
            $wish->fill([
                'imdb_id'       => $faker->word,
                'themoviedb_id' => $faker->word,
                'title'         => $faker->sentence(2),
                'year'          => $faker->year
            ]);
            $wish->save();
        }
    }
}