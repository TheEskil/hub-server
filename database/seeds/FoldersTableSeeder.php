<?php

use App\Models\Folder;
use Illuminate\Database\Seeder;

/**
 * Class FoldersTableSeeder
 */
class FoldersTableSeeder extends Seeder {

    /**
     *
     */
    public function run()
    {
        $TestFolders = [
            '/home/vagrant/MPC-Disk1',
            '/home/vagrant/MPC-Disk2',
            '/home/vagrant/MPC-Disk3'
        ];

        foreach( $TestFolders as $TestFolder )
        {
            if( ! is_dir($TestFolder) )
            {
                @mkdir($TestFolder, 0700);
            }

            $folder = new Folder();
            $folder->fill([
                'name' => $TestFolder
            ]);
            $folder->save();
        }
    }
}