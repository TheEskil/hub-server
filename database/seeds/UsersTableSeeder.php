<?php

use Faker\Factory as Faker;
use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder {

    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        $user = User::create([
            'ip_address'    => '127.0.0.1',
            'first_name'    => 'Eskil',
            'last_name'     => 'Kvalnes',
            'email'         => 'eskil.kvalnes@gmail.com',
            'password'      => Hash::make('secret'),
            'confirmed_at'  => $faker->dateTime,
            'approved_at'   => $faker->dateTime
        ]);

        foreach( range(1, 10) as $index )
        {
            $user = User::create([
                'ip_address'    => $faker->ipv4,
                'first_name'    => $faker->firstName,
                'last_name'     => $faker->lastName,
                'email'         => $faker->email,
                'password'      => Hash::make($faker->word)
            ]);
        }
    }

}