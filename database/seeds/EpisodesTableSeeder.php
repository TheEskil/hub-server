<?php

use Faker\Factory as Faker;
use App\Models\Episode;
use App\Models\Series;
use Illuminate\Database\Seeder;

/**
 * Class EpisodesTableSeeder
 */
class EpisodesTableSeeder extends Seeder {

    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        $serieIds = Series::lists('id');
        foreach( range(1, 10) as $index )
        {
            Episode::create([
                'season_number'     => $faker->numberBetween(1, 10),
                'episode_number'    => $faker->numberBetween(1, 24),
                'airdate'           => $faker->dateTime,
                'remote_link'       => $faker->url,
                'title'             => $faker->sentence(3),
                'remote_image_uri'  => $faker->url,
                'local_image_uri'   => $faker->url,
                'series_id'         => $faker->randomElement($serieIds->toArray())
            ]);
        }
    }

}