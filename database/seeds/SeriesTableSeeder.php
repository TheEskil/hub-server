<?php

use Faker\Factory as Faker;
use App\Models\Series;
use Illuminate\Database\Seeder;

/**
 * Class SeriesTableSeeder
 */
class SeriesTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        $tvMazeIds = [
            124, 30, 955, 315, 4, 1821, 618, 182, 1855, 2118, 102, 65, 1216, 49, 68, 14, 1819, 1851, 565, 94, 201, 104, 133,
            1355, 32, 1366, 82, 11, 67, 10, 24, 74, 33, 1415, 175, 145, 3368, 106, 54, 2473, 1859, 670, 385, 336, 31, 1369,
            1370, 3023, 2174, 355, 1858, 80, 284, 379, 2705, 72, 170, 16, 2, 299, 3144, 2114, 152, 44, 1367, 150, 329, 335,
            143, 42, 1844, 804, 172, 1850, 19, 1747, 6, 157, 66, 69, 13, 751, 21, 340, 1804, 62, 2244, 2036, 3981, 73, 430,
            296, 248, 5, 130, 1, 29, 434, 1847, 1863, 370, 38,
        ];
        foreach ( $tvMazeIds as $tvMazeId ) {
            $series = new Series();
            $series->fill( [
                               'title'            => $faker->sentence( 3 ),
                               'tvmaze_id'        => $tvMazeId,
                               'remote_link'      => $faker->url,
                               'remote_image_uri' => $faker->url,
                               'local_image_uri'  => $faker->url,
                               'started_at'       => $faker->date,
                               'status'           => $faker->word,
                               'type'             => $faker->word,
                               'summary'          => $faker->sentence( 20 ),
                               'rating'           => $faker->randomFloat( 0, 10 ),
                               'votes'            => $faker->numberBetween( 0, 1000 ),
                               'runtime'          => $faker->numberBetween( 20, 60 ),
                               'network'          => $faker->company,
                               'airtime'          => $faker->numberBetween( 18, 23 ) . ':' . $faker->numberBetween( 1, 59 ),
                               'airday'           => $faker->dayOfWeek,
                               'timezone'         => $faker->timezone,
                           ] );
            $series->save();
        }
    }
}