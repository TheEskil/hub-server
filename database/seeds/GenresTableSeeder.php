<?php

use App\Models\Genre;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

/**
 * Class GenresTableSeeder
 */
class GenresTableSeeder extends Seeder {

    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        foreach( range(1, 10) as $index )
        {
            Genre::create([
                'name' => $faker->word
            ]);
        }
    }

}