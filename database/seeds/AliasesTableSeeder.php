<?php

use Faker\Factory as Faker;
use App\Models\Series;
use App\Models\Alias;
use Illuminate\Database\Seeder;

/**
 * Class SeriesAliasesTableSeeder
 */
class AliasesTableSeeder extends Seeder {

    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        $seriesIds = Series::lists('id');
        foreach( range(1, 10) as $index )
        {
            Alias::create([
                'title'     => $faker->sentence(3),
                'series_id' => $faker->randomElement($seriesIds->toArray())
            ]);
        }
    }

}