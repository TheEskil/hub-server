<?php

use Faker\Factory as Faker;
use App\Models\Logbook;
use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class LogbooksTableSeeder
 */
class LogbooksTableSeeder extends Seeder {

    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        $userIds = User::lists('id');
        foreach( range(1, 10) as $index )
        {
            Logbook::firstOrCreate([
                'ip_address' => $faker->ipv4,
                'user_id'    => $faker->randomElement($userIds->toArray()),
                'event'      => $faker->word,
                'type'       => $faker->randomElement(['Success', 'Failure', 'Error']),
                'message'    => $faker->sentence(5)
            ]);
        }
    }
}