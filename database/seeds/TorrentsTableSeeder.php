<?php

use Faker\Factory as Faker;
use App\Models\Feed;
use App\Models\Torrent;
use Illuminate\Database\Seeder;

/**
 * Class TorrentsTableSeeder
 */
class TorrentsTableSeeder extends Seeder {

    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        $feedIds = Feed::lists('id');
        foreach( range(1, 10) as $index )
        {
            Torrent::create([
                'title'        => $faker->sentence(3),
                'uri'          => $faker->url,
                'size'         => $faker->numberBetween(1, 5) . '.' . $faker->numberBetween(1, 99) . ' GB',
                'category'     => $faker->word,
                'published_at' => $faker->dateTime,
                'feed_id'      => $faker->randomElement($feedIds->toArray())
            ]);
        }
    }

}