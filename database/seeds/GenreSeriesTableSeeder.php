<?php

use App\Models\Genre;
use App\Models\Series;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

/**
 * Class GenreSeriesTableSeeder
 */
class GenreSeriesTableSeeder extends Seeder {

    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        $genreIds = Genre::lists('id');
        $seriesIds = Series::lists('id');
        foreach( range(1, 10) as $index )
        {
            DB::table('genre_series')->insert([
                'genre_id' => $faker->randomElement($genreIds->toArray()),
                'series_id' => $faker->randomElement($seriesIds->toArray())
            ]);
        }
    }

}