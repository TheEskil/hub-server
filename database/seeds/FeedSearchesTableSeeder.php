<?php

use Faker\Factory as Faker;
use App\Models\Feed;
use App\Models\FeedSearch;
use Illuminate\Database\Seeder;

/**
 * Class FeedSearchesTableSeeder
 */
class FeedSearchesTableSeeder extends Seeder {

    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        $feedIds = Feed::lists('id');
        foreach( range(1, 10) as $index )
        {
            FeedSearch::create([
                'title'   => $faker->sentence(3),
                'uri'     => $faker->url,
                'feed_id' => $faker->randomElement($feedIds->toArray())
            ]);
        }
    }

}