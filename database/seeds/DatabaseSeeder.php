<?php

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder {

    /**
     * @var array
     */
    protected $truncateTables = [
        'feeds',
        'feed_searches',
        'folders',
        'series',
        'episodes',
        'genres',
        'aliases',
        'users',
        'logbooks',
        'wishes',
        'torrents',
        'genre_series',
    ];

    /**
     * @var array
     */
    protected $seeders = [
        'Feeds',
        'FeedSearches',
        'Folders',
        'Series',
        'Episodes',
        'Genres',
        'Aliases',
        'Users',
        'Logbooks',
        'Wishes',
        'Torrents',
        'GenreSeries', // Genres <-> Series
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables();
        $this->runIndividualSeeds();
    }

    /**
     *
     */
    protected function truncateTables()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        foreach( $this->truncateTables AS $table )
        {
            DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     *
     */
    protected function runIndividualSeeds()
    {
        Eloquent::unguard();

        foreach( $this->seeders as $seeder )
        {
            $this->command->info('Seeding ' . $seeder);
            $this->call($seeder . 'TableSeeder');
        }
    }
}