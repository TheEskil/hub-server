<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEpisodesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'episodes', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->integer( 'tvmaze_id' );
			$table->integer( 'season_number' )->nullable();
			$table->integer( 'episode_number' )->nullable();
			$table->timestamp( 'airdate' )->nullable();
			$table->string( 'remote_link' )->nullable();
			$table->string( 'title' )->nullable();
			$table->string( 'remote_image_uri' )->nullable();
			$table->string( 'local_image_uri' )->nullable();
			$table->integer( 'series_id' )->unsigned();
			$table->foreign( 'series_id' )->references( 'id' )->on( 'series' )->onDelete( 'cascade' );
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'episodes' );
	}
}
