<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeedSearchesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'feed_searches', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'title' );
			$table->string( 'uri' );
			$table->integer( 'feed_id' )->unsigned();
			$table->foreign( 'feed_id' )->references( 'id' )->on( 'feeds' )->onDelete( 'cascade' );
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'feed_searches' );
	}
}
