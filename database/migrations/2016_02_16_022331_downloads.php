<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Downloads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'downloads', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->integer( 'torrent_id' )->unsigned()->nullable();
            $table->foreign( 'torrent_id' )->references( 'id' )->on( 'torrents' )->onDelete( 'cascade' );
            $table->integer( 'episode_id' )->unsigned()->nullable();
            $table->foreign( 'episode_id' )->references( 'id' )->on( 'episodes' )->onDelete( 'cascade' );
            $table->integer( 'wish_id' )->unsigned()->nullable();
            $table->foreign( 'wish_id' )->references( 'id' )->on( 'wishes' )->onDelete( 'cascade' );
            $table->string( 'hash' )->nullable();
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'downloads' );
    }
}
