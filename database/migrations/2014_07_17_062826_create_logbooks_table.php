<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogbooksTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'logbooks', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'ip_address' );
			$table->integer( 'user_id' )->unsigned();
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );
			$table->string( 'event' );
			$table->enum( 'type', [ 'Success', 'Failure', 'Error' ] )->default( 'Success' );
			$table->text( 'message' );
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'logbooks' );
	}
}
