<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGenreSeriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'genre_series', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->integer( 'genre_id' )->unsigned()->index();
			$table->foreign( 'genre_id' )->references( 'id' )->on( 'genres' )->onDelete( 'cascade' );
			$table->integer( 'series_id' )->unsigned()->index();
			$table->foreign( 'series_id' )->references( 'id' )->on( 'series' )->onDelete( 'cascade' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'genre_series' );
	}
}