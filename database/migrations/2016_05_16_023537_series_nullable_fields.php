<?php

use Illuminate\Database\Migrations\Migration;

class SeriesNullableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'series', function ( $table ) {
            $table->string( 'network' )->nullable()->change();
            $table->string( 'timezone' )->nullable()->change();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
