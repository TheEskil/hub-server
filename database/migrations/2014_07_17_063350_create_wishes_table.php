<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'wishes', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->string( 'imdb_id' );
            $table->string( 'themoviedb_id' );
            $table->string( 'original_title' )->nullable();
            $table->string( 'title' );
            $table->integer( 'year' );
            $table->timestamp( 'release' );
            $table->text( 'summary' )->nullable();
            $table->string( 'country' )->nullable();
            $table->string( 'remote_image_uri' )->nullable();
            $table->string( 'local_image_uri' )->nullable();
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'wishes' );
    }
}
