<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTorrentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'torrents', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'title' );
			$table->index('title');
			$table->string( 'uri' );
			$table->string( 'size' );
			$table->string( 'category' );
			$table->timestamp( 'published_at' );
			$table->integer( 'feed_id' )->unsigned();
			$table->foreign( 'feed_id' )->references( 'id' )->on( 'feeds' )->onDelete( 'cascade' );
			$table->timestamps();
			$table->softDeletes();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'torrents' );
	}
}
