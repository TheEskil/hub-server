<?php

use Illuminate\Database\Migrations\Migration;

class SeriesRuntimeNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'series', function ( $table ) {
            $table->integer( 'runtime' )->nullable()->change();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}