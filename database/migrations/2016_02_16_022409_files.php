<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Files extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'files', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->integer( 'episode_id' )->unsigned()->nullable();
            $table->foreign( 'episode_id' )->references( 'id' )->on( 'episodes' )->onDelete( 'cascade' );
            $table->integer( 'wish_id' )->unsigned()->nullable();
            $table->foreign( 'wish_id' )->references( 'id' )->on( 'wishes' )->onDelete( 'cascade' );
            $table->enum( 'type', [ 'Series', 'Daily', 'Movie', 'Uncategorized', 'Subtitle' ] )->default( 'Uncategorized' )->nullable();
            $table->string( 'path' );
            $table->string( 'file' );
            $table->integer( 'size' );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'files' );
    }
}
