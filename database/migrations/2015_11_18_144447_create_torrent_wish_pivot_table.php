<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTorrentWishPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('torrent_wish', function (Blueprint $table) {
            $table->integer('torrent_id')->unsigned()->index();
            $table->foreign('torrent_id')->references('id')->on('torrents')->onDelete('cascade');
            $table->integer('wish_id')->unsigned()->index();
            $table->foreign('wish_id')->references('id')->on('wishes')->onDelete('cascade');
            $table->primary(['torrent_id', 'wish_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('torrent_wish');
    }
}
