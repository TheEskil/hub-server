<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodeTorrentPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episode_torrent', function (Blueprint $table) {
            $table->integer('episode_id')->unsigned()->index();
            $table->foreign('episode_id')->references('id')->on('episodes')->onDelete('cascade');
            $table->integer('torrent_id')->unsigned()->index();
            $table->foreign('torrent_id')->references('id')->on('torrents')->onDelete('cascade');
            $table->primary(['episode_id', 'torrent_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('episode_torrent');
    }
}
