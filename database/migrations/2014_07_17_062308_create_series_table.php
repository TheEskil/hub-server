<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tvmaze_id');
            $table->string('title');
            $table->string('remote_link');
            $table->string('remote_image_uri')->nullable();
            $table->string('local_image_uri')->nullable();
            $table->timestamp('started_at')->nullable();
            $table->string('status')->nullable();
            $table->string('type')->nullable();
            $table->integer('runtime')->nullable();
            $table->text('summary')->nullable();
            $table->float('rating')->nullable();
            $table->integer('votes')->nullable();
            $table->string('network')->nullable();
            $table->string('airtime')->nullable();
            $table->string('airday')->nullable();
            $table->string('timezone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('series');
    }
}
