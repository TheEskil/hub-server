# Hub

## Setup

Clone both hub-server and hub-client

> git clone git@gitlab.com:TheEskil/hub-server.git ~/Code/hub-server
>
> git clone git@gitlab.com:TheEskil/hub-client.git ~/Code/hub-client

### Install Virtual Machine

Installing The Homestead Vagrant Box:

> vagrant box add laravel/homestead

Install Homestead:

> git clone https://github.com/laravel/homestead.git ~/Homestead
> 
> cd ~/Homestead
> 
> bash init.sh

Configure Homestead by adding this code to `~/Homestead/Homestead.yaml`:

```yaml
---
ip: "192.168.10.10"
memory: 2048
cpus: 1
provider: virtualbox

authorize: ~/.ssh/id_rsa.pub

keys:
    - ~/.ssh/id_rsa

folders:
    - map: ~/Code
      to: /home/vagrant/code

sites:
    - map: api.hub.hs
      to: /home/vagrant/code/hub-server/public
      php: "7.1"

    - map: hub.hs
      to: /home/vagrant/code/hub-client

databases:
    - hub
```

Then start up the virtual machine by typing

> vagrant up

Append your new virtual machine information to `/etc/hosts`

> echo "192.168.10.10 api.hub.hs hub.hs" >> /etc/hosts

### Virtual machine configuration

First off, you have to set the default PHP version to 7.1

> php71

Install PHP extension mcrypt

> sudo apt-get install php7.1-mcrypt

Install PECL extension rar

> sudo pecl channel-update pecl.php.net
> 
> sudo pecl install rar

Enable PECL extension rar for both PHP-FPM and PHP CLI

> sudo echo "extension=rar.so" > /etc/php/7.1/mods-available/rar.ini
> 
> sudo echo "extension=rar.so" > /etc/php/7.1/cli/conf.d/20-rar.ini

## Hub server configuration

Enter the code repository

> cd ~/code/hub-server

Install dependencies

> composer install

Make a copy of the environment variables file

> cp .env.example .env

Generate a new application key

> artisan key:generate

Enter the database specific environment variables

> sed -i "s/DB\_DATABASE=.*/DB\_DATABASE=hub/" .env
> 
> sed -i "s/DB\_USERNAME=.*/DB\_USERNAME=homestead/" .env

Create the database migration table

> artisan migrate:install

Migrate the database

> artisan migrate:refresh

Seed the database

> artisan db:seed

If the above fails, try to dump autoloaded files first

> composer dump-autoload

## Hub client configuration

Edit `baseUrl` to point to the virtual machine

> sed -i "s|baseUrl = '.*'|baseUrl = 'http://api.hub.hs'|" ~/code/hub-client/assets/js/configs/config.constants.js

# Done!

That's it. Everything should be up and running. Try to open [http://api.hub.hs/v1/series/](http://api.hub.hs/v1/series/) or [http://hub.hs/](http://hub.hs/) in your browser. 

If you get an error, check the nginx logs in `~/code/hub-server/storage/logs/` and `/var/log/nginx/`
