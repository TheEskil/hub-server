<?php

use App\ReleaseParser\ReleaseParser;

class ReleaseParserTest extends TestCase
{
    private $releaseParser;

    /**
     * ReleaseParserTest constructor.
     */
    public function __construct()
    {
        $this->releaseParser = new ReleaseParser();
    }

    public function testSeriesEpisodeTraditionalTypeWithGroupParse()
    {
        $episode = 'House.of.Cards.S04E03.720p.hdtv.x264-fleet';
        $parsed = $this->releaseParser->parse( $episode );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseSeries', $parsed );
        $this->assertEquals( 'Series', $parsed->type );
        $this->assertEquals( 'House of Cards', $parsed->title );

        $this->assertEquals( 1, $parsed->episodes->count() );
        $this->assertEquals( 4, $parsed->episodes->get( 0 )->season_number );
        $this->assertEquals( 3, $parsed->episodes->get( 0 )->episode_number );

        $this->assertEquals( 3, $parsed->quality->count(), 'Should be three (3) quality words' );
        $this->assertEquals( '720p', $parsed->quality->get( 0 )->label );
        $this->assertEquals( 'hdtv', $parsed->quality->get( 1 )->label );
        $this->assertEquals( 'x264', $parsed->quality->get( 2 )->label );

        $this->assertEquals( 'fleet', $parsed->group );

        $this->assertEquals( $episode, $parsed->getReleaseString() );
    }

    public function testSeriesEpisodeTraditionalTypeWithoutGroupParse()
    {
        $episode = 'House.of.Cards.S04E03.720p.hdtv.x264';
        $parsed = $this->releaseParser->parse( $episode );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseSeries', $parsed );
        $this->assertEquals( 'Series', $parsed->type );
        $this->assertEquals( 'House of Cards', $parsed->title );

        $this->assertEquals( 1, $parsed->episodes->count() );
        $this->assertEquals( 4, $parsed->episodes->get( 0 )->season_number );
        $this->assertEquals( 3, $parsed->episodes->get( 0 )->episode_number );

        $this->assertEquals( 3, $parsed->quality->count() );
        $this->assertEquals( '720p', $parsed->quality->get( 0 )->label );
        $this->assertEquals( 'hdtv', $parsed->quality->get( 1 )->label );
        $this->assertEquals( 'x264', $parsed->quality->get( 2 )->label );

        $this->assertNull( $parsed->group );

        $this->assertEquals( $episode, $parsed->getReleaseString() );
    }

    public function testSeriesEpisodeLazyTypeWithGroupParse()
    {
        $episode = 'Modern.Family.716.HDTV.x264-KILLERS';
        $parsed = $this->releaseParser->parse( $episode );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseSeries', $parsed );
        $this->assertEquals( 'Series', $parsed->type );
        $this->assertEquals( 'Modern Family', $parsed->title );

        $this->assertEquals( 1, $parsed->episodes->count() );
        $this->assertEquals( 7, $parsed->episodes->get( 0 )->season_number );
        $this->assertEquals( 16, $parsed->episodes->get( 0 )->episode_number );

        $this->assertEquals( 2, $parsed->quality->count() );
        $this->assertEquals( 'HDTV', $parsed->quality->get( 0 )->label );
        $this->assertEquals( 'x264', $parsed->quality->get( 1 )->label );

        $this->assertEquals( 'KILLERS', $parsed->group );

        $this->assertEquals( 'Modern.Family.S07E16.HDTV.x264-KILLERS', $parsed->getReleaseString() );
    }

    public function testSeriesEpisodeLazyTypeWithoutGroupParse()
    {
        $episode = 'Modern.Family.716.HDTV.x264';
        $parsed = $this->releaseParser->parse( $episode );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseSeries', $parsed );
        $this->assertEquals( 'Series', $parsed->type );
        $this->assertEquals( 'Modern Family', $parsed->title );

        $this->assertEquals( 1, $parsed->episodes->count() );
        $this->assertEquals( 7, $parsed->episodes->get( 0 )->season_number );
        $this->assertEquals( 16, $parsed->episodes->get( 0 )->episode_number );

        $this->assertEquals( 2, $parsed->quality->count() );
        $this->assertEquals( 'HDTV', $parsed->quality->get( 0 )->label );
        $this->assertEquals( 'x264', $parsed->quality->get( 1 )->label );

        $this->assertNull( $parsed->group );

        $this->assertEquals( 'Modern.Family.S07E16.HDTV.x264', $parsed->getReleaseString() );
    }

    public function testSeriesMultipleEpisodesWithGroupParse()
    {
        $episode = 'Modern.Family.S07E16-17.PROPER.720p.HDTV.x264-KILLERS';
        $parsed = $this->releaseParser->parse( $episode );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseSeries', $parsed );
        $this->assertEquals( 'Series', $parsed->type );
        $this->assertEquals( 'Modern Family', $parsed->title );

        $this->assertEquals( 2, $parsed->episodes->count() );
        $this->assertEquals( 7, $parsed->episodes->get( 0 )->season_number );
        $this->assertEquals( 16, $parsed->episodes->get( 0 )->episode_number );
        $this->assertEquals( 7, $parsed->episodes->get( 1 )->season_number );
        $this->assertEquals( 17, $parsed->episodes->get( 1 )->episode_number );

        $this->assertEquals( 4, $parsed->quality->count() );
        $this->assertEquals( 'PROPER', $parsed->quality->get( 0 )->label );
        $this->assertEquals( '720p', $parsed->quality->get( 1 )->label );
        $this->assertEquals( 'HDTV', $parsed->quality->get( 2 )->label );
        $this->assertEquals( 'x264', $parsed->quality->get( 3 )->label );

        $this->assertEquals( 'KILLERS', $parsed->group );

        $this->assertEquals( $episode, $parsed->getReleaseString() );
    }

    public function testSeriesMultipleEpisodesWithoutGroupParse()
    {
        $episode = 'Modern.Family.S07E16-17.PROPER.720p.HDTV.x264';
        $parsed = $this->releaseParser->parse( $episode );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseSeries', $parsed );
        $this->assertEquals( 'Series', $parsed->type );
        $this->assertEquals( 'Modern Family', $parsed->title );

        $this->assertEquals( 2, $parsed->episodes->count() );
        $this->assertEquals( 7, $parsed->episodes->get( 0 )->season_number );
        $this->assertEquals( 16, $parsed->episodes->get( 0 )->episode_number );
        $this->assertEquals( 7, $parsed->episodes->get( 1 )->season_number );
        $this->assertEquals( 17, $parsed->episodes->get( 1 )->episode_number );

        $this->assertEquals( 4, $parsed->quality->count() );
        $this->assertEquals( 'PROPER', $parsed->quality->get( 0 )->label );
        $this->assertEquals( '720p', $parsed->quality->get( 1 )->label );
        $this->assertEquals( 'HDTV', $parsed->quality->get( 2 )->label );
        $this->assertEquals( 'x264', $parsed->quality->get( 3 )->label );

        $this->assertNull( $parsed->group );

        $this->assertEquals( $episode, $parsed->getReleaseString() );
    }

    public function testDailyEpisodeWithGroupParse()
    {
        $episode = 'The.Tonight.Show.Starring.Jimmy.Fallon.2016.03.16.jennifer.garner.720p.hdtv.x264-crooks';
        $parsed = $this->releaseParser->parse( $episode );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseDaily', $parsed );
        $this->assertEquals( 'Daily', $parsed->type );
        $this->assertEquals( '2016', $parsed->year );
        $this->assertEquals( '03', $parsed->month );
        $this->assertEquals( '16', $parsed->day );
        $this->assertEquals( 'The Tonight Show Starring Jimmy Fallon', $parsed->title );

        $this->assertEquals( 5, $parsed->quality->count() );

        $this->assertEquals( '720p', $parsed->quality->get( 2 )->label );
        $this->assertEquals( 'hdtv', $parsed->quality->get( 3 )->label );
        $this->assertEquals( 'x264', $parsed->quality->get( 4 )->label );

        $this->assertEquals( 'crooks', $parsed->group );

        $this->assertEquals( $episode, $parsed->getReleaseString() );
    }

    public function testDailyEpisodeWithoutGroupParse()
    {
        $episode = 'The.Tonight.Show.Starring.Jimmy.Fallon.2016.03.16.jennifer.garner.720p.hdtv.x264';
        $parsed = $this->releaseParser->parse( $episode );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseDaily', $parsed );
        $this->assertEquals( 'Daily', $parsed->type );
        $this->assertEquals( '2016', $parsed->year );
        $this->assertEquals( '03', $parsed->month );
        $this->assertEquals( '16', $parsed->day );
        $this->assertEquals( 'The Tonight Show Starring Jimmy Fallon', $parsed->title );

        $this->assertEquals( 5, $parsed->quality->count() );

        $this->assertEquals( '720p', $parsed->quality->get( 2 )->label );
        $this->assertEquals( 'hdtv', $parsed->quality->get( 3 )->label );
        $this->assertEquals( 'x264', $parsed->quality->get( 4 )->label );

        $this->assertNull( $parsed->group );

        $this->assertEquals( $episode, $parsed->getReleaseString() );
    }

    public function testMovieCrouchingTigerHiddenDragonWithGroupParse()
    {
        $movie = 'Crouching.Tiger.Hidden.Dragon.Sword.of.Destiny.2016.720p.HDRip.X264.AC3-EVO';
        $parsed = $this->releaseParser->parse( $movie );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseMovie', $parsed );
        $this->assertEquals( '2016', $parsed->year );
        $this->assertEquals( 'Movie', $parsed->type );
        $this->assertEquals( 'Crouching Tiger Hidden Dragon Sword of Destiny', $parsed->title );

        $this->assertEquals( 4, $parsed->quality->count() );

        $this->assertEquals( '720p', $parsed->quality->get( 0 )->label );
        $this->assertEquals( 'HDRip', $parsed->quality->get( 1 )->label );
        $this->assertEquals( 'X264', $parsed->quality->get( 2 )->label );
        $this->assertEquals( 'AC3', $parsed->quality->get( 3 )->label );

        $this->assertEquals( 'EVO', $parsed->group );

        $this->assertEquals( $movie, $parsed->getReleaseString() );
    }

    public function testMovieCrouchingTigerHiddenDragonWithoutGroupParse()
    {
        $movie = 'Crouching.Tiger.Hidden.Dragon.Sword.of.Destiny.2016.720p.HDRip.X264.AC3';

        $releaseParser = new ReleaseParser();
        $parsed = $releaseParser->parse( $movie );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseMovie', $parsed );
        $this->assertEquals( 'Movie', $parsed->type );
        $this->assertEquals( '2016', $parsed->year );
        $this->assertEquals( 'Crouching Tiger Hidden Dragon Sword of Destiny', $parsed->title );

        $this->assertEquals( 4, $parsed->quality->count() );

        $this->assertEquals( '720p', $parsed->quality->get( 0 )->label );
        $this->assertEquals( 'HDRip', $parsed->quality->get( 1 )->label );
        $this->assertEquals( 'X264', $parsed->quality->get( 2 )->label );
        $this->assertEquals( 'AC3', $parsed->quality->get( 3 )->label );

        $this->assertNull( $parsed->group );

        $this->assertEquals( $movie, $parsed->getReleaseString() );
    }

    public function testMovieTheInvitationWithGroupParse()
    {
        $movie = 'The.Invitation.2015.German.720p.BluRay.x264-ENCOUNTERS';
        $parsed = $this->releaseParser->parse( $movie );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseMovie', $parsed );
        $this->assertEquals( '2015', $parsed->year );
        $this->assertEquals( 'Movie', $parsed->type );
        $this->assertEquals( 'The Invitation', $parsed->title );

        $this->assertEquals( 4, $parsed->quality->count() );

        $this->assertEquals( '720p', $parsed->quality->get( 1 )->label );
        $this->assertEquals( 'BluRay', $parsed->quality->get( 2 )->label );
        $this->assertEquals( 'x264', $parsed->quality->get( 3 )->label );

        $this->assertEquals( 'ENCOUNTERS', $parsed->group );

        $this->assertEquals( $movie, $parsed->getReleaseString() );
    }

    public function testMovieTheInvitationWithoutGroupParse()
    {
        $movie = 'The.Invitation.2015.German.720p.BluRay.x264';

        $releaseParser = new ReleaseParser();
        $parsed = $releaseParser->parse( $movie );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseMovie', $parsed );
        $this->assertEquals( 'Movie', $parsed->type );
        $this->assertEquals( '2015', $parsed->year );
        $this->assertEquals( 'The Invitation', $parsed->title );

        $this->assertEquals( 4, $parsed->quality->count() );

        $this->assertEquals( '720p', $parsed->quality->get( 1 )->label );
        $this->assertEquals( 'BluRay', $parsed->quality->get( 2 )->label );
        $this->assertEquals( 'x264', $parsed->quality->get( 3 )->label );

        $this->assertNull( $parsed->group );

        $this->assertEquals( $movie, $parsed->getReleaseString() );
    }

    public function testSeriesEpisodeX264WithoutGroup() {
        $episode = 'Game.Of.Thrones.S06E05.x264.WEBRip';

        $parsed = $this->releaseParser->parse( $episode );

        $this->assertInstanceOf( 'App\ReleaseParser\Models\ReleaseSeries', $parsed );
        $this->assertEquals( 'Series', $parsed->type );
        $this->assertEquals( 'Game Of Thrones', $parsed->title );

        $this->assertEquals( 1, $parsed->episodes->count() );
        $this->assertEquals( 6, $parsed->episodes->get( 0 )->season_number );
        $this->assertEquals( 5, $parsed->episodes->get( 0 )->episode_number );

        $this->assertEquals( 2, $parsed->quality->count(), 'Should be two (2) quality words' );
        $this->assertEquals( 'x264', $parsed->quality->get( 0 )->label );
        $this->assertEquals( 'WEBRip', $parsed->quality->get( 1 )->label );

        $this->assertNull( $parsed->group );

        $this->assertEquals( $episode, $parsed->getReleaseString() );
    }
}