Feature: Folders
  Ability to add, delete, update and retrieve one or all folders

  Scenario: Creating a new Folder
    Given I have the payload:
    """
    { "name": "/home/vagrant/MPC-Disk1" }
    """
    When I request "POST /api/v1/folders"
    Then I get a "201" response
    And the properties exist:
    """
    data
    """
    And scope into the "data" property
    And the properties exist:
    """
    created_record_id
    """
    And the "created_record_id" property is a integer equalling "1"

  Scenario: Failing to create a new Folder
    Given I have the payload:
    """

    """
    When I request "POST /api/v1/folders"
    Then I get a "422" response
    And the properties exist:
    """
    error
    """
    And scope into the "error" property
    And the properties exist:
    """
    message
    status_code
    """

  Scenario: Finding a specific folder
    When I request "GET /api/v1/folders/1"
    Then I get a "200" response
    And the properties exist:
    """
    id
    name
    """
    And the "id" property is an integer
    And the "name" property equals "/home/vagrant/MPC-Disk1"
    And the folder "/home/vagrant/MPC-Disk1" exists

  Scenario: Getting all folders
    When I request "GET /api/v1/folders"
    Then I get a "200" response
    And the properties exist:
    """
    space
    data
    """
    And the "data" property contains 1 items

  Scenario: Updating a folder
    Given I have the payload:
    """
    { "name": "/home/vagrant/MPC-DiskXXX" }
    """
    When I request "PATCH /api/v1/folders/1"
    Then I get a "200" response
    And scope into the "data" property
    And the properties exist:
    """
    updated_record
    """
    And scope into the "updated_record" property
    And the properties exist:
    """
    name
    """
    And the "name" property equals "/home/vagrant/MPC-DiskXXX"

  Scenario: Deleting a Folder
    When I request "DELETE /api/v1/folders/1"
    Then I get a "200" response
    And scope into the "data" property
    And the properties exist:
    """
    deleted_record
    """
    And delete the folder "/home/vagrant/MPC-Disk1" if its empty

  Scenario: Looking for a folder that does not exist
    When I request "GET /api/v1/folders/1"
    Then I get a "404" response
    And the properties exist:
    """
    error
    """
    And scope into the "error" property
    And the properties exist:
    """
    message
    status_code
    """