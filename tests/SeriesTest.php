<?php

class SeriesTest extends TestCase
{
    public function testPostSeriesWithValidData()
    {
    }

    public function testPostSeriesWithInvalidData()
    {
        $this->json( 'POST', '/v1/series' )
             ->seeJson( [
                            'tvmaze_id'   => [ 'The tvmaze id field is required.' ],
                            'status_code' => 400,
                        ] );
    }
}
