<?php

namespace App\Jobs;

use App\Contracts\LogbookRepository;
use Carbon\Carbon;
use RuntimeException;
use Transmission\Transmission;

/**
 * Class RemoveFinishedTorrentsJob
 * @package App\Jobs
 */
class RemoveFinishedTorrentsJob extends Job
{
    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @param \App\Contracts\LogbookRepository $logbookRepository
     * @param \Carbon\Carbon                   $carbon
     */
    public function handle( LogbookRepository $logbookRepository, Carbon $carbon )
    {
        $transmission = new Transmission( env( 'TRANSMISSION_HOST' ), env( 'TRANSMISSION_PORT' ) );
        $torrentsRemoved = 0;

        try {
            $transmission->getSession();

            $torrents = $transmission->all();

            foreach ( $torrents as $torrent ) {
                if ( $torrent->isFinished() ) {
                    $transmission->remove( $torrent );
                    $torrentsRemoved++;
                }
            }
        }
        catch ( RuntimeException $e ) {
            $logbookRepository->addError( 'Transmission/Finished', $e->getMessage() );
            echo $carbon->now() . ' [ERROR] RemoveFinishedTorrentsJob: ' . $e->getMessage() . "\n";
        }

        if ( $torrentsRemoved ) {
            $logbookRepository->addSuccess( 'Transmission/Finished', 'Removed ' . $torrentsRemoved . ' finished torrents' );
        }
    }
}