<?php

namespace App\Jobs;

use App\Support\FilesHelper;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class CompletedFilesJob
 * @package App\Jobs
 */
class CompletedFilesJob extends Job
{
    use DispatchesJobs;

    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @param \App\Support\FilesHelper $filesHelper
     */
    public function handle( FilesHelper $filesHelper )
    {
        $files = $filesHelper->getCompletedFiles( 'Completed' );

        foreach ( $files as $file ) {
            $job = ( new ProcessFileJob( $file ) );
            $this->dispatch( $job );
        }
    }
}
