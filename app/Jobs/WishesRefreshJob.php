<?php

namespace App\Jobs;

use App\Contracts\LogbookRepository;
use App\Contracts\TorrentRepository;
use App\Contracts\WishRepository;
use App\Models\Wish;
use App\ReleaseParser\ReleaseParser;
use Exception;

/**
 * Class WishesRefreshJob
 * @package App\Jobs
 */
class WishesRefreshJob extends Job
{
    /**
     * @var
     */
    private $wish;

    /**
     * WishesRefreshJob constructor.
     *
     * @param \App\Models\Wish $wish
     */
    public function __construct( Wish $wish )
    {
        $this->wish = $wish;
    }

    /**
     * Execute the job.
     *
     * @param \App\Contracts\WishRepository    $wishRepository
     * @param \App\Contracts\LogbookRepository $logbookRepository
     * @param \App\Contracts\TorrentRepository $torrentRepository
     */
    public function handle( WishRepository $wishRepository, LogbookRepository $logbookRepository, TorrentRepository $torrentRepository )
    {
        try {
            $wish = $wishRepository->updateRecord( $this->wish );

            if ( ! is_null( $wish ) ) {
                $logbookRepository->addSuccess( 'Wish/Refresh', 'Refreshed wish "' . $wish->title . ' (' . $wish->year . ')"' );

                // TODO: Clean up line 69-87
                $torrents = $torrentRepository->findLikeTitle( $wish->title . ' ' . $wish->year );
                foreach ( $torrents as $torrent ) {
                    $torrentRepository->pairTorrent( $torrent );
                }

                // TODO: Check if necessary
                // Re-get the Wish object to ensure that newly paired torrents are there.
                $wish = $wishRepository->findById( $wish->id );

                $parser = new ReleaseParser();
                $torrents = $wish->torrents->sortByDesc( function ( $torrent ) use ( $parser ) {
                    $parsed = $parser->parse( $torrent->title );

                    return is_null( $parsed ) ? 0 : $parsed->calculateQualityRank();
                } );

                foreach ( $torrents as $torrent ) {
                    $torrentRepository->downloadMatch( $torrent, $wish );
                }

                $wishRepository->downloadPoster( $wish );
            }
        }
        catch ( Exception $e ) {
            $this->release( 30 );
        }
    }
}
