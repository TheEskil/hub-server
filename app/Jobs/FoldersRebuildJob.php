<?php

namespace App\Jobs;

use App\Contracts\FolderRepository;
use App\Contracts\LogbookRepository;
use App\Contracts\SeriesRepository;
use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem as Flysystem;

/**
 * Class FoldersRebuildJob
 * @package App\Jobs
 */
class FoldersRebuildJob extends Job
{
    /**
     * FoldersRebuildJob constructor.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @param \App\Contracts\FolderRepository   $folderRepository
     * @param \App\Contracts\SeriesRepository   $seriesRepository
     * @param \App\Contracts\LogbookRepository  $logbookRepository
     * @param \Carbon\Carbon                    $carbon
     * @param \Illuminate\Filesystem\Filesystem $flysystem
     */
    public function handle( FolderRepository $folderRepository, SeriesRepository $seriesRepository, LogbookRepository $logbookRepository, Carbon $carbon, Flysystem $flysystem )
    {
        $folders = $folderRepository->getAll();
        $series = $seriesRepository->getAll();

        $numberOfFoldersCreated = 0;
        foreach ( $folders as $folder ) {
            foreach ( $series as $serie ) {
                if ( ! $flysystem->isDirectory( $folder->name . '/Media/TV/' . $serie->title ) ) {
                    $flysystem->makeDirectory( $folder->name . '/Media/TV/' . $serie->title );

                    if ( $flysystem->isDirectory( $folder->name . '/Media/TV/' . $serie->title ) ) {
                        $numberOfFoldersCreated++;

                        echo $carbon->now() . ' Created ' . $folder->name . '/Media/TV/' . $serie->title . "\n";
                    }
                }
            }
        }

        if ( $numberOfFoldersCreated ) {
            $logbookRepository->addSuccess( 'Series/Folders', 'Created ' . $numberOfFoldersCreated . ' missing folders' );
        }
    }
}