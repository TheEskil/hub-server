<?php

namespace App\Jobs;

use App\Contracts\FileRepository;
use App\Contracts\LogbookRepository;
use Carbon\Carbon;
use ErrorException;
use Illuminate\Filesystem\Filesystem as Flysystem;
use RarArchive;

/**
 * Class ProcessFileJob
 * @package App\Jobs
 */
class ProcessFileJob extends Job
{
    /**
     * @var
     */
    private $file;

    /**
     * Create a new job instance.
     *
     * @param $file
     */
    public function __construct( $file )
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @param \App\Contracts\LogbookRepository  $logbookRepository
     * @param \Carbon\Carbon                    $carbon
     * @param \Illuminate\Filesystem\Filesystem $flysystem
     * @param \App\Contracts\FileRepository     $fileRepository
     */
    public function handle( LogbookRepository $logbookRepository, Carbon $carbon, Flysystem $flysystem, FileRepository $fileRepository )
    {
        $dateStart = $carbon->now();
        echo $carbon->now() . ' Processing ' . basename( $this->file[ 'from' ] ) . ' to ' . $this->file[ 'to' ] . "\n";

        if ( ! is_file( $this->file[ 'to' ] ) ) {
            if ( $this->file[ 'action' ] == 'extract' ) {
                try {
                    $rarArchive = RarArchive::open( $this->file[ 'from' ] );

                    if ( ($rarEntries = $rarArchive->getEntries()) === false ) {
                        $logbookRepository->addFailure( 'File/Process', 'Failed to open rar archive ' . $this->file[ 'from' ] );
                    }
                    else {
                        foreach ( $rarArchive->getEntries() AS $rarFile ) {
                            if ( $rarFile->extract( false, $this->file[ 'to' ] ) ) {
                                $logbookRepository->addSuccess( 'File/Process', 'Extracted "' . $this->file[ 'from' ] . '" to "' . $this->file[ 'to' ] . '"' );

                                $parentDirectoryPath = dirname( $this->file[ 'from' ] );
                                $parentDirectoryName = basename( $parentDirectoryPath );

                                if ( $parentDirectoryName != 'Completed' ) {
                                    $flysystem->deleteDirectory( $parentDirectoryPath );

                                    if ( $flysystem->isDirectory( $parentDirectoryPath ) ) {
                                        $logbookRepository->addFailure( 'File/Process', 'Failed to delete "' . $parentDirectoryPath . '"' );
                                    }
                                    else {
                                        $logbookRepository->addSuccess( 'File/Process', 'Deleted "' . $parentDirectoryPath . '"' );
                                    }
                                }

                                $fileRepository->matchFile( trim( basename( $this->file[ 'to' ] ) ), trim( dirname( $this->file[ 'to' ] ) ) );
                            }
                            else {
                                $logbookRepository->addFailure( 'File/Process', 'Failed to extract ' . $this->file[ 'from' ] . ' to ' . $this->file[ 'to' ] );
                            }
                        }

                        $rarArchive->close();
                    }
                }
                catch ( ErrorException $e ) {
                    $logbookRepository->addError( 'File/Process', $e->getMessage() . ' when processing "' . $this->file[ 'from' ] . '"' );
                    echo $carbon->now() . ' [ERROR] ProcessFileJob: ' . $e->getMessage() . ' when processing "' . $this->file[ 'from' ] . '"' . "\n";

                    if ( strstr( $e->getMessage(), 'ERAR_BAD_DATA' ) ) {
                        $parentDirectoryPath = dirname( $this->file[ 'from' ] );
                        $parentDirectoryName = basename( $parentDirectoryPath );

                        if ( $parentDirectoryName != 'Completed' ) {
                            $flysystem->deleteDirectory( $parentDirectoryPath );

                            if ( $flysystem->isDirectory( $parentDirectoryPath ) ) {
                                $logbookRepository->addFailure( 'File/Process', 'Failed to delete "' . $parentDirectoryPath . '"' );
                            }
                            else {
                                $logbookRepository->addSuccess( 'File/Process', 'Deleted "' . $parentDirectoryPath . '"' );
                            }
                        }
                        else {
                            $flysystem->delete( $this->file[ 'from' ] );

                            if ( $flysystem->isFile( $this->file[ 'from' ] ) ) {
                                $logbookRepository->addFailure( 'File/Process', 'Failed to delete "' . $this->file[ 'from' ] . '"' );
                            }
                            else {
                                $logbookRepository->addSuccess( 'File/Process', 'Deleted "' . $this->file[ 'from' ] . '"' );
                            }
                        }
                    }
                }
            }
            else if ( $this->file[ 'action' ] == 'move' ) {
                $flysystem->move( $this->file[ 'from' ], $this->file[ 'to' ] );

                if ( $flysystem->isFile( $this->file[ 'to' ] ) ) {
                    $logbookRepository->addSuccess( 'File/Process', 'Moved "' . $this->file[ 'from' ] . '" to "' . $this->file[ 'to' ] . '"' );
                    $fileRepository->matchFile( trim( basename( $this->file[ 'to' ] ) ), trim( dirname( $this->file[ 'to' ] ) ) );

                    $parentDirectoryPath = dirname( $this->file[ 'from' ] );
                    $parentDirectoryName = basename( $parentDirectoryPath );
                    $parentDirectorySize = collect( $flysystem->allFiles( $parentDirectoryPath ) )->sum( function ( $file ) use ( $flysystem ) {
                        return $flysystem->size( $file );
                    } );

                    if ( $parentDirectoryName != 'Completed' && $parentDirectorySize <= 104857600 ) { // 100MB
                        $flysystem->deleteDirectory( $parentDirectoryPath );

                        if ( $flysystem->isDirectory( $parentDirectoryPath ) ) {
                            $logbookRepository->addFailure( 'File/Process', 'Failed to delete "' . $parentDirectoryPath . '"' );
                        }
                        else {
                            $logbookRepository->addSuccess( 'File/Process', 'Deleted "' . $parentDirectoryPath . '"' );
                        }
                    }
                }
                else {
                    $logbookRepository->addFailure( 'File/Process', 'Failed to rename ' . $this->file[ 'from' ] . ' to ' . $this->file[ 'to' ] );
                }
            }
        }
        else {
            echo $carbon->now() . ' ' . $this->file[ 'to' ] . ' already exists on the filesystem.' . "\n";
        }

        echo $carbon->now() . ' Operation took: ' . $carbon->now()->diffForHumans( $dateStart, true ) . "\n";
    }
}