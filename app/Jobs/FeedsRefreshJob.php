<?php

namespace App\Jobs;

use App\Contracts\FeedRepository;
use App\Contracts\LogbookRepository;
use App\Contracts\TorrentRepository;
use App\Events\TorrentWasAdded;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use SimplePie;

/**
 * Class FeedsRefreshJob
 * @package App\Jobs
 */
class FeedsRefreshJob extends Job
{
    /**
     * FeedsRefreshJob constructor.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @param \App\Contracts\FeedRepository    $feedRepository
     * @param \App\Contracts\TorrentRepository $torrentRepository
     * @param \App\Contracts\LogbookRepository $logbookRepository
     * @param \Carbon\Carbon                   $carbon
     */
    public function handle( FeedRepository $feedRepository, TorrentRepository $torrentRepository, LogbookRepository $logbookRepository, Carbon $carbon )
    {
        $feeds = $feedRepository->getAll();

        $torrentsAdded = 0;
        foreach ( $feeds as $feed ) {
            $pie = new SimplePie();
            $pie->enable_cache( false );
            $pie->set_feed_url( $feed->uri );
            $pie->force_feed( true );
            $pie->init();

            foreach ( $pie->get_items() as $item ) {
                $torrentObject = [
                    'title'        => $item->get_title(),
                    'uri'          => $item->get_link(),
                    'category'     => is_object( $item->get_category() ) ? $item->get_category()->get_label() : "",
                    'published_at' => $carbon->parse( $item->get_date() )->tz( env( 'TIMEZONE' ) ),
                    'feed_id'      => $feed->id,
                ];

                $validator = Validator::make( $torrentObject, [
                    'title'    => 'required',
                    'uri'      => 'required|url|unique:torrents,uri',
                    'category' => 'required',
                    'feed_id'  => 'required',
                ] );

                if ( ! $validator->fails() ) {
                    $torrent = $torrentRepository->addRecord( $torrentObject );
                    $torrentsAdded++;

                    $matches = $torrentRepository->pairTorrent( $torrent );
                    if ( $matches ) {
                        foreach ( $matches as $match ) {
                            $torrentRepository->downloadMatch( $torrent, $match );
                        }
                    }
                }
            }
        }

        $lastLogEntry = $logbookRepository->getLastEntry();
        if ( $lastLogEntry && ($lastLogEntry->event == 'Feeds/Refresh' && $lastLogEntry->type == 'Success') ) {
            preg_match( '/.*?(\d+).*?/', $lastLogEntry->message, $lastLogEntryMatch );

            $lastLogEntry->message = 'Added ' . ($lastLogEntryMatch[ 1 ] + $torrentsAdded) . ' new torrents';
            $lastLogEntry->save();
        }
        else {
            if ( $torrentsAdded ) {
                $logbookRepository->addSuccess( 'Feeds/Refresh', 'Added ' . $torrentsAdded . ' new torrents' );
            }
        }
    }
}