<?php

namespace App\Jobs;

use App\Contracts\LogbookRepository;
use App\Contracts\TorrentRepository;
use App\Models\Download;
use App\Models\Torrent;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class RedownloadFailedTorrentsJob
 * @package App\Jobs
 */
class RedownloadFailedTorrentsJob extends Job
{
    use DispatchesJobs;

    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @param \App\Contracts\TorrentRepository $torrentRepository
     * @param \App\Contracts\LogbookRepository $logbookRepository
     * @param \Carbon\Carbon                   $carbon
     */
    public function handle( TorrentRepository $torrentRepository, LogbookRepository $logbookRepository, Carbon $carbon )
    {
        $downloads = Download::where( 'hash', null )->get();

        if ( $downloads ) {
            foreach ( $downloads as $download ) {
                $torrent = Torrent::find( $download->torrent_id );

                if ( $torrent ) {
                    try {
                        $torrentRepository->download( $torrent, $download );
                    }
                    catch ( Exception $e ) {
                        $logbookRepository->addError( 'Torrent/Download', $e->getMessage() );

                        echo $carbon->now() . ' [ERROR] RedownloadFailedTorrentsJob:' . $e->getMessage() . "\n";
                    }
                }
            }
        }
    }
}