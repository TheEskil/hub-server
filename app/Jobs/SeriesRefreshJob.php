<?php

namespace App\Jobs;

use App\Contracts\FolderRepository;
use App\Contracts\LogbookRepository;
use App\Contracts\SeriesRepository;
use App\TVMaze\Exceptions\NoEpisodesFoundException;
use App\TVMaze\Exceptions\NoSeriesFoundException;
use Carbon\Carbon;

/**
 * Class SeriesRefreshJob
 * @package App\Jobs
 */
class SeriesRefreshJob extends Job
{
    /**
     * @var null
     */
    public $seriesId;

    /**
     * @var
     */
    private $tvMazeId;

    /**
     * SeriesRefreshJob constructor.
     *
     * @param $seriesId
     * @param $tvMazeId
     */
    public function __construct( $seriesId, $tvMazeId )
    {
        $this->seriesId = $seriesId;
        $this->tvMazeId = $tvMazeId;
    }

    /**
     * @param \App\Contracts\SeriesRepository  $seriesRepository
     * @param \App\Contracts\LogbookRepository $logbookRepository
     * @param \Carbon\Carbon                   $carbon
     * @param \App\Contracts\FolderRepository  $folderRepository
     */
    public function handle( SeriesRepository $seriesRepository, LogbookRepository $logbookRepository, Carbon $carbon, FolderRepository $folderRepository )
    {
        try {
            $series = $seriesRepository->updateRecord( $this->seriesId, $this->tvMazeId );

            if ( ! is_null( $series ) ) {
                $logbookRepository->addSuccess( 'Series/Refresh', 'Refreshed "' . $series->title . '"' );

                $folderRepository->createSubfolders( '/Media/TV/' . $series->title );
                $seriesRepository->downloadPoster( $series );
            }
        }
        catch ( NoEpisodesFoundException $e ) {
            $logbookRepository->addError( 'Series/Refresh', $e->getMessage() );

            echo $carbon->now() . ' [ERROR] SeriesRefreshJob:' . $e->getMessage() . "\n";
        }
        catch ( NoSeriesFoundException $e ) {
            $logbookRepository->addError( 'Series/Refresh', $e->getMessage() );

            echo $carbon->now() . ' [ERROR] SeriesRefreshJob:' . $e->getMessage() . "\n";
        }
    }
}
