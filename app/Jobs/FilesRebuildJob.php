<?php

namespace App\Jobs;

use App\Contracts\FileRepository;
use App\Contracts\FolderRepository;
use App\Models\File;
use Illuminate\Filesystem\Filesystem as Flysystem;

/**
 * Class FilesRebuildJob
 * @package App\Jobs
 */
class FilesRebuildJob extends Job
{
    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param \App\Contracts\FolderRepository   $folderRepository
     * @param \App\Contracts\FileRepository     $fileRepository
     * @param \Illuminate\Filesystem\Filesystem $flysystem
     */
    public function handle( FolderRepository $folderRepository, FileRepository $fileRepository, Flysystem $flysystem )
    {
        $folders = $folderRepository->getAll();

        $fileIds = [ ];
        foreach ( $folders as $folder ) {
            $files = $flysystem->allFiles( $folder->name . '/Media' );

            foreach ( $files as $splFileObj ) {
                $fileId = $fileRepository->matchFile( $splFileObj->getFileName(), dirname( $splFileObj->getPathName() ) );

                if ( ! is_null( $fileId ) ) {
                    $fileIds[] = $fileId;
                }
            }
        };

        File::whereNotIn( 'id', $fileIds )->delete();
    }
}