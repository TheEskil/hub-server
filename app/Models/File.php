<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class File
 * @package App\Models
 */
class File extends Model
{
    /**
     * @var string
     */
    protected $table = 'files';

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'episode_id',
        'wish_id',
        'type',
        'path',
        'file',
        'size',
    ];

    /**
     * Each file belongs to one episode
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function episode()
    {
        return $this->belongsTo( 'App\Models\Episode' );
    }

    /**
     * Each file belongs to one wish
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wish()
    {
        return $this->belongsTo( 'App\Models\Wish' );
    }
}
