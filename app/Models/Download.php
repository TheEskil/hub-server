<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Download
 * @package App\Models
 */
class Download extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'downloads';

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'torrent_id',
        'episode_id',
        'wish_id',
        'hash',
    ];

    /**
     * Each download belongs to one episode
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function episodes()
    {
        return $this->belongsTo( 'App\Models\Episode' );
    }

    /**
     * Each download belongs to one torrent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function torrents()
    {
        return $this->belongsTo( 'App\Models\Torrent' );
    }

    /**
     * Each download belongs to one wish
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wishes()
    {
        return $this->belongsTo( 'App\Models\Wish' );
    }
}
