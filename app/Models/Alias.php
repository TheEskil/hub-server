<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Alias
 * @package App\Models
 */
class Alias extends Eloquent
{
    /**
     * @var string
     */
    protected $table = 'aliases';

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    /**
     * @param       $title
     * @param array $columns
     *
     * @TODO Move to repository
     *
     * @return null
     */
    public static function findByTitle( $title, $columns = [ '*' ] )
    {
        if ( ! is_null( $alias = static::whereTitle( $title )->first( $columns ) ) ) {
            return $alias;
        }

        return null;
    }

    /**
     * Each series alias belongs to one series
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function series()
    {
        return $this->belongsTo( 'App\Models\Series' );
    }
}