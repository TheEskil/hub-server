<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Logbook
 * @package App\Models
 */
class Logbook extends Eloquent
{
    /**
     * @var string
     */
    protected $table = 'logbooks';

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'ip_address',
        'user_id',
        'event',
        'type',
        'message',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo( 'App\Models\User' );
    }
}