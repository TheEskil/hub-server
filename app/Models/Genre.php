<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Genre
 * @package App\Models
 */
class Genre extends Eloquent
{
    /**
     * @var string
     */
    protected $table = 'genres';

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @param       $name
     * @param array $columns
     *
     * @TODO Move to repository
     *
     * @return null
     */
    public static function findByName( $name, $columns = [ '*' ] )
    {
        if ( ! is_null( $genre = static::whereName( $name )->first( $columns ) ) ) {
            return $genre;
        }

        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function series()
    {
        return $this->belongsToMany( 'App\Models\Series' );
    }
}