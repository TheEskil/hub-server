<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Feed
 * @package App\Models
 */
class Feed extends Eloquent
{
    /**
     * @var string
     */
    protected $table = 'feeds';

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'uri',
    ];

    /**
     * Each feed has many FeedSearches
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function searches()
    {
        return $this->hasMany( 'App\Models\FeedSearch' );
    }

    /**
     * Each feed has many Torrents
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function torrents()
    {
        return $this->hasMany( 'App\Models\Torrent' );
    }
}