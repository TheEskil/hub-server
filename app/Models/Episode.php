<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Episode
 * @package App\Models
 */
class Episode extends Eloquent
{
    /**
     * @var string
     */
    protected $table = 'episodes';

    /**
     * @var array
     */
    protected $dates = [
        'airdate',
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'tvmaze_id',
        'season_number',
        'episode_number',
        'airdate',
        'remote_link',
        'title',
        'remote_image_uri',
        'local_image_uri',
    ];

    /**
     * Each episode has many torrents
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function torrents()
    {
        return $this->belongsToMany( 'App\Models\Torrent' );
    }

    /**
     * Each episode has many downloads
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function downloads()
    {
        return $this->hasMany( 'App\Models\Download' );
    }

    /**
     * Each episode has many files
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany( 'App\Models\File' );
    }

    /**
     * Each episode belongs to one Series
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function series()
    {
        return $this->belongsTo( 'App\Models\Series' );
    }
}