<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Wish
 * @package App\Models
 */
class Wish extends Eloquent
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'wishes';

    /**
     * @var array
     */
    protected $dates = [
        'release',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'imdb_id',
        'themoviedb_id',
        'original_title',
        'title',
        'year',
        'release',
        'summary',
        'country',
        'remote_image_uri',
        'local_image_uri',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function torrents()
    {
        return $this->belongsToMany( 'App\Models\Torrent' );
    }

    /**
     * Each wish has many downloads
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function downloads()
    {
        return $this->hasMany( 'App\Models\Download' );
    }

    /**
     * Each wish has many files
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany( 'App\Models\File' );
    }
}