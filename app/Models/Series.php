<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Series
 * @package App\Models
 */
class Series extends Eloquent
{
    /**
     * @var string
     */
    protected $table = 'series';

    /**
     * @var array
     */
    protected $dates = [
        'started_at',
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'tvmaze_id',
        'title',
        'remote_link',
        'remote_image_uri',
        'started_at',
        'status',
        'type',
        'runtime',
        'summary',
        'rating',
        'votes',
        'network',
        'airtime',
        'airday',
        'timezone',
    ];

    /**
     * @param       $title
     * @param array $columns
     *
     * @TODO Move to repository
     *
     * @return null
     */
    public static function findByTitle( $title, $columns = [ '*' ] )
    {
        if ( ! is_null( $series = static::whereTitle( $title )->first( $columns ) ) ) {
            return $series;
        }

        return null;
    }

    /**
     * Each series belongs to many episodes
     * 
     * @return mixed
     */
    public function episodes()
    {
        return $this->hasMany( 'App\Models\Episode' )->orderBy( 'airdate', 'desc' );
    }

    /**
     * Each series belongs to many genres
     *
     * @return mixed
     */
    public function genres()
    {
        return $this->belongsToMany( 'App\Models\Genre' );
    }

    /**
     * Each series has many titles
     *
     * @return mixed
     */
    public function aliases()
    {
        return $this->hasMany( 'App\Models\Alias' );
    }
}