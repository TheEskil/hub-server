<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Folder
 * @package App\Models
 */
class Folder extends Eloquent
{
    /**
     * @var array
     */
    public $folderStructure = [
        'Completed',
        'Downloads',
        'Media',
        'Media/TV',
        'Media/Movies',
        'Unsorted',
    ];

    /**
     * @var string
     */
    protected $table = 'folders';

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}