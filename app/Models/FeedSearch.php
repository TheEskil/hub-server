<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class FeedSearch
 * @package App\Models
 */
class FeedSearch extends Eloquent
{
    /**
     * @var string
     */
    protected $table = 'feed_searches';

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'uri',
    ];

    /**
     * Each feed search belongs to ONE feed
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feed()
    {
        return $this->belongsTo( 'App\Models\Feed' );
    }
}