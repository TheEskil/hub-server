<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Torrent
 * @package App\Models
 */
class Torrent extends Eloquent
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'torrents';

    /**
     * @var array
     */
    protected $dates = [
        'published_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'uri',
        'size',
        'category',
        'published_at',
        'feed_id' // feeds.id
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function episodes()
    {
        return $this->belongsToMany( 'App\Models\Torrent' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function wishes()
    {
        return $this->belongsToMany( 'App\Models\Torrent' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function downloads()
    {
        return $this->hasOne( 'App\Models\Download' );
    }

    /**
     * Each torrent belongs to one feed
     *
     * @return mixed
     */
    public function feed()
    {
        return $this->belongsTo( 'App\Models\Feed' );
    }
}