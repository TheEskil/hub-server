<?php namespace App\Repositories;

use App\Contracts\EpisodeRepository;
use App\Exceptions\NotFoundException;
use App\Models\Episode;
use Carbon\Carbon;

/**
 * Class EloquentEpisodeRepository
 * @package App\Repositories
 */
class EloquentEpisodeRepository implements EpisodeRepository
{
    /**
     * @var \Carbon\Carbon
     */
    private $carbon;

    /**
     * EloquentEpisodeRepository constructor.
     *
     * @param \Carbon\Carbon $carbon
     */
    public function __construct( Carbon $carbon )
    {
        $this->carbon = $carbon;
    }

    /**
     * @param int $numberOfDays
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     */
    public function getRecent( $numberOfDays = 5 )
    {
        $episodes = Episode::with( 'downloads', 'files', 'series', 'torrents' )
                           ->where( 'airdate', '>', $this->carbon->subDays( $numberOfDays )->toDateString() )
                           ->where( 'airdate', '<', $this->carbon->now() )
                           ->orderBy( 'airdate', 'desc' )
                           ->orderBy( 'series_id' )
                           ->get();

        if ( ! $episodes ) {
            throw new NotFoundException( 'No recent episodes.' );
        }

        $episodes->each( function ( $episode ) {
            $episode->airdateRelative = $episode->airdate->diffForHumans();
        } );

        return $episodes;
    }

    /**
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     */
    public function getUpcoming()
    {
        $episodes = Episode::with( 'downloads', 'files', 'series', 'torrents' )
                           ->where( 'airdate', '>', $this->carbon->now() )
                           ->groupBy( 'series_id' )
                           ->orderBy( 'airdate', 'asc' )
                           ->orderBy( 'series_id' )
                           ->get();

        if ( ! $episodes ) {
            throw new NotFoundException( 'No upcoming episodes.' );
        }

        $episodes->each( function ( $episode ) {
            $episode->airdateRelative = $episode->airdate->diffForHumans();
        } );

        return $episodes;
    }
}