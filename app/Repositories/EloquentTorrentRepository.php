<?php namespace App\Repositories;

use App\Contracts\FolderRepository;
use App\Contracts\LogbookRepository;
use App\Contracts\TorrentRepository;
use App\Exceptions\DownloadFailedException;
use App\Exceptions\NotFoundException;
use App\Models\Alias;
use App\Models\Download;
use App\Models\Episode;
use App\Models\Series;
use App\Models\Torrent;
use App\Models\Wish;
use App\ReleaseParser\ReleaseParser;
use Carbon\Carbon;
use ErrorException;
use Exception;
use Illuminate\Filesystem\Filesystem as Flysystem;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Storage;
use RuntimeException;
use Transmission\Transmission;

/**
 * Class EloquentTorrentRepository
 * @package App\Repositories
 */
class EloquentTorrentRepository implements TorrentRepository
{
    use DispatchesJobs;

    /**
     * @var \App\ReleaseParser\ReleaseParser
     */
    private $releaseParser;

    /**
     * @var \Carbon\Carbon
     */
    private $carbon;

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    private $flysystem;

    /**
     * @var \App\Contracts\LogbookRepository
     */
    private $logbookRepository;

    /**
     * @var \App\Contracts\FolderRepository
     */
    private $folderRepository;

    /**
     * EloquentTorrentRepository constructor.
     *
     * @param \App\ReleaseParser\ReleaseParser  $releaseParser
     * @param \Carbon\Carbon                    $carbon
     * @param \Illuminate\Filesystem\Filesystem $flysystem
     * @param \App\Contracts\LogbookRepository  $logbookRepository
     * @param \App\Contracts\FolderRepository   $folderRepository
     */
    public function __construct( ReleaseParser $releaseParser, Carbon $carbon, Flysystem $flysystem, LogbookRepository $logbookRepository, FolderRepository $folderRepository )
    {
        $this->releaseParser = $releaseParser;
        $this->carbon = $carbon;
        $this->flysystem = $flysystem;
        $this->logbookRepository = $logbookRepository;
        $this->folderRepository = $folderRepository;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        $torrents = Torrent::all();

        return $torrents;
    }

    /**
     * @param $title
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     */
    public function findByTitle( $title )
    {
        $torrent = Torrent::where( 'title', '=', $title )
                          ->get();

        if ( ! $torrent ) {
            throw new NotFoundException( 'Torrent does not exist.' );
        }

        return $torrent;
    }

    /**
     * @param $title
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     */
    public function findLikeTitle( $title )
    {
        $torrent = Torrent::where( 'title', 'LIKE', $title . '%' )
                          ->with( 'feed' )
                          ->with( 'downloads' )
                          ->orderBy( 'published_at', 'DESC' )
                          ->get();

        if ( ! $torrent ) {
            throw new NotFoundException( 'Found no torrents matching "' . $title . '"' );
        }

        return $torrent;
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     */
    public function findById( $id )
    {
        $torrent = Torrent::find( $id );

        if ( ! $torrent ) {
            throw new NotFoundException( 'Torrent does not exist.' );
        }

        return $torrent;
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     */
    public function findByFeedId( $id )
    {
        $torrents = Torrent::where( 'feed_id', '=', $id )
                           ->with( 'downloads' )
                           ->orderBy( 'published_at', 'DESC' )
                           ->paginate( 100 );

        if ( ! $torrents ) {
            throw new NotFoundException( 'No torrents founds' );
        }

        return $torrents->toArray();
    }

    /**
     * @param $input
     *
     * @return \App\Models\Torrent
     */
    public function addRecord( $input )
    {
        $torrent = new Torrent();
        $torrent->fill( $input );
        $torrent->save();

        return $torrent;
    }

    /**
     * @param \App\Models\Torrent $torrent
     *
     * @return \Illuminate\Support\Collection|null
     */
    public function pairTorrent( Torrent $torrent )
    {
        $matches = collect();

        if ( ! is_null( $parsed = $this->releaseParser->parse( $torrent->title ) ) ) {
            switch ( $parsed->type ) {
                case 'Series':
                case 'Daily':
                    if ( ! is_null( $series = Series::findByTitle( $parsed->title ) ) ) {
                        $series = $series;
                    }
                    else if ( ! is_null( $series = Alias::findByTitle( $parsed->title ) ) ) {
                        $series = $series->series()->first();
                    }

                    if ( $parsed->type == 'Series' && $series ) {
                        $episodes = $series->episodes()
                                           ->where( 'season_number', $parsed->episodes->min()->season_number )
                                           ->where( 'episode_number', '>=', $parsed->episodes->min()->episode_number )
                                           ->where( 'episode_number', '<=', $parsed->episodes->max()->episode_number )
                                           ->get();
                    }
                    else if ( $parsed->type == 'Daily' && $series ) {
                        try {
                            $convertedAirdate = $this->carbon->parse( $parsed->year . '-' . $parsed->month . '-' . $parsed->day . ' ' . $series->airtime, $series->timezone )->tz( env( 'TIMEZONE' ) );

                            $episodes = $series->episodes()
                                               ->whereYear( 'airdate', '=', $convertedAirdate->year )
                                               ->whereMonth( 'airdate', '=', $convertedAirdate->month )
                                               ->whereDay( 'airdate', '=', $convertedAirdate->day )
                                               ->get();
                        }
                        catch ( Exception $e ) {
                            var_dump( 'Torrent: ' . $torrent->title );
                            var_dump( 'Failed to parse date: ' . $parsed->year . '-' . $parsed->month . '-' . $parsed->day . ' ' . $series->airtime );
                        }
                    }

                    if ( isset($episodes) && $episodes ) {
                        foreach ( $episodes as $episode ) {
                            if ( ! $episode->torrents->contains( $torrent->id ) ) {
                                //var_dump( 'Paired ' . $torrent->title . ' to series "' . $episode->series->title . '"' );
                                $episode->torrents()->save( $torrent );
                                $matches->push( $episode );
                            }
                        }
                    }
                    break;
                case 'Movie':
                    $wishes = Wish::where( function ( $query ) use ( $parsed ) {
                        $query->orWhere( 'title', $parsed->title );
                        $query->orWhere( 'original_title', $parsed->title );
                    } )
                                  ->where( 'year', $parsed->year )
                                  ->get();

                    foreach ( $wishes as $wish ) {
                        if ( ! $wish->torrents->contains( $torrent->id ) ) {
                            //var_dump( 'Paired ' . $torrent->title . ' to wish ' . $wish->title . ' ' . $wish->year );
                            $wish->torrents()->save( $torrent );
                            $matches->push( $wish );
                        }
                    }
                    break;
            }

            return $matches;
        }

        return null;
    }

    /**
     * @param \App\Models\Torrent  $torrent
     * @param \App\Models\Download $download
     *
     * @return \App\Models\Torrent
     * @throws \App\Exceptions\DownloadFailedException
     * @throws \App\Exceptions\NotFoundException
     * @throws \Exception
     */
    function download( Torrent $torrent, Download $download )
    {
        try {
            $transmission = new Transmission( env( 'TRANSMISSION_HOST' ), env( 'TRANSMISSION_PORT' ) );

            $session = $transmission->getSession();
            $folder = $this->folderRepository->getActiveFolder();

            $session->setDownloadDir( $folder->name . '/Completed' );
            $session->setIncompleteDir( $folder->name . '/Downloads' );
            $session->save();

            if ( ! Storage::has( 'torrents/' . basename( $torrent->uri ) ) ) {
                Storage::put( 'torrents/' . basename( $torrent->uri ), file_get_contents( $torrent->uri ) );
            }

            $file = storage_path() . '/app/torrents/' . basename( $torrent->uri );

            if ( ! $addedTorrent = $transmission->add( $file ) ) {
                throw new DownloadFailedException( 'Failed to download torrent "' . $torrent->title . '"' );
            }

            $download->hash = $addedTorrent->getHash();
            $download->save();

            Storage::delete( 'torrents/' . basename( $torrent->uri ) );

            $this->logbookRepository->addSuccess( 'Torrent/Download', 'Downloaded torrent "' . $torrent->title . '"' );

            return $torrent;
        }
        catch ( RuntimeException $e ) {
            throw new Exception( $e->getMessage() );
        }
        catch ( ErrorException $e ) {
            if ( strpos( $e->getMessage(), 'HTTP/1.1 500 Internal Server Error' ) !== false ) {
                if ( $download ) {
                    $download->forceDelete();
                }

                $torrent->delete();
                $this->logbookRepository->addSuccess( 'Torrent/Delete', 'Deleted retired torrent "' . $torrent->title . '"' );

                throw new NotFoundException( '"' . $torrent->title . '" has been retired from the feed' );
            }

            throw new DownloadFailedException( $e->getMessage() );
        }
    }

    /**
     * @param \App\Models\Torrent $torrent
     * @param                     $match
     *
     * @return bool
     */
    function downloadMatch( Torrent $torrent, $match )
    {
        $download = new Download();
        $download->torrent_id = $torrent->id;

        if ( $match instanceof \App\Models\Episode ) {
            // var_dump( 'Episode: ' . $torrent->title . ' matched to "' . $match->series->title . '"" S' . $match->season_number . 'E' . $match->episode_number );

            $download->episode_id = $match->id;
            $match = Episode::find( $match->id );
        }
        else if ( $match instanceof \App\Models\Wish ) {
            // var_dump( 'Wish: ' . $torrent->title . ' matches "' . $match->title . ' ' . $match->year . '"' );

            $download->wish_id = $match->id;
            $match = Wish::find( $match->id );
        }

        if ( ! $match->downloads->contains( 'torrent_id', $torrent->id ) ) {
            $parsed = $this->releaseParser->parse( $torrent->title );

            $newTorrentQuality = is_null( $parsed ) ? 0 : $parsed->calculateQualityRank();
            if ( $newTorrentQuality >= env( 'MIN_QUALITY' ) && $newTorrentQuality <= env( 'MAX_QUALITY' ) ) {
                $highestFile = $match->files->sortByDesc( function ( $file ) {
                    $parsed = $this->releaseParser->parse( $file->file );

                    return is_null( $parsed ) ? 0 : $parsed->calculateQualityRank();
                } )->first();

                if ( $highestFile ) {
                    $highestFileParsed = $this->releaseParser->parse( $highestFile->file );
                    $highestFileQuality = is_null( $highestFileParsed ) ? 0 : $highestFileParsed->calculateQualityRank();
                    if ( $newTorrentQuality > $highestFileQuality ) {
                        $match->files->each( function ( $file ) {
                            if ( $this->flysystem->delete( $file->path . '/' . $file->file ) ) {
                                $file->delete();
                                $this->logbookRepository->addSuccess( 'File/Delete', 'Deleted "' . $file->path . '/' . $file->file . '"' );
                            }
                            else {
                                $this->logbookRepository->addFailure( 'File/Delete', 'Failed to delete "' . $file->path . '/' . $file->file . '"' );
                            }
                        } );
                    }
                    else {
                        return null;
                    }
                }

                $highestDownload = $match->downloads->sortByDesc( function ( $download ) {
                    $torrent = Torrent::find( $download->torrent_id );
                    $parsed = $this->releaseParser->parse( $torrent->title );

                    return is_null( $parsed ) ? 0 : $parsed->calculateQualityRank();
                } )->first();

                if ( $highestDownload ) {
                    $previouslyDownloadedTorrent = Torrent::find( $highestDownload->torrent_id );

                    $highestDownloadParsed = $this->releaseParser->parse( $previouslyDownloadedTorrent->title );
                    $highestDownloadQuality = is_null( $highestDownloadParsed ) ? 0 : $highestDownloadParsed->calculateQualityRank();
                    if ( $newTorrentQuality > $highestDownloadQuality ) {
                        $match->downloads->each( function ( $download ) use ( $highestDownloadParsed ) {
                            $torrent = Torrent::find( $download->torrent_id );

                            // TODO: Find active torrents and stop them
                            var_dump( 'I should have checked for "' . $torrent->title . ' (' . $download->hash . ')" and stopped the torrent.' );

                            $folders = $this->folderRepository->getAll();
                            $folders->each( function ( $folder ) use ( $highestDownloadParsed ) {
                                $completedDirectories = $this->flysystem->directories( $folder->name . '/Completed' );

                                foreach ( $completedDirectories as $completedDirectory ) {
                                    if ( ! is_null( $completedDirectoryParsed = $this->releaseParser->parse( basename( $completedDirectory ) ) ) ) {
                                        if ( $highestDownloadParsed == $completedDirectoryParsed ) {
                                            $this->flysystem->deleteDirectory( $completedDirectory );

                                            if ( ! $this->flysystem->isDirectory( $completedDirectory ) ) {
                                                $this->logbookRepository->addSuccess( 'Download/Delete', 'Deleted folder "' . $completedDirectory . '"' );
                                            }
                                            else {
                                                $this->logbookRepository->addFailure( 'Download/Delete', 'Failed to delete folder "' . $completedDirectory . '"' );
                                            }
                                        }
                                    }
                                }

                                $completedFiles = $this->flysystem->files( $folder->name . '/Completed' );

                                foreach ( $completedFiles as $completedFile ) {
                                    if ( ! is_null( $completedFileParsed = $this->releaseParser->parse( basename( $completedFile ) ) ) ) {
                                        if ( $highestDownloadParsed == $completedFileParsed ) {
                                            $this->flysystem->delete( $completedFile );

                                            if ( ! $this->flysystem->isFile( $completedFile ) ) {
                                                $this->logbookRepository->addSuccess( 'Download/Delete', 'Deleted file "' . $completedFile . '"' );
                                            }
                                            else {
                                                $this->logbookRepository->addFailure( 'Download/Delete', 'Failed to delete file "' . $completedFile . '"' );
                                            }
                                        }
                                    }
                                }
                            } );

                            $download->delete();

                            $this->logbookRepository->addSuccess( 'Download/Delete', 'Deleted download ' . $torrent->title );
                        } );
                    }
                    else {
                        return null;
                    }
                }

                $match->downloads()->save( $download );

                try {
                    $this->download( $torrent, $download );
                }
                catch ( Exception $e ) {
                    $this->logbookRepository->addError( 'Torrent/Download', $e->getMessage() );
                }

                return true;
            }
        }

        return false;
    }
}