<?php namespace App\Repositories;

use App\Contracts\FolderRepository;
use App\Contracts\LogbookRepository;
use App\Contracts\SeriesRepository;
use App\Contracts\TorrentRepository;
use App\Exceptions\NotFoundException;
use App\Models\Episode;
use App\Models\Genre;
use App\Models\Series;
use App\TVMaze\Exceptions\NoSeriesFoundException;
use App\TVMaze\Models\TVMazeSeries;
use App\TVMaze\TVMaze;
use Exception;
use Illuminate\Filesystem\Filesystem as Flysystem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class EloquentSeriesRepository
 * @package App\Repositories
 */
class EloquentSeriesRepository implements SeriesRepository
{
    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    private $flysystem;

    /**
     * @var \App\TVMaze\TVMaze
     */
    private $TVMaze;

    /**
     * @var \App\Contracts\FolderRepository
     */
    private $folderRepository;

    /**
     * @var \App\Contracts\LogbookRepository
     */
    private $logbookRepository;

    /**
     * @var \App\Contracts\TorrentRepository
     */
    private $torrentRepository;

    /**
     * EloquentSeriesRepository constructor.
     *
     * @param \Illuminate\Filesystem\Filesystem $flysystem
     * @param \App\TVMaze\TVMaze                $TVMaze
     * @param \App\Contracts\FolderRepository   $folderRepository
     * @param \App\Contracts\LogbookRepository  $logbookRepository
     * @param \App\Contracts\TorrentRepository  $torrentRepository
     */
    public function __construct( Flysystem $flysystem, TVMaze $TVMaze, FolderRepository $folderRepository, LogbookRepository $logbookRepository, TorrentRepository $torrentRepository )
    {
        $this->flysystem = $flysystem;
        $this->TVMaze = $TVMaze;
        $this->folderRepository = $folderRepository;
        $this->logbookRepository = $logbookRepository;
        $this->torrentRepository = $torrentRepository;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $series = Series::with( 'aliases' )
                        ->orderBy( 'title' )
                        ->get();

        $series = $series->sortBy( function ( $series ) {
            if ( strpos( $series->title, 'The ' ) === 0 ) {
                return substr( $series->title, 4, strlen( $series->title ) );
            }

            return $series->title;
        } );

        return $series->values()->all();
    }

    /**
     * @param $tvMazeId
     *
     * @return \App\Models\Series
     * @throws \App\TVMaze\Exceptions\NoSeriesFoundException
     * @throws \Exception
     */
    public function addRecord( $tvMazeId )
    {
        DB::beginTransaction();

        $tvMazeSeries = $this->TVMaze->showInfo( $tvMazeId );

        $series = new Series();
        $series->fill( (array) $tvMazeSeries );
        $series->save();

        if ( is_object( $tvMazeSeries->genres ) ) {
            foreach ( $tvMazeSeries->genres as $tvMazeGenre ) {
                $genre = Genre::findByName( $tvMazeGenre->name );

                if ( ! $genre ) {
                    $genre = new Genre();
                    $genre->fill( (array) $tvMazeGenre );
                    $genre->save();
                }

                $series->genres()->save( $genre );
            }
        }

        if ( is_object( $tvMazeSeries->episodes ) ) {
            foreach ( $tvMazeSeries->episodes as $tvMazeEpisode ) {
                $episode = new Episode();
                $episode->fill( (array) $tvMazeEpisode );
                $series->episodes()->save( $episode );
            }
        }

        DB::commit();

        try {
            $torrents = $this->torrentRepository->findLikeTitle( $series->title );
            foreach ( $torrents as $torrent ) {
                $this->torrentRepository->pairTorrent( $torrent );
            }
        }
        catch ( NotFoundException $e ) {
        }

        return $series;
    }

    /**
     * @param $id
     * @param $tvMazeId
     *
     * @return mixed
     * @throws \App\TVMaze\Exceptions\NoSeriesFoundException
     * @throws \Exception
     */
    public function updateRecord( $id, $tvMazeId )
    {
        $tvMazeSeries = $this->TVMaze->showInfo( $tvMazeId );

        if ( is_null( $series = Series::find( $id ) ) ) {
            throw new NoSeriesFoundException( 'Series does not exist' );
        }

        DB::beginTransaction();

        $series->fill( (array) $tvMazeSeries );
        $series->genres()->detach();
        $series->save();

        if ( is_object( $tvMazeSeries->genres ) ) {
            foreach ( $tvMazeSeries->genres as $tvMazeGenre ) {
                $genre = Genre::findByName( $tvMazeGenre->name );

                if ( ! $genre ) {
                    $genre = new Genre();
                    $genre->fill( (array) $tvMazeGenre );
                    $genre->save();
                }

                $series->genres()->save( $genre );
            }
        }

        if ( is_object( $tvMazeSeries->episodes ) ) {
            $tvMazeIdsUpdated = [ ];
            foreach ( $tvMazeSeries->episodes as $tvMazeEpisode ) {
                $episode = Episode::where( 'tvmaze_id', $tvMazeEpisode->tvmaze_id )
                                  ->first();

                if ( ! $episode ) {
                    $episode = new Episode();
                }

                $episode->fill( (array) $tvMazeEpisode );
                $series->episodes()->save( $episode );
                $tvMazeIdsUpdated[] = $tvMazeEpisode->tvmaze_id;
            }

            $series->episodes()
                   ->whereNotIn( 'tvmaze_id', $tvMazeIdsUpdated )
                   ->delete();
        }

        DB::commit();

        return $series;
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     * @throws \Exception
     */
    public function deleteRecord( $id )
    {
        $series = Series::find( $id );

        if ( ! $series ) {
            throw new NotFoundException( 'Series does not exist.' );
        }

        $folders = $this->folderRepository->getAll();

        $folders->each( function ( $folder ) use ( $series ) {
            $folderName = $folder->name . '/Media/TV/' . $series->title;

            if ( $this->flysystem->isDirectory( $folderName ) ) {
                $this->flysystem->deleteDirectory( $folderName );

                if ( ! $this->flysystem->isDirectory( $folderName ) ) {
                    $this->logbookRepository->addSuccess( 'Series/Delete', 'Deleted folder "' . $folderName . '"' );
                }
                else {
                    $this->logbookRepository->addFailure( 'Series/Delete', 'Failed to delete folder "' . $folderName . '"' );
                }
            }
        } );

        $series->episodes()->delete();
        $series->aliases()->delete();
        $series->genres()->detach();

        if ( $series->delete() ) {
            return $series;
        }

        throw new Exception( 'Unable to delete series.' );
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws \App\Exceptions\NotFoundException
     */
    public function findById( $id )
    {
        $series = Series::with( 'genres', 'aliases', 'episodes.files', 'episodes.downloads', 'episodes.torrents' )
                        ->find( $id );

        if ( ! $series ) {
            throw new NotFoundException( 'Series does not exist.' );
        }

        $series[ 'local_image_uri' ] = (is_file( $series[ 'local_image_uri' ] )) ?: '';

        return $series;
    }

    /**
     * @param \App\Models\Series $series
     *
     * @return mixed
     */
    public function downloadPoster( Series $series )
    {
        if ( $series->remote_image_uri ) {
            $remoteImageExtension = pathinfo( $series->remote_image_uri, PATHINFO_EXTENSION );
            $sanitizedSeriesTitle = preg_replace( '/[^a-zA-Z0-9_]+/', '', str_replace( ' ', '_', $series->title ) );
            $localImageFileName = $sanitizedSeriesTitle . '-' . $series->tvmaze_id . '.' . $remoteImageExtension;
            $local_image_uri = 'posters/series/' . $localImageFileName;

            if ( ! Storage::has( $local_image_uri ) ) {
                Storage::put( $local_image_uri, file_get_contents( $series->remote_image_uri ) );

                $series->local_image_uri = '/' . $local_image_uri;
                $series->save();
            }
        }
    }
}