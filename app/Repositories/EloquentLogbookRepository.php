<?php namespace App\Repositories;

use App\Contracts\LogbookRepository;
use App\Exceptions\NotFoundException;
use App\Models\Logbook;
use Exception;

/**
 * Class EloquentLogbookRepository
 * @package App\Repositories
 */
class EloquentLogbookRepository implements LogbookRepository
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        $logbooks = Logbook::with( 'user' )
                           ->orderBy( 'updated_at', 'DESC' )
                           ->orderBy( 'id', 'DESC' )
                           ->paginate( 20 )
                           ->toArray();

        return $logbooks;
    }

    /**
     * @return mixed
     */
    public function getLastEntry()
    {
        $logbook = Logbook::with( 'user' )
                          ->orderBy( 'updated_at', 'DESC' )
                          ->first();

        return $logbook;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws \App\Exceptions\NotFoundException
     */
    public function findById( $id )
    {
        $logbook = Logbook::with( 'user' )
                          ->find( $id );

        if ( ! $logbook ) {
            throw new NotFoundException( 'Log entry does not exist' );
        }

        return $logbook;
    }

    /**
     * @param $event
     * @param $type
     * @param $message
     */
    public function addRecord( $event, $type, $message )
    {
        Logbook::create( [
                             'ip_address' => request()->ip(),
                             'user_id'    => '1',
                             'event'      => $event,
                             'type'       => $type,
                             'message'    => $message,
                         ] );
    }

    /**
     * @param $event
     * @param $message
     */
    public function addSuccess( $event, $message )
    {
        $this->addRecord( $event, 'Success', $message );
    }

    /**
     * @param $event
     * @param $message
     */
    public function addFailure( $event, $message )
    {
        $this->addRecord( $event, 'Failure', $message );
    }

    /**
     * @param $event
     * @param $message
     */
    public function addError( $event, $message )
    {
        $this->addRecord( $event, 'Error', $message );
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     * @throws \Exception
     */
    public function deleteRecord( $id )
    {
        $logbook = Logbook::find( $id );

        if ( ! $logbook ) {
            throw new NotFoundException( 'Log entry does not exist.' );
        }

        if ( $logbook->delete() ) {
            return $logbook;
        }

        throw new Exception( 'Unable to delete log entry' );
    }
}