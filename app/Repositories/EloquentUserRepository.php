<?php namespace App\Repositories;

use App\Contracts\UserRepository;
use App\Exceptions\NotFoundException;
use App\Models\User;
use Exception;

/**
 * Class EloquentUserRepository
 * @package App\Repositories
 */
class EloquentUserRepository implements UserRepository
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        $users = User::all();

        return $users;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws \App\Exceptions\NotFoundException
     */
    public function findById( $id )
    {
        $user = User::with( 'logbooks' )
                    ->find( $id );

        if ( ! $user ) {
            throw new NotFoundException( 'User does not exist.' );
        }

        return $user;
    }

    /**
     * @param $input
     *
     * @return \App\Models\User
     */
    public function addRecord( $input )
    {
        $user = new User();
        $user->fill( $input );
        $user->save();

        return $user;
    }

    /**
     * @param $id
     * @param $input
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     */
    public function updateRecord( $id, $input )
    {
        $user = User::find( $id );

        if ( ! $user ) {
            throw new NotFoundException( 'User does not exist.' );
        }

        $user->fill( $input );
        $user->save();

        return $user;
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     * @throws \Exception
     */
    public function deleteRecord( $id )
    {
        $user = User::find( $id );

        if ( ! $user ) {
            throw new NotFoundException( 'User does not exist.' );
        }

        if ( $user->delete() ) {
            return $user;
        }

        throw new Exception( 'Unable to delete user.' );
    }
}