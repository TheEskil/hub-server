<?php namespace App\Repositories;

use App\Contracts\WishRepository;
use App\Exceptions\NotFoundException;
use App\Models\Wish;
use App\TheMovieDB\TheMovieDB;
use Exception;
use Illuminate\Support\Facades\Storage;

/**
 * Class EloquentWishRepository
 * @package App\Repositories
 */
class EloquentWishRepository implements WishRepository
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        $wishes = Wish::with( 'downloads', 'files', 'torrents' )
                      ->orderBy( 'title' )
                      ->get();

        $wishes = $wishes->sortBy( function ( $wish ) {
            if ( strpos( $wish->title, 'The ' ) === 0 ) {
                return substr( $wish->title, 4, strlen( $wish->title ) );
            }

            return $wish->title;
        } );

        return $wishes->values()->all();
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws \App\Exceptions\NotFoundException
     */
    public function findById( $id )
    {
        $wish = Wish::with( 'downloads', 'files', 'torrents' )
                    ->withTrashed()
                    ->find( $id );

        if ( ! $wish ) {
            throw new NotFoundException( 'Wish does not exist.' );
        }

        return $wish;
    }

    /**
     * @param $input
     *
     * @return \App\Models\Wish
     * @throws \App\TheMovieDB\Exceptions\NoResultsFoundException
     * @throws \Exception
     */
    public function addRecord( $input )
    {
        if ( array_key_exists( 'themoviedb_id', $input ) ) {
            $movieId = $input[ 'themoviedb_id' ];
        }
        else if ( array_key_exists( 'imdb_id', $input ) ) {
            $movieId = $input[ 'imdb_id' ];
        }

        $tmdb = new TheMovieDB();
        $movie = $tmdb->getMovie( $movieId );

        $wish = new Wish();
        $wish->fill( (array) $movie );
        $wish->save();

        return $wish;
    }

    /**
     * @param \App\Models\Wish $wish
     *
     * @return \App\Models\Wish
     * @throws \App\TheMovieDB\Exceptions\NoResultsFoundException
     * @throws \Exception
     */
    public function updateRecord( Wish $wish )
    {
        $tmdb = new TheMovieDB();
        $movie = $tmdb->getMovie( $wish->themoviedb_id );

        $wish->fill( (array) $movie );
        $wish->save();

        return $wish;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \App\Exceptions\NotFoundException
     * @throws \Exception
     */
    public function deleteRecord( $id )
    {
        $wish = Wish::with( 'files', 'downloads' )
                    ->get()
                    ->find( $id );

        if ( ! $wish ) {
            throw new NotFoundException( 'Wish does not exist.' );
        }

        if ( ! count( $wish->files ) && ! count( $wish->downloads ) ) {
            $deletedWish = $wish->forceDelete();
        }
        else {
            $deletedWish = $wish->delete();
        }

        if ( $deletedWish ) {
            return $wish;
        }

        throw new Exception( 'Unable to delete wish.' );
    }

    /**
     * @return array
     */
    public function getBadge()
    {
        $fulfilledWishes = Wish::has( 'downloads' )->get()->count() ?: 0;
        $unfulfilledWishes = Wish::has( 'downloads', '<=', 0 )->get()->count() ?: 0;

        return [
            'fulfilled'   => $fulfilledWishes,
            'unfulfilled' => $unfulfilledWishes,
        ];
    }

    /**
     * @param \App\Models\Wish $wish
     *
     * @return mixed
     */
    public function downloadPoster( Wish $wish )
    {
        if ( $wish->remote_image_uri ) {
            $tmdb = new TheMovieDB();

            $base_url = $tmdb->configuration->images->base_url;
            $image_size = $tmdb->configuration->images->poster_sizes[ 2 ];
            $remote_image_uri = $base_url . $image_size . $wish->remote_image_uri;

            $local_image_uri = 'posters/movies/' . basename( $remote_image_uri );

            if ( ! Storage::has( $local_image_uri ) ) {
                Storage::put( $local_image_uri, file_get_contents( $remote_image_uri ) );

                $wish->local_image_uri = '/' . $local_image_uri;
                $wish->save();
            }
        }
    }
}
