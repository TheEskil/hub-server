<?php namespace App\Repositories;

use App\Contracts\FeedRepository;
use App\Exceptions\NotFoundException;
use App\Models\Feed;
use App\Models\Torrent;
use Exception;

/**
 * Class EloquentFeedRepository
 * @package App\Repositories
 */
class EloquentFeedRepository implements FeedRepository
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        $feeds = Feed::orderBy( 'title' )
                     ->get();

        $feeds->each(
            function ( $feed ) {
                $feed->numberOfTorrents = Torrent::where( 'feed_id', '=', $feed->id )
                                                 ->count();

                if ( $feed->numberOfTorrents >= 1000 ) {
                    $feed->numberOfTorrents = str_replace( '000', '', round( $feed->numberOfTorrents, -3 ) ) . 'K';
                }
            }
        );

        return $feeds;
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     */
    public function findById( $id )
    {
        $feeds = Feed::find( $id );

        if ( ! $feeds ) {
            throw new NotFoundException( 'Feed does not exist.' );
        }

        return $feeds;
    }

    /**
     * @param $input
     *
     * @return \App\Models\Feed
     */
    public function addRecord( $input )
    {
        $feed = new Feed();
        $feed->fill( $input );
        $feed->save();

        return $feed;
    }

    /**
     * @param $id
     * @param $input
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     */
    public function updateRecord( $id, $input )
    {
        $feed = Feed::find( $id );

        if ( ! $feed ) {
            throw new NotFoundException( 'Feed does not exist.' );
        }

        $feed->fill( $input );
        $feed->save();

        return $feed;
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     * @throws \Exception
     */
    public function deleteRecord( $id )
    {
        $feed = Feed::find( $id );

        if ( ! $feed ) {
            throw new NotFoundException( 'Feed does not exist.' );
        }

        if ( $feed->delete() ) {
            $feed->searches()->delete();
            $feed->torrents()->forceDelete();

            return $feed;
        }

        throw new Exception( 'Unable to delete feed.' );
    }
}