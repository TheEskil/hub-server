<?php namespace App\Repositories;

use App\Contracts\FolderRepository;
use App\Contracts\LogbookRepository;
use App\Exceptions\NotFoundException;
use App\Models\Folder;
use Exception;
use Illuminate\Filesystem\Filesystem as Flysystem;

/**
 * Class EloquentFolderRepository
 * @package App\Repositories
 */
class EloquentFolderRepository implements FolderRepository
{
    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    private $flysystem;

    /**
     * @var \App\Contracts\LogbookRepository
     */
    private $logbookRepository;

    /**
     * EloquentFolderRepository constructor.
     *
     * @param \Illuminate\Filesystem\Filesystem $flysystem
     * @param \App\Contracts\LogbookRepository  $logbookRepository
     */
    public function __construct( Flysystem $flysystem, LogbookRepository $logbookRepository )
    {
        $this->flysystem = $flysystem;
        $this->logbookRepository = $logbookRepository;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        $folders = Folder::all();

        $folders->each(
            function ( $folder ) {
                $folder->space = $this->getFolderSpace( $folder );
            }
        );

        return $folders;
    }

    /**
     * @param $input
     *
     * @return \App\Models\Folder
     * @throws \Exception
     */
    public function addRecord( $input )
    {
        $folder = new Folder();
        $folder->fill( $input );

        if ( ! $this->flysystem->isDirectory( $input[ 'name' ] ) ) {
            $this->flysystem->makeDirectory( $input[ 'name' ], 0700 );

            if ( ! $this->flysystem->isDirectory( $input[ 'name' ] ) ) {
                throw new Exception( error_get_last() );
            }
        }

        foreach ( $folder->folderStructure as $subFolder ) {
            if ( ! $this->flysystem->isDirectory( $input[ 'name' ] . '/' . $subFolder ) ) {
                $this->flysystem->makeDirectory( $input[ 'name' ] . '/' . $subFolder );
            }
        }

        $folder->save();

        return $folder;
    }

    /**
     * @param $id
     * @param $input
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     * @throws \Exception
     */
    public function updateRecord( $id, $input )
    {
        $folder = Folder::find( $id );

        if ( ! $folder ) {
            throw new NotFoundException( 'Folder does not exist.' );
        }

        $folderBeforeUpdate = $folder->name;
        $folder->fill( $input );

        if ( ! rename( $folderBeforeUpdate, $input[ 'name' ] ) ) {
            throw new Exception( error_get_last() );
        }

        $folder->save();

        return $folder;
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     * @throws \Exception
     */
    public function deleteRecord( $id )
    {
        $folder = Folder::find( $id );

        if ( ! $folder ) {
            throw new NotFoundException( 'Folder does not exist.' );
        }

        if ( $folder->delete() ) {
            return $folder;
        }

        throw new Exception( 'Unable to delete folder' );
    }

    /**
     * @param $folder
     *
     * @return array
     */
    public function getFolderSpace( $folder )
    {
        if ( is_dir( $folder->name ) ) {
            $folderSpace = [ ];
            $folderSpace[ 'free' ][ 'bytes' ] = disk_free_space( $folder->name );
            $folderSpace[ 'total' ][ 'bytes' ] = disk_total_space( $folder->name );

            $folderSpace[ 'free' ][ 'human' ] = formatFileSize( $folderSpace[ 'free' ][ 'bytes' ] );
            $folderSpace[ 'total' ][ 'human' ] = formatFileSize( $folderSpace[ 'total' ][ 'bytes' ] );

            return $folderSpace;
        }
        else {
            $folderSpace = [ ];
            $folderSpace[ 'free' ][ 'bytes' ] = 0;
            $folderSpace[ 'total' ][ 'bytes' ] = 0;
            $folderSpace[ 'free' ][ 'human' ] = 0;
            $folderSpace[ 'total' ][ 'human' ] = 0;

            return $folderSpace;
        }
    }

    /**
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     * @throws \Exception
     */
    function getActiveFolder()
    {
        $folders = $this->getAll();

        if ( ! $folders ) {
            throw new NotFoundException( 'No folders available.' );
        }

        $folder = $folders->sortByDesc( function ( $folder ) {
            return disk_free_space( $folder->name );
        } )->first();

        if ( $folder->space[ 'free' ][ 'bytes' ] <= 5368709120 ) { // Has to have atleast 5GB of free space
            throw new Exception( 'Not enough space on any of the folders.' );
        }

        return $folder;
    }

    /**
     * @param $folderSubPath
     *
     * @return mixed
     */
    public function createSubfolders( $folderSubPath )
    {
        $folders = $this->getAll();

        $numberOfFoldersCreated = 0;
        foreach ( $folders as $folder ) {
            if ( ! $this->flysystem->isDirectory( $folder->name . $folderSubPath ) ) {
                $this->flysystem->makeDirectory( $folder->name . $folderSubPath );

                if ( $this->flysystem->isDirectory( $folder->name . $folderSubPath ) ) {
                    $numberOfFoldersCreated++;
                    // echo $this->carbon->now() . ' Created ' . $folder->name . '/Media/TV/' . $event->series->title;
                }
            }
        }

        if ( $numberOfFoldersCreated ) {
            $this->logbookRepository->addSuccess( 'Series/Folders', 'Created ' . $numberOfFoldersCreated . ' missing folders' );
        }
    }
}