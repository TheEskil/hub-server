<?php namespace App\Repositories;

use App\Contracts\AliasRepository;
use App\Contracts\SeriesRepository;
use App\Exceptions\NotFoundException;
use App\Models\Alias;
use Exception;

/**
 * Class EloquentAliasRepository
 * @package App\Repositories
 */
class EloquentAliasRepository implements AliasRepository
{
    /**
     * @var \App\Contracts\SeriesRepository
     */
    private $seriesRepository;

    /**
     * EloquentAliasRepository constructor.
     *
     * @param \App\Contracts\SeriesRepository $seriesRepository
     */
    public function __construct( SeriesRepository $seriesRepository )
    {
        $this->seriesRepository = $seriesRepository;
    }

    /**
     * @param $input
     *
     * @return \App\Models\Alias
     */
    public function addRecord( $input )
    {
        $series = $this->seriesRepository->findById( $input[ 'series_id' ] );
        $alias = new Alias();
        $alias->fill( $input );
        $series->aliases()->save( $alias );

        return $alias;
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws \App\Exceptions\NotFoundException
     * @throws \Exception
     */
    public function deleteRecord( $id )
    {
        $alias = Alias::find( $id );

        if ( ! $alias ) {
            throw new NotFoundException( 'Series alias does not exist' );
        }

        if ( $alias->delete() ) {
            return $alias;
        }

        throw new Exception( 'Unable to delete series alias' );
    }
}