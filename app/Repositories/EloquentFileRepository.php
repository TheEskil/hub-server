<?php

namespace App\Repositories;

use App\Contracts\FileRepository;
use App\Contracts\FolderRepository;
use App\Models\Alias;
use App\Models\File;
use App\Models\Series;
use App\Models\Wish;
use App\ReleaseParser\ReleaseParser;
use App\Support\FilesHelper;
use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem as Flysystem;

/**
 * Class EloquentFileRepository
 * @package App\Repositories
 */
class EloquentFileRepository implements FileRepository
{
    /**
     * @var \App\Contracts\FolderRepository
     */
    private $folderRepository;

    /**
     * @var \App\Support\FilesHelper
     */
    private $filesHelper;

    /**
     * @var \Carbon\Carbon
     */
    private $carbon;

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    private $flysystem;

    /**
     * EloquentFileRepository constructor.
     *
     * @param \App\Contracts\FolderRepository   $folderRepository
     * @param \App\Support\FilesHelper          $filesHelper
     * @param \Carbon\Carbon                    $carbon
     * @param \Illuminate\Filesystem\Filesystem $flysystem
     */
    public function __construct( FolderRepository $folderRepository, FilesHelper $filesHelper, Carbon $carbon, Flysystem $flysystem )
    {
        $this->folderRepository = $folderRepository;
        $this->filesHelper = $filesHelper;
        $this->carbon = $carbon;
        $this->flysystem = $flysystem;
    }

    /**
     * @param $file
     * @param $path
     *
     * @return mixed|null
     */
    public function matchFile( $file, $path )
    {
        $parser = new ReleaseParser();

        if ( is_null( $fileObj = File::where( 'path', $path )
                                     ->where( 'file', $file )
                                     ->first() )
        ) {
            $fileObj = File::create( [
                                         'path' => $path,
                                         'file' => $file,
                                         'size' => $this->flysystem->size( $path . '/' . $file ),
                                     ] );
        }

        if ( ! is_null( $parsed = $parser->parse( $file ) ) ) {
            switch ( $parsed->type ) {
                case 'Series':
                case 'Daily':
                    if ( ! is_null( $series = Series::findByTitle( $parsed->title ) ) ) {
                        $series = $series;
                    }
                    else if ( ! is_null( $series = Alias::findByTitle( $parsed->title ) ) ) {
                        $series = $series->series()->first();
                    }

                    if ( $parsed->type == 'Series' ) {
                        $parsed->episodes->each( function ( $episode ) use ( $series, $fileObj ) {
                            if ( $series ) {
                                $episode = $series->episodes()
                                                  ->where( 'season_number', $episode->season_number )
                                                  ->where( 'episode_number', $episode->episode_number )
                                                  ->first();

                                if ( $episode ) {
                                    $fileObj->type = (pathinfo( $fileObj->file, PATHINFO_EXTENSION ) == 'srt') ? 'Subtitle' : 'Series';
                                    $episode->files()->save( $fileObj );
                                }
                            }
                        } );
                    }
                    else if ( $parsed->type == 'Daily' ) {
                        if ( $series ) {
                            $convertedAirdate = $this->carbon->parse( $parsed->year . '-' . $parsed->month . '-' . $parsed->day . ' ' . $series->airtime, $series->timezone )->tz( env( 'TIMEZONE' ) );

                            $episode = $series->episodes()
                                              ->whereYear( 'airdate', '=', $convertedAirdate->year )
                                              ->whereMonth( 'airdate', '=', $convertedAirdate->month )
                                              ->whereDay( 'airdate', '=', $convertedAirdate->day )
                                              ->first();

                            if ( $episode ) {
                                $fileObj->type = 'Daily';
                                $episode->files()->save( $fileObj );
                            }
                        }
                    }
                    break;
                case 'Movie':
                    $fileObj->type = (pathinfo( $fileObj->file, PATHINFO_EXTENSION ) == 'srt') ? 'Subtitle' : 'Movie';
                    $fileObj->save();

                    $wish = Wish::where( function ( $query ) use ( $parsed ) {
                        $query->orWhere( 'title', $parsed->title );
                        $query->orWhere( 'original_title', $parsed->title );
                    } )
                                ->where( 'year', $parsed->year )
                                ->first();

                    if ( $wish ) {
                        $wish->files()->save( $fileObj );
                    }

                    break;
            }

            return $fileObj->id;
        }

        return null;
    }

    /**
     *
     */
    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    /**
     *
     */
    public function getEpisodes()
    {
        // TODO: Implement getEpisodes() method.
    }

    /**
     *
     */
    public function getMovies()
    {
        // TODO: Implement getMovies() method.
    }

    /**
     *
     */
    public function getWishes()
    {
        // TODO: Implement getWishes() method.
    }

    /**
     *
     */
    public function getSubtitles()
    {
        // TODO: Implement getSubtitles() method.
    }

    /**
     *
     */
    public function getUncategorized()
    {
        // TODO: Implement getUncategorized() method.
    }

    /**
     *
     */
    public function getDuplicates()
    {
        // TODO: Implement getDuplicates() method.
    }

    /**
     *
     */
    public function getDownloads()
    {
        // TODO: Implement getDownloads() method.
    }

    /**
     * @param $id
     */
    public function deleteRecord( $id )
    {
        // TODO: Implement deleteRecord() method.
    }
}