<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use Illuminate\View\View;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    use ValidatesRequests, DispatchesJobs;

    /**
     * @var int
     */
    protected $statusCode = 200;

    /**
     *
     */
    protected function setupLayout()
    {
        if ( ! is_null( $this->layout ) ) {
            $this->layout = View::make( $this->layout );
        }
    }

    /**
     * @param       $message
     * @param       $record_id
     * @param array $record
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondCreated( $message, $record_id, array $record )
    {
        return $this->setStatusCode( 201 )->respond(
            [
                'data' => [
                    'message'           => $message,
                    'created_record_id' => $record_id,
                    'created_record'    => $record,
                ],
            ]
        );
    }

    /**
     * @param       $data
     * @param array $headers
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respond( $data, $headers = [ ] )
    {
        if ( (is_array( $data ) && ! array_key_exists( 'error', $data )) ||
             (is_object( $data ) && ! property_exists( $data, 'error' ))
        ) {
            if ( is_array( $data ) && ! array_key_exists( 'data', $data ) ) {
                $data = [ 'data' => $data ];
            }
            else {
                if ( is_object( $data ) && ! property_exists( $data, 'data' ) ) {
                    $data = [ 'data' => $data ];
                }
            }
        }

        return response()->json( $data, $this->getStatusCode(), $headers );
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param $statusCode
     *
     * @return $this
     */
    public function setStatusCode( $statusCode )
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param       $message
     * @param       $record_id
     * @param array $record
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondUpdated( $message, $record_id, array $record )
    {
        return $this->setStatusCode( 200 )->respond(
            [
                'data' => [
                    'message'           => $message,
                    'updated_record_id' => $record_id,
                    'updated_record'    => $record,
                ],
            ]
        );
    }

    /**
     * @param $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondJobDispatched( $message )
    {
        return $this->setStatusCode( 200 )->respond(
            [
                'data' => [
                    'message' => $message,
                ],
            ]
        );
    }

    /**
     * @param       $message
     * @param       $record_id
     * @param array $record
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondDownloaded( $message, $record_id, array $record )
    {
        return $this->setStatusCode( 200 )->respond(
            [
                'data' => [
                    'message'              => $message,
                    'downloaded_record_id' => $record_id,
                    'downloaded_record'    => $record,
                ],
            ]
        );
    }

    /**
     * @param       $message
     * @param       $record_id
     * @param array $record
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondDeleted( $message, $record_id, array $record )
    {
        return $this->setStatusCode( 200 )->respond(
            [
                'data' => [
                    'message'           => $message,
                    'deleted_record_id' => $record_id,
                    'deleted_record'    => $record,
                ],
            ]
        );
    }

    /**
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondNotFound( $message = 'Not found.' )
    {
        return $this->setStatusCode( 404 )->respondWithError( $message );
    }

    /**
     * @param $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithError( $message )
    {
        return $this->respond(
            [
                'error' => [
                    'message'     => $message,
                    'status_code' => $this->getStatusCode(),
                ],
            ]
        );
    }

    /**
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithBadRequest( $message = 'Bad request.' )
    {
        return $this->setStatusCode( 400 )->respondWithError( $message );
    }

    /**
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithUnauthorized( $message = 'Unauthorized.' )
    {
        return $this->setStatusCode( 401 )->respondWithError( $message );
    }

    /**
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithForbidden( $message = 'Forbidden.' )
    {
        return $this->setStatusCode( 403 )->respondWithError( $message );
    }

    /**
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithUnprocessableEntity( $message = 'Unprocessable Entity' )
    {
        return $this->setStatusCode( 422 )->respondWithError( $message );
    }

    /**
     * @param $items
     * @param $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithPagination( $items, $data )
    {
        $data = array_merge(
            $data,
            [
                'paginator' => [
                    'total_count'  => $items->getTotal(),
                    'total_pages'  => ceil( $items->getTotal() / $items->getPerPage() ),
                    'current_page' => $items->getCurrentPage(),
                    'limit'        => $items->getPerPage(),
                ],
            ]
        );

        return $this->respond( $data );
    }
}