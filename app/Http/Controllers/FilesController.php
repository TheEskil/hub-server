<?php namespace App\Http\Controllers;

use App\Contracts\LogbookRepository;
use App\Http\Requests;
use App\Jobs\CompletedFilesJob;
use App\Models\File;
use App\Support\FilesHelper;
use Illuminate\Filesystem\Filesystem as Flysystem;

/**
 * Class FilesController
 * @package App\Http\Controllers
 */
class FilesController extends ApiController
{
    /**
     * @var \App\Support\FilesHelper
     */
    private $filesHelper;

    /**
     * @var \App\Contracts\LogbookRepository
     */
    private $logbookRepository;

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    private $flysystem;

    /**
     * FilesController constructor.
     *
     * @param \App\Support\FilesHelper          $filesHelper
     * @param \App\Contracts\LogbookRepository  $logbookRepository
     * @param \Illuminate\Filesystem\Filesystem $flysystem
     */
    public function __construct( FilesHelper $filesHelper, LogbookRepository $logbookRepository, Flysystem $flysystem )
    {
        $this->filesHelper = $filesHelper;
        $this->logbookRepository = $logbookRepository;
        $this->flysystem = $flysystem;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $files = File::orderBy( 'created_at', 'DESC' )->paginate( 50 )->toArray();

        return $this->respond( $files );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEpisodes()
    {
        $files = File::whereNotNull( 'episode_id' )->where( 'file', 'LIKE', 'B%' )->orderBy( 'file' )->paginate( 100 )->toArray();

        return $this->respond( $files );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMovies()
    {
        $files = File::where( 'type', 'Movie' )->orderBy( 'file' )->paginate( 100 )->toArray();

        return $this->respond( $files );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWishes()
    {
        $files = File::whereNotNull( 'wish_id' )->orderBy( 'file' )->paginate( 100 )->toArray();

        return $this->respond( $files );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubtitles()
    {
        $files = File::where( 'type', 'Subtitle' )->orderBy( 'file' )->paginate( 100 )->toArray();;

        return $this->respond( $files );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUncategorized()
    {
        $files = File::where( 'type', 'Uncategorized' )->orderBy( 'file' )->paginate( 100 )->toArray();;

        return $this->respond( $files );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDuplicates()
    {
        /*
        $files = File::whereIn( 'id', function ( $query ) {
            $query->select( 'id' )->from( 'users' )->groupBy( 'ip' )->havingRaw( 'count(*) > 1' );
        } )->get();
*/
        $files = File::whereIn( 'id', function ( $query ) {
            $query->select( 'episode_id' )->from( 'files' )->groupBy( 'id' )->havingRaw( 'count(*) > 1' );
        } )->whereNotNull( 'episode_id' )->get()->toArray();

        $files = File::select( '*' )->from( 'files' )->groupBy( 'wish_id' )->havingRaw( 'count(*) > 1' )->get()->toArray();

        /*
         * Duplicate Episodes
         *
        select files.* from files inner join
        (select episode_id from files
        group by episode_id
        having count(*)>1) t1
        on files.episode_id=t1.episode_id;
        */

        /*
         * Duplicate Wishes
         *
        select files.* from files inner join
        (select wish_id from files
        group by wish_id
        having count(*)>1) t1
        on files.wish_id=t1.wish_id;
        */

        return $this->respond( $files );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDownloads()
    {
        $files = $this->filesHelper->getCompletedFiles( 'Completed' );

        return $this->respond( $files );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function process()
    {
        $job = ( new CompletedFilesJob() );
        $responseMessage = 'Added "Completed Files" to job queue.';
        $this->dispatch( $job );

        $this->logbookRepository->addSuccess( 'File/Process', $responseMessage );

        return $this->respondJobDispatched( $responseMessage );
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete( $id )
    {
        $file = File::find( $id );

        if ( ! $file ) {
            return $this->respondNotFound( 'File not found' );
        }

        if ( $this->flysystem->exists( $file->path . '/' . $file->file ) ) {
            $this->flysystem->delete( $file->path . '/' . $file->file );

            if ( ! $this->flysystem->exists( $file->path . '/' . $file->file ) ) {
                $file->delete();

                return $this->respondDeleted( 'Deleted file', $id, $file->toArray() );
            }
            else {
                return $this->respondWithError( 'Failed to delete file' );
            }
        }
        else {
            $file->delete();

            return $this->respondDeleted( 'Deleted file', $id, $file->toArray() );
        }
    }
}