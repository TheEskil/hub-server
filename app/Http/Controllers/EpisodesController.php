<?php namespace App\Http\Controllers;

use App\Contracts\EpisodeRepository;
use App\Exceptions\NotFoundException;

/**
 * Class EpisodesController
 * @package App\Http\Controllers
 */
class EpisodesController extends ApiController
{
    /**
     * @var \App\Contracts\EpisodeRepository
     */
    private $episodeRepository;

    /**
     * EpisodesController constructor.
     *
     * @param \App\Contracts\EpisodeRepository $episodeRepository
     */
    public function __construct( EpisodeRepository $episodeRepository )
    {
        $this->episodeRepository = $episodeRepository;
    }

    /**
     * @param int $numberOfDays
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRecent( $numberOfDays = 5 )
    {
        try {
            $episodes = $this->episodeRepository->getRecent( $numberOfDays );

            return $this->respond( $episodes );
        }
        catch ( NotFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUpcoming()
    {
        try {
            $episodes = $this->episodeRepository->getUpcoming();

            return $this->respond( $episodes );
        }
        catch ( NotFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }
}