<?php namespace App\Http\Controllers;

use App\Contracts\FolderRepository;
use App\Contracts\LogbookRepository;
use App\Contracts\SeriesRepository;
use App\Events\SeriesWasAdded;
use App\Exceptions\NotFoundException;
use App\Http\Requests\SeriesRequest;
use App\Jobs\SeriesRefreshJob;
use App\TVMaze\Exceptions\NoSeriesFoundException;
use Exception;
use Illuminate\Support\Facades\Event;

/**
 * Class SeriesController
 * @package App\Http\Controllers
 */
class SeriesController extends ApiController
{
    /**
     * @var \App\Contracts\SeriesRepository
     */
    protected $seriesRepository;

    /**
     * @var \App\Contracts\LogbookRepository
     */
    private $logbookRepository;

    /**
     * @var \App\Contracts\FolderRepository
     */
    private $folderRepository;

    /**
     * SeriesController constructor.
     *
     * @param \App\Contracts\SeriesRepository  $seriesRepository
     * @param \App\Contracts\LogbookRepository $logbookRepository
     * @param \App\Contracts\FolderRepository  $folderRepository
     */
    function __construct( SeriesRepository $seriesRepository, LogbookRepository $logbookRepository, FolderRepository $folderRepository )
    {
        $this->seriesRepository = $seriesRepository;
        $this->logbookRepository = $logbookRepository;
        $this->folderRepository = $folderRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $series = $this->seriesRepository->getAll();

        return $this->respond( $series );
    }

    /**
     * @param \App\Http\Requests\SeriesRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add( SeriesRequest $request )
    {
        try {
            $series = $this->seriesRepository->addRecord( $request->get( 'tvmaze_id' ) );

            if ( ! is_null( $series ) ) {
                // TODO: Move into SeriesRepository::addRecord()
                $this->logbookRepository->addSuccess( 'Series/Add', 'Added series "' . $series->title . '"' );

                $this->folderRepository->createSubfolders( '/Media/TV/' . $series->title );
                $this->seriesRepository->downloadPoster( $series );

                return $this->respondCreated( 'Successfully added series', $series->id, $series->toArray() );
            }

            return $this->respondWithError( 'Failed to add series. Check the logs for information.' );
        }
        catch ( NoSeriesFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOne( $id )
    {
        try {
            $series = $this->seriesRepository->findById( $id );

            return $this->respond( $series );
        }
        catch ( NotFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEpisodes( $id )
    {
        try {
            $series = $this->seriesRepository->findById( $id )->episodes;

            return $this->respond( $series );
        }
        catch ( NotFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete( $id )
    {
        try {
            $series = $this->seriesRepository->deleteRecord( $id );

            // TODO: Move into SeriesRepository::deleteRecord()
            $this->logbookRepository->addSuccess( 'Series/Delete', 'Deleted series "' . $series->title . '"' );

            return $this->respondDeleted( 'Successfully deleted series', $series->id, $series->toArray() );
        }
        catch ( NotFoundException $e ) {
            // TODO: Move into SeriesRepository::deleteRecord()
            $this->logbookRepository->addError( 'Series/Delete', $e->getMessage() );

            return $this->respondNotFound( $e->getMessage() );
        }
        catch ( Exception $e ) {
            // TODO: Move into SeriesRepository::deleteRecord()
            $this->logbookRepository->addError( 'Series/Delete', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param null $seriesId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh( $seriesId = null )
    {
        if ( $seriesId ) {
            try {
                $series = $this->seriesRepository->findById( $seriesId );

                $job = ( new SeriesRefreshJob( $series->id, $series->tvmaze_id ) );
                $this->dispatch( $job );

                $responseMessage = 'Added "Series Refresh: ' . $series->title . '" to job queue.';

                $this->logbookRepository->addSuccess( 'Series/Refresh', $responseMessage );

                return $this->respondJobDispatched( $responseMessage );
            }
            catch ( NotFoundException $e ) {
                $this->logbookRepository->addError( 'Series/Refresh', $e->getMessage() );

                return $this->respondNotFound( $e->getMessage() );
            }
        }
        else {
            $series = $this->seriesRepository->getAll();

            foreach ( $series as $serie ) {
                $job = ( new SeriesRefreshJob( $serie->id, $serie->tvmaze_id ) );

                $this->dispatch( $job );
            }

            $responseMessage = 'Added "Series Refresh" to job queue.';

            $this->logbookRepository->addSuccess( 'Series/Refresh', $responseMessage );

            return $this->respondJobDispatched( $responseMessage );
        }
    }
}