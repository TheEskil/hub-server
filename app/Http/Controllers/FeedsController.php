<?php namespace App\Http\Controllers;

use App\Contracts\FeedRepository;
use App\Contracts\LogbookRepository;
use App\Contracts\TorrentRepository;
use App\Exceptions\NotFoundException;
use App\Http\Requests\FeedRequest;
use App\Jobs\FeedsRefreshJob;
use Exception;

/**
 * Class FeedsController
 * @package App\Http\Controllers
 */
class FeedsController extends ApiController
{
    /**
     * @var \App\Contracts\FeedRepository
     */
    protected $feedRepository;

    /**
     * @var \App\Contracts\TorrentRepository
     */
    private $torrentRepository;

    /**
     * @var \App\Contracts\LogbookRepository
     */
    private $logbookRepository;

    /**
     * FeedsController constructor.
     *
     * @param \App\Contracts\FeedRepository    $feedRepository
     * @param \App\Contracts\TorrentRepository $torrentRepository
     * @param \App\Contracts\LogbookRepository $logbookRepository
     */
    function __construct( FeedRepository $feedRepository, TorrentRepository $torrentRepository, LogbookRepository $logbookRepository )
    {
        $this->feedRepository = $feedRepository;
        $this->torrentRepository = $torrentRepository;
        $this->logbookRepository = $logbookRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $feeds = $this->feedRepository->getAll();

        return $this->respond( $feeds );
    }

    /**
     * @param \App\Http\Requests\FeedRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add( FeedRequest $request )
    {
        try {
            $feed = $this->feedRepository->addRecord( $request->all() );

            // TODO: Move into FeedRepository::addRecord()
            $this->logbookRepository->addSuccess( 'Feed/Add', 'Added feed "' . $feed->title . '"' );

            return $this->respondCreated( 'Successfully added feed', $feed->id, $feed->toArray() );
        }
        catch ( Exception $e ) {
            // TODO: Move into FeedRepository::addRecord()
            $this->logbookRepository->addError( 'Feed/Add', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOne( $id )
    {
        try {
            $feed = $this->feedRepository->findById( $id );
            $torrents = $this->torrentRepository->findByFeedId( $id );

            return $this->respond( [
                                       'feed'     => $feed,
                                       'torrents' => $torrents,
                                   ] );
        }
        catch ( NotFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }

    /**
     * @param \App\Http\Requests\FeedRequest $request
     * @param                                $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( FeedRequest $request, $id )
    {
        try {
            $feed = $this->feedRepository->updateRecord( $id, $request->all() );

            // TODO: Move into FeedRepository::updateRecord()
            $this->logbookRepository->addSuccess( 'Feed/Update', 'Updated feed "' . $feed->title . '"' );

            return $this->respondUpdated( 'Successfully updated feed', $feed->id, $feed->toArray() );
        }
        catch ( NotFoundException $e ) {
            // TODO: Move into FeedRepository::updateRecord()
            $this->logbookRepository->addError( 'Feed/Update', $e->getMessage() );

            return $this->respondNotFound( $e->getMessage() );
        }
        catch ( Exception $e ) {
            // TODO: Move into FeedRepository::updateRecord()
            $this->logbookRepository->addError( 'Feed/Update', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete( $id )
    {
        try {
            $feed = $this->feedRepository->deleteRecord( $id );

            // TODO: Move into FeedRepository::deleteRecord()
            $this->logbookRepository->addSuccess( 'Feed/Delete', 'Deleted feed "' . $feed->title . '"' );

            return $this->respondDeleted( 'Successfully deleted feed', $feed->id, $feed->toArray() );
        }
        catch ( NotFoundException $e ) {
            // TODO: Move into FeedRepository::deleteRecord()
            $this->logbookRepository->addError( 'Feed/Update', $e->getMessage() );

            return $this->respondNotFound( $e->getMessage() );
        }
        catch ( Exception $e ) {
            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param null $feedId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh( $feedId = null )
    {
        if ( $feedId ) {
            try {
                $feed = $this->feedRepository->findById( $feedId );

                $job = ( new FeedsRefreshJob( $feedId ) );
                $responseMessage = 'Added "Feeds Refresh: ' . $feed->title . '" to job queue.';
            }
            catch ( NotFoundException $e ) {
                $this->logbookRepository->addError( 'Feed/Refresh', $e->getMessage() );

                return $this->respondNotFound( $e->getMessage() );
            }
        }
        else {
            $job = ( new FeedsRefreshJob() );
            $responseMessage = 'Added "Feeds Refresh" to job queue.';
        }

        $this->dispatch( $job );

        $this->logbookRepository->addSuccess( 'Feed/Refresh', $responseMessage );

        return $this->respondJobDispatched( $responseMessage );
    }
}