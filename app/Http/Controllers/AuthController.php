<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends ApiController
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate( Request $request )
    {
        $credentials = $request->only( 'email', 'password' );

        try {
            if ( ! $token = JWTAuth::attempt( $credentials ) ) {
                return $this->respondWithUnauthorized( 'Invalid credentials' );
            }
        }
        catch ( JWTException $e ) {
            return $this->setStatusCode( 500 )->respondWithError( 'Could not create token' );
        }

        return response()->json( compact( 'token' ) );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthenticatedUser()
    {
        try {
            if ( ! $user = JWTAuth::parseToken()->authenticate() ) {
                return response()->json( [ 'user_not_found' ], 404 );
            }
        }
        catch ( TokenExpiredException $e ) {
            return response()->json( [ 'token_expired' ], $e->getStatusCode() );
        }
        catch ( TokenInvalidException $e ) {
            return response()->json( [ 'token_invalid' ], $e->getStatusCode() );
        }
        catch ( JWTException $e ) {
            return response()->json( [ 'token_absent' ], $e->getStatusCode() );
        }

        return response()->json( compact( 'user' ) );
    }
}