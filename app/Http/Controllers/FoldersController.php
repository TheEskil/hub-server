<?php namespace App\Http\Controllers;

use App\Contracts\FolderRepository;
use App\Contracts\LogbookRepository;
use App\Exceptions\NotFoundException;
use App\Http\Requests\FolderRequest;
use App\Jobs\FoldersRebuildJob;
use Exception;

/**
 * Class FoldersController
 * @package App\Http\Controllers
 */
class FoldersController extends ApiController
{
    /**
     * @var \App\Contracts\FolderRepository
     */
    protected $folderRepository;

    /**
     * @var \App\Contracts\LogbookRepository
     */
    private $logbookRepository;

    /**
     * FoldersController constructor.
     *
     * @param \App\Contracts\FolderRepository  $folderRepository
     * @param \App\Contracts\LogbookRepository $logbookRepository
     */
    function __construct( FolderRepository $folderRepository, LogbookRepository $logbookRepository )
    {
        $this->folderRepository = $folderRepository;
        $this->logbookRepository = $logbookRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $folders = $this->folderRepository->getAll();

        return $this->respond( $folders );
    }

    /**
     * @param \App\Http\Requests\FolderRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add( FolderRequest $request )
    {
        try {
            $folder = $this->folderRepository->addRecord( $request->all() );

            // TODO: Move into FolderRepository::addRecord()
            $this->logbookRepository->addSuccess( 'Folder/Add', 'Added folder "' . $folder->name . '"' );

            return $this->respondCreated( 'Successfully added folder', $folder->id, $folder->toArray() );
        }
        catch ( Exception $e ) {
            // TODO: Move into FolderRepository::addRecord()
            $this->logbookRepository->addError( 'Folder/Add', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param \App\Http\Requests\FolderRequest $request
     * @param                                  $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( FolderRequest $request, $id )
    {
        try {
            $folder = $this->folderRepository->updateRecord( $id, $request->all() );

            // TODO: Move into FolderRepository::updateRecord()
            $this->logbookRepository->addSuccess( 'Folder/Update', 'Updated folder "' . $folder->name . '"' );

            return $this->respondUpdated( 'Successfully updated folder', $folder->id, $folder->toArray() );
        }
        catch ( NotFoundException $e ) {
            // TODO: Move into FolderRepository::updateRecord()
            $this->logbookRepository->addError( 'Folder/Update', $e->getMessage() );

            return $this->respondNotFound( $e->getMessage() );
        }
        catch ( Exception $e ) {
            // TODO: Move into FolderRepository::updateRecord()
            $this->logbookRepository->addError( 'Folder/Update', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete( $id )
    {
        try {
            $folder = $this->folderRepository->deleteRecord( $id );

            // TODO: Move into FolderRepository::deleteRecord()
            $this->logbookRepository->addSuccess( 'Folder/Delete', 'Deleted folder "' . $folder->name . '"' );

            return $this->respondDeleted( 'Successfully deleted folder', $folder->id, $folder->toArray() );
        }
        catch ( NotFoundException $e ) {
            // TODO: Move into FolderRepository::deleteRecord()
            $this->logbookRepository->addError( 'Folder/Delete', $e->getMessage() );

            return $this->respondNotFound( $e->getMessage() );
        }
        catch ( Exception $e ) {
            // TODO: Move into FolderRepository::deleteRecord()
            $this->logbookRepository->addError( 'Folder/Delete', $e->getMessage() );

            return $this->respondWithError( 'Failed to delete folder' );
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function rebuild()
    {
        $job = ( new FoldersRebuildJob() );
        $this->dispatch( $job );

        $responseMessage = 'Added "Folders Rebuild" to job queue.';

        $this->logbookRepository->addSuccess( 'Folder/Rebuild', $responseMessage );

        return $this->respondJobDispatched( $responseMessage );
    }
}