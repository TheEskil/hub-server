<?php namespace App\Http\Controllers;

use App\Contracts\FolderRepository;
use App\Contracts\LogbookRepository;
use App\Contracts\TorrentRepository;
use App\Exceptions\DownloadFailedException;
use App\Exceptions\NotFoundException;
use App\Models\Download;

/**
 * Class TorrentsController
 * @package App\Http\Controllers
 */
class TorrentsController extends ApiController
{
    /**
     * @var \App\Contracts\TorrentRepository
     */
    protected $torrentRepository;

    /**
     * @var \App\Contracts\LogbookRepository
     */
    private $logbookRepository;

    /**
     * @var \App\Contracts\FolderRepository
     */
    private $folderRepository;

    /**
     * TorrentsController constructor.
     *
     * @param \App\Contracts\TorrentRepository $torrentRepository
     * @param \App\Contracts\LogbookRepository $logbookRepository
     * @param \App\Contracts\FolderRepository  $folderRepository
     */
    public function __construct( TorrentRepository $torrentRepository, LogbookRepository $logbookRepository, FolderRepository $folderRepository )
    {
        $this->torrentRepository = $torrentRepository;
        $this->logbookRepository = $logbookRepository;
        $this->folderRepository = $folderRepository;
    }

    /**
     * @param      $id
     * @param null $type
     * @param null $typeId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function download( $id, $type = null, $typeId = null )
    {
        // TODO: Move into TorrentRepository
        try {
            $torrent = $this->torrentRepository->findById( $id );

            if ( $torrent->trashed() ) {
                return $this->respondDeleted( 'Torrent has been deleted', $torrent->id, $torrent->toArray() );
            }

            $download = new Download();
            $download->torrent_id = $torrent->id;

            if ( $type !== null && $typeId !== null ) {
                switch ( $type ) {
                    case 'episode':
                        $download->episode_id = $typeId;
                        break;
                    case 'wish':
                        $download->wish_id = $typeId;
                        break;
                }
            }

            $download->save();

            if ( ! is_null( $downloaded = $this->torrentRepository->download( $torrent, $download ) ) ) {
                return $this->respondDownloaded( 'Downloaded torrent', $downloaded->id, $downloaded->toArray() );
            }

            return $this->respondWithError( 'Something went wrong with downloading the torrent' );
        }
        catch ( NotFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
        catch ( DownloadFailedException $e ) {
            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param $query
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search( $query )
    {
        try {
            $torrents = $this->torrentRepository->findLikeTitle( $query );

            return $this->respond( $torrents );
        }
        catch ( NotFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }
}