<?php namespace App\Http\Controllers;

use App\Contracts\LogbookRepository;
use App\Exceptions\NotFoundException;
use App\Http\Requests;
use App\Repositories\EloquentFolderRepository;
use Exception;
use RuntimeException;
use Transmission\Transmission;

/**
 * Class TransmissionController
 * @package App\Http\Controllers
 */
class TransmissionController extends ApiController
{
    /**
     * @var \Transmission\Transmission
     */
    private $transmission;

    /**
     * @var \App\Repositories\EloquentFolderRepository
     */
    private $folderRepository;

    /**
     * @var \App\Contracts\LogbookRepository
     */
    private $logbookRepository;

    /**
     * TransmissionController constructor.
     *
     * @param \App\Repositories\EloquentFolderRepository $folderRepository
     * @param \App\Contracts\LogbookRepository           $logbookRepository
     */
    public function __construct( EloquentFolderRepository $folderRepository, LogbookRepository $logbookRepository )
    {
        $this->transmission = new Transmission( env( 'TRANSMISSION_HOST' ), env( 'TRANSMISSION_PORT' ) );
        $this->folderRepository = $folderRepository;
        $this->logbookRepository = $logbookRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        try {
            $this->transmission->getSession();

            $transmissionTorrents = $this->transmission->all();

            $torrents = [ ];
            foreach ( $transmissionTorrents as $torrent ) {
                if ( $torrent->getEta() != -1 ) {
                    $dtF = new \DateTime( "@0" );
                    $dtT = new \DateTime( "@" . $torrent->getEta() );

                    $etaHuman = $dtF->diff( $dtT )->format( '%ad%hh%im%ss' );
                }
                else {
                    $etaHuman = '∞';
                }

                $size = [ 'B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB' ];
                $factor = floor( (strlen( $torrent->getSize() ) - 1) / 3 );
                $sizeHuman = sprintf( "%.2f", $torrent->getSize() / pow( 1024, $factor ) ) . @$size[ $factor ];

                $status = $torrent->getStatus();
                $statusHuman = 'NA';
                switch ( $status ) {
                    case 0:
                        $statusHuman = ($torrent->getPercentDone() == 100) ? 'Finished' : 'Stopped';
                        break;
                    case 1:
                        $statusHuman = 'Check (Waiting)';
                        break;
                    case 2:
                        $statusHuman = 'Checking';
                        break;
                    case 3:
                        $statusHuman = 'Download (Waiting)';
                        break;
                    case 4:
                        $statusHuman = 'Downloading';
                        break;
                    case 5:
                        $statusHuman = 'Seed (Waiting)';
                        break;
                    case 6:
                        $statusHuman = 'Seeding';
                        break;
                }

                $factor = floor( (strlen( $torrent->getUploadRate() ) - 1) / 3 );
                $uploadRateHuman = sprintf( "%.2f", $torrent->getUploadRate() / pow( 1024, $factor ) ) . @$size[ $factor ] . '/s';

                $factor = floor( (strlen( $torrent->getDownloadRate() ) - 1) / 3 );
                $downloadRateHuman = sprintf( "%.2f", $torrent->getDownloadRate() / pow( 1024, $factor ) ) . @$size[ $factor ] . '/s';

                $factor = floor( (strlen( $torrent->getSize() * ($torrent->getPercentDone() / 100) ) - 1) / 3 );
                $progressHuman = sprintf( "%.2f", $torrent->getSize() * ($torrent->getPercentDone() / 100) / pow( 1024, $factor ) ) . @$size[ $factor ];

                $files = [ ];
                foreach ( $torrent->getFiles() as $file ) {
                    $files[] = [
                        'name'      => $file->getName(),
                        'size'      => $file->getSize(),
                        'completed' => $file->getCompleted(),
                        'isDone'    => $file->isDone(),
                    ];
                }

                $trackerStats = [ ];
                foreach ( $torrent->getTrackerStats() as $trackerStat ) {
                    $trackerStats[] = [
                        'host'               => $trackerStat->getHost(),
                        'lastAnnounceResult' => $trackerStat->getLastAnnounceResult(),
                        'lastScrapeResult'   => $trackerStat->getLastScrapeResult(),
                        'seederCount'        => $trackerStat->getSeederCount(),
                        'leecherCount'       => $trackerStat->getLeecherCount(),
                    ];
                }

                $torrents[] = [
                    'id'                => $torrent->getId(),
                    'hash'              => $torrent->getHash(),
                    'eta'               => $torrent->getEta(),
                    'etaHuman'          => $etaHuman,
                    'size'              => $torrent->getSize(),
                    'sizeHuman'         => $sizeHuman,
                    'name'              => $torrent->getName(),
                    'status'            => $torrent->getStatus(),
                    'statusHuman'       => $statusHuman,
                    'isFinished'        => $torrent->isFinished(),
                    'startDate'         => $torrent->getStartDate(),
                    'uploadRate'        => $torrent->getUploadRate(),
                    'uploadRateHuman'   => $uploadRateHuman,
                    'downloadRate'      => $torrent->getDownloadRate(),
                    'downloadRateHuman' => $downloadRateHuman,
                    'peersConnected'    => $torrent->getPeersConnected(),
                    'progressHuman'     => $progressHuman,
                    'percentDone'       => $torrent->getPercentDone(),
                    'files'             => $files,
                    'trackerStats'      => $trackerStats,
                    'uploadRatio'       => $torrent->getUploadRatio(),
                    'isStopped'         => $torrent->isStopped(),
                    'isChecking'        => $torrent->isChecking(),
                    'isDownloading'     => $torrent->isDownloading(),
                    'isSeeding'         => $torrent->isSeeding(),
                    'downloadDir'       => $torrent->getDownloadDir(),
                ];
            }
        }
        catch ( RuntimeException $e ) {
            return $this->respondWithError( $e->getMessage() );
        }

        $torrents = collect( $torrents );

        $torrents = $torrents->sortByDesc( function ( $torrent ) {
            return $torrent[ 'percentDone' ];
        } );

        return $this->respond( $torrents->values()->all() );
    }

    /**
     * @param $file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add( $file )
    {
        try {
            $session = $this->transmission->getSession();

            $folder = $this->folderRepository->getActiveFolder();

            $session->setDownloadDir( $folder->name . '/Completed' );
            $session->setIncompleteDir( $folder->name . '/Downloads' );
            $session->save();

            $torrent = $this->transmission->add( $file );

            if ( ! $torrent ) {
                return $this->respondWithError( 'Something went wrong' );
            }

            $this->logbookRepository->addSuccess( 'Transmission/Add', 'Added torrent "' . $torrent->getName() . '"' );

            return $this->respondDownloaded( 'Downloaded torrent', $torrent->getHash(), $torrent->toArray() );
        }
        catch ( RuntimeException $e ) {
            $this->logbookRepository->addError( 'Transmission/Add', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
        catch ( NotFoundException $e ) {
            $this->logbookRepository->addError( 'Transmission/Add', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
        catch ( Exception $e ) {
            $this->logbookRepository->addError( 'Transmission/Add', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param $torrentId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function start( $torrentId )
    {
        try {
            $this->transmission->getSession();

            $torrent = $this->transmission->get( $torrentId );

            if ( ! $torrent ) {
                return $this->respondWithError( 'No such torrent' );
            }

            $this->transmission->start( $torrent );

            $responseMessage = 'Started torrent "' . $torrent->getName() . '"';

            $this->logbookRepository->addSuccess( 'Transmission/Start', $responseMessage );

            return $this->respond( $responseMessage );
        }
        catch ( RuntimeException $e ) {
            $this->logbookRepository->addError( 'Transmission/Start', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function startAll()
    {
        try {
            $this->transmission->getSession();

            $torrents = $this->transmission->all();

            $torrentsStarted = 0;
            foreach ( $torrents as $torrent ) {
                $this->transmission->start( $torrent );
                $torrentsStarted++;
            }

            if ( ! $torrentsStarted && ! $torrents ) {
                return $this->respondWithError( 'There are no torrents to start' );
            }

            $responseMessage = 'Started ' . $torrentsStarted . ' torrents';

            $this->logbookRepository->addSuccess( 'Transmission/Start All', $responseMessage );

            return $this->respond( $responseMessage );
        }
        catch ( RuntimeException $e ) {
            $this->logbookRepository->addError( 'Transmission/Start All', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param $torrentId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function stop( $torrentId )
    {
        try {
            $this->transmission->getSession();

            $torrent = $this->transmission->get( $torrentId );

            if ( ! $torrent ) {
                return $this->respondWithError( 'No such torrent' );
            }

            $this->transmission->stop( $torrent );

            $responseMessage = 'Stopped torrent "' . $torrent->getName() . '"';
            $this->logbookRepository->addSuccess( 'Transmission/Stop', $responseMessage );

            return $this->respond( $responseMessage );
        }
        catch ( RuntimeException $e ) {
            $this->logbookRepository->addError( 'Transmission/Stop', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function stopAll()
    {
        try {
            $this->transmission->getSession();

            $torrents = $this->transmission->all();

            $torrentsStopped = 0;
            foreach ( $torrents as $torrent ) {
                $this->transmission->stop( $torrent );
                $torrentsStopped++;
            }

            if ( ! $torrentsStopped && ! $torrents ) {
                return $this->respondWithError( 'There are no torrents to stop' );
            }

            $responseMessage = 'Stopped ' . $torrentsStopped . ' torrents';

            $this->logbookRepository->addSuccess( 'Transmission/Stop All', $responseMessage );

            return $this->respond( $responseMessage );
        }
        catch ( RuntimeException $e ) {
            $this->logbookRepository->addError( 'Transmission/Stop All', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param      $torrentId
     * @param bool $removeLocalData
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove( $torrentId, $removeLocalData = false )
    {
        try {
            $this->transmission->getSession();

            $torrent = $this->transmission->get( $torrentId );

            if ( ! $torrent ) {
                return $this->respondWithError( 'No such torrent' );
            }

            $this->transmission->remove( $torrent, (bool) $removeLocalData );

            $responseMessage = 'Removed torrent ' . $torrent->getName() . ( ! $removeLocalData) ?: ' along with local data';

            $this->logbookRepository->addSuccess( 'Transmission/Remove', $responseMessage );

            return $this->respond( $responseMessage );
        }
        catch ( RuntimeException $e ) {
            $this->logbookRepository->addError( 'Transmission/Remove', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeAllFinished()
    {
        try {
            $this->transmission->getSession();

            $torrents = $this->transmission->all();

            $torrentsRemoved = 0;
            foreach ( $torrents as $torrent ) {
                if ( $torrent->isFinished() ) {
                    $this->transmission->remove( $torrent );
                    $torrentsRemoved++;
                }
            }

            if ( ! $torrentsRemoved && ! $torrents ) {
                return $this->respondWithError( 'There are no torrents to remove' );
            }

            $responseMessage = 'Removed ' . $torrentsRemoved . ' torrents';

            $this->logbookRepository->addSuccess( 'Transmission/Remove Finished', $responseMessage );

            return $this->respond( $responseMessage );
        }
        catch ( RuntimeException $e ) {
            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBadge()
    {
        try {
            $this->transmission->getSession();

            $torrents = $this->transmission->all();

            $finishedTorrents = $unfinishedTorrents = 0;
            $queueDownloaded = $queueSize = 0;
            foreach ( $torrents as $torrent ) {
                if ( $torrent->isFinished() ) {
                    $finishedTorrents++;
                }
                else {
                    $unfinishedTorrents++;
                }

                $queueSize += $torrent->getSize();
                $queueDownloaded += ($torrent->getPercentDone() / 100) * $torrent->getSize();
            }

            $progressPercentage = ($queueSize) ? ($queueDownloaded / $queueSize) * 100 : 0;

            return $this->respond( [
                                       'finished'           => $finishedTorrents,
                                       'unfinished'         => $unfinishedTorrents,
                                       'progressPercentage' => $progressPercentage,
                                   ] );
        }
        catch ( RuntimeException $e ) {
            return $this->respondWithError( $e->getMessage() );
        }
    }
}