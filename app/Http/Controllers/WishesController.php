<?php namespace App\Http\Controllers;

use App\Contracts\LogbookRepository;
use App\Contracts\TorrentRepository;
use App\Contracts\WishRepository;
use App\Events\WishWasAdded;
use App\Exceptions\NotFoundException;
use App\Http\Requests\WishRequest;
use App\Jobs\WishesRefreshJob;
use App\ReleaseParser\ReleaseParser;
use Exception;
use Illuminate\Support\Facades\Event;

/**
 * Class WishesController
 * @package App\Http\Controllers
 */
class WishesController extends ApiController
{
    /**
     * @var \App\Contracts\WishRepository
     */
    protected $wishRepository;

    /**
     * @var \App\Contracts\LogbookRepository
     */
    private $logbookRepository;

    /**
     * @var \App\Contracts\TorrentRepository
     */
    private $torrentRepository;

    /**
     * WishesController constructor.
     *
     * @param \App\Contracts\WishRepository    $wishRepository
     * @param \App\Contracts\LogbookRepository $logbookRepository
     * @param \App\Contracts\TorrentRepository $torrentRepository
     */
    function __construct( WishRepository $wishRepository, LogbookRepository $logbookRepository, TorrentRepository $torrentRepository )
    {
        $this->wishRepository = $wishRepository;
        $this->logbookRepository = $logbookRepository;
        $this->torrentRepository = $torrentRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $wishes = $this->wishRepository->getAll();

        return $this->respond( $wishes );
    }

    /**
     * @param \App\Http\Requests\WishRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add( WishRequest $request )
    {
        try {
            $wish = $this->wishRepository->addRecord( $request->all() );
            $this->logbookRepository->addSuccess( 'Wish/Add', 'Added "' . $wish->title . ' (' . $wish->year . ')"' );

            // TODO: Clean up line 69-87
            $torrents = $this->torrentRepository->findLikeTitle( $wish->title . ' ' . $wish->year );
            foreach ( $torrents as $torrent ) {
                $this->torrentRepository->pairTorrent( $torrent );
            }

            // TODO: Check if necessary
            // Re-get the Wish object to ensure that newly paired torrents are there.
            $wish = $this->wishRepository->findById( $wish->id );

            $parser = new ReleaseParser();
            $torrents = $wish->torrents->sortByDesc( function ( $torrent ) use ( $parser ) {
                $parsed = $parser->parse( $torrent->title );

                return is_null( $parsed ) ? 0 : $parsed->calculateQualityRank();
            } );

            foreach ( $torrents as $torrent ) {
                $this->torrentRepository->downloadMatch( $torrent, $wish );
            }

            $this->wishRepository->downloadPoster( $wish );

            return $this->respondCreated( 'Successfully added wish', $wish->id, $wish->toArray() );
        }
        catch ( Exception $e ) {
            $this->logbookRepository->addError( 'Wish/Add', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete( $id )
    {
        try {
            $wish = $this->wishRepository->deleteRecord( $id );

            $this->logbookRepository->addSuccess( 'Wish/Delete', 'Deleted wish "' . $wish->title . ' (' . $wish->year . ')"' );

            return $this->respondDeleted( 'Successfully deleted wish', $wish->id, $wish->toArray() );
        }
        catch ( NotFoundException $e ) {
            $this->logbookRepository->addError( 'Wish/Delete', $e->getMessage() );

            return $this->respondNotFound( $e->getMessage() );
        }
        catch ( Exception $e ) {
            $this->logbookRepository->addError( 'Wish/Delete', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param null $wishId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh( $wishId = null )
    {
        if ( $wishId ) {
            try {
                $wish = $this->wishRepository->findById( $wishId );

                $job = ( new WishesRefreshJob( $wish ) );
                $this->dispatch( $job );

                $responseMessage = 'Added "Wish Refresh: ' . $wish->title . '" to job queue.';

                $this->logbookRepository->addSuccess( 'Wish/Refresh', $responseMessage );

                return $this->respondJobDispatched( $responseMessage );
            }
            catch ( NotFoundException $e ) {
                $this->logbookRepository->addError( 'Wish/Refresh', $e->getMessage() );

                return $this->respondNotFound( $e->getMessage() );
            }
        }
        else {
            $wishes = $this->wishRepository->getAll();

            foreach ( $wishes as $wish ) {
                $job = ( new WishesRefreshJob( $wish ) );

                $this->dispatch( $job );
            }

            $responseMessage = 'Added "Wishes Refresh" to job queue.';
            $this->logbookRepository->addSuccess( 'Wish/Refresh', $responseMessage );

            return $this->respondJobDispatched( $responseMessage );
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBadge()
    {
        try {
            $badge = $this->wishRepository->getBadge();

            return $this->respond( $badge );
        }
        catch ( NotFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }
}