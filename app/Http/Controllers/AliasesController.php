<?php namespace App\Http\Controllers;

use App\Contracts\AliasRepository;
use App\Contracts\TorrentRepository;
use App\Exceptions\NotFoundException;
use App\Http\Requests\AliasRequest;
use Exception;

/**
 * Class AliasesController
 * @package App\Http\Controllers
 */
class AliasesController extends ApiController
{
    /**
     * @var \App\Contracts\AliasRepository
     */
    protected $aliasRepository;

    /**
     * @var \App\Contracts\TorrentRepository
     */
    private $torrentRepository;

    /**
     * AliasesController constructor.
     *
     * @param \App\Contracts\AliasRepository   $aliasRepository
     * @param \App\Contracts\TorrentRepository $torrentRepository
     */
    public function __construct( AliasRepository $aliasRepository, TorrentRepository $torrentRepository )
    {
        $this->aliasRepository = $aliasRepository;
        $this->torrentRepository = $torrentRepository;
    }

    /**
     * @param \App\Http\Requests\AliasRequest $request
     *
     * @return mixed
     */
    public function add( AliasRequest $request )
    {
        try {
            $alias = $this->aliasRepository->addRecord( $request->all() );

            // TODO: Refactor into AliasRepository->addRecord()
            try {
                $torrents = $this->torrentRepository->findLikeTitle( $alias->title );
                foreach ( $torrents as $torrent ) {
                    $this->torrentRepository->pairTorrent( $torrent );
                }
            }
            catch ( NotFoundException $e ) {
            }

            return $this->respondCreated( 'Successfully added alias', $alias->id, $alias->toArray() );
        }
        catch ( Exception $e ) {
            return $this->respondWithUnprocessableEntity( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function delete( $id )
    {
        try {
            $alias = $this->aliasRepository->deleteRecord( $id );

            return $this->respondDeleted( 'Successfully deleted alias', $alias->id, $alias->toArray() );
        }
        catch ( NotFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
        catch ( Exception $e ) {
            return $this->respondWithError( $e->getMessage() );
        }
    }
}