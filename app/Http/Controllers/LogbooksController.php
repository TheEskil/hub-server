<?php namespace App\Http\Controllers;

use App\Contracts\LogbookRepository;

/**
 * Class LogbooksController
 * @package App\Http\Controllers
 */
class LogbooksController extends ApiController
{
    /**
     * @var \App\Contracts\LogbookRepository
     */
    protected $logbookRepository;

    /**
     * LogbooksController constructor.
     *
     * @param \App\Contracts\LogbookRepository $logbookRepository
     */
    public function __construct( LogbookRepository $logbookRepository )
    {
        $this->logbookRepository = $logbookRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $logbooks = $this->logbookRepository->getAll();

        return $this->respond( $logbooks );
    }
}