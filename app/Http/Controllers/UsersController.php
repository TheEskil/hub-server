<?php namespace App\Http\Controllers;

use App\Contracts\LogbookRepository;
use App\Contracts\UserRepository;
use App\Exceptions\NotFoundException;
use App\Http\Requests\UserRequest;
use Exception;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends ApiController
{
    /**
     * @var \App\Contracts\UserRepository
     */
    private $userRepository;

    /**
     * @var \App\Contracts\LogbookRepository
     */
    private $logbookRepository;

    /**
     * UsersController constructor.
     *
     * @param \App\Contracts\UserRepository    $userRepository
     * @param \App\Contracts\LogbookRepository $logbookRepository
     */
    public function __construct( UserRepository $userRepository, LogbookRepository $logbookRepository )
    {

        $this->userRepository = $userRepository;
        $this->logbookRepository = $logbookRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $users = $this->userRepository->getAll();

        return $this->respond( $users );
    }

    /**
     * @param \App\Http\Requests\UserRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add( UserRequest $request )
    {
        try {
            $user = $this->userRepository->addRecord( $request->all() );

            // TODO: Move into UserRepository::addRecord()
            $this->logbookRepository->addSuccess( 'User/Add', 'Added user "' . $user->first_name . '"' );

            return $this->respondCreated( 'Successfully added user', $user->id, $user->toArray() );
        }
        catch ( Exception $e ) {
            // TODO: Move into UserRepository::addRecord()
            $this->logbookRepository->addError( 'User/Add', $e->getMessage() );

            return $this->respondWithUnprocessableEntity( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOne( $id )
    {
        try {
            $user = $this->userRepository->findById( $id );

            return $this->respond( $user );
        }
        catch ( NotFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }

    /**
     * @param \App\Http\Requests\UserRequest $request
     * @param                                $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( UserRequest $request, $id )
    {
        try {
            $user = $this->userRepository->updateRecord( $id, $request->all() );

            // TODO: Move into UserRepository::updateRecord()
            $this->logbookRepository->addSuccess( 'User/Update', 'Updated user "' . $user->first_name . '"' );

            return $this->respondUpdated( 'Successfully updated user', $user->id, $user->toArray() );
        }
        catch ( NotFoundException $e ) {
            // TODO: Move into UserRepository::updateRecord()
            $this->logbookRepository->addError( 'User/Update', $e->getMessage() );

            return $this->respondNotFound( $e->getMessage() );
        }
        catch ( Exception $e ) {
            // TODO: Move into UserRepository::updateRecord()
            $this->logbookRepository->addError( 'User/Update', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete( $id )
    {
        try {
            $user = $this->userRepository->deleteRecord( $id );

            // TODO: Move into UserRepository::deleteRecord()
            $this->logbookRepository->addSuccess( 'User/Delete', 'Deleted user "' . $user->first_name . '"' );

            return $this->respondDeleted( 'Successfully deleted user', $user->id, $user->toArray() );
        }
        catch ( NotFoundException $e ) {
            // TODO: Move into UserRepository::deleteRecord()
            $this->logbookRepository->addError( 'User/Delete', $e->getMessage() );

            return $this->respondNotFound( $e->getMessage() );
        }
        catch ( Exception $e ) {
            // TODO: Move into UserRepository::deleteRecord()
            $this->logbookRepository->addError( 'User/Delete', $e->getMessage() );

            return $this->respondWithError( $e->getMessage() );
        }
    }
}