<?php namespace App\Http\Controllers;

use App\Models\Series;
use App\TVMaze\Exceptions\NoSeriesFoundException;
use App\TVMaze\TVMaze;

/**
 * Class TVMazeController
 * @package App\Http\Controllers
 */
class TVMazeController extends ApiController
{
    /**
     * @var \App\TVMaze\TVMaze
     */
    protected $tvMaze;

    /**
     * TVMazeController constructor.
     *
     * @param \App\TVMaze\TVMaze $tvMaze
     */
    public function __construct( TVMaze $tvMaze )
    {
        $this->tvMaze = $tvMaze;
    }

    /**
     * @param $query
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search( $query )
    {
        try {
            return $this->respond(
                [
                    'data' => $this->tvMaze->singleSearch( $query ),
                ]
            );
        }
        catch ( NoSeriesFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }

    /**
     * @param $query
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fullSearch( $query )
    {
        try {
            $results = $this->tvMaze->fullSearch( $query );

            $results->each( function ( $result ) {
                if ( ! is_null( $series = Series::where( 'tvmaze_id', $result->tvmaze_id )->first() ) ) {
                    $result->series_id = $series->id;
                }
                else {
                    $result->series_id = null;
                }
            } );

            return $this->respond( $results );
        }
        catch ( NoSeriesFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return \App\TVMaze\Models\TVMazeSeries|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function showInfo( $id )
    {
        try {
            return $this->tvMaze->showInfo( $id );
        }
        catch ( NoSeriesFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fullShowInfo( $id )
    {
        try {
            return $this->tvMaze->fullShowInfo( $id );
        }
        catch ( NoSeriesFoundException $e ) {
            return $this->respondNotFound( $e->getMessage() );
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Support\Collection|null
     */
    public function episodeList( $id )
    {
        if ( ! is_null( $episodeList = $this->tvMaze->episodeList( $id ) ) ) {
            return $episodeList;
        }

        return $this->respondNotFound( 'No episodes found for series id ' . $id );
    }

    /**
     * @param $showName
     * @param $season
     * @param $episode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function episodeInfo( $showName, $season, $episode )
    {
        if ( ! is_null( $episodeInfo = $this->tvMaze->episodeInfo( $showName, $season, $episode ) ) ) {
            return $episodeInfo;
        }

        return $this->respondNotFound( 'No episode info found for series ' . $showName . ' episode ' . $season . 'x' . $episode );
    }
}