<?php
/*
OUT:
-------------
INSERT INTO
	torrents_new (`title`, `uri`, `category`, `published_at`, `created_at`, `updated_at`, `feed_id`)
SELECT
	`Title`, `URI`, `Category`, from_unixtime(`PubDate`), from_unixtime(`Date`), from_unixtime(`Date`), 1
FROM torrents

IN:
-------------
INSERT INTO
	torrents (`title`, `uri`, `category`, `published_at`, `created_at`, `updated_at`, `feed_id`)
SELECT
	`title`, `uri`, `category`, `published_at`, `created_at`, `updated_at`, 1
FROM torrents_new
*/

Route::get( 'fixLegacySeriesFolders', function () {
    function sanitize( $string )
    {
        $string = str_replace( '.', '', $string );
        $string = str_replace( '\'', '', $string );

        return $string;
    }

    function dir_size( $directory )
    {
        $size = 0;
        foreach ( new RecursiveIteratorIterator( new RecursiveDirectoryIterator( $directory ) ) as $file ) {
            $size += $file->getSize();
        }

        return $size;
    }

    $folders = \App\Models\Folder::all();

    $flysystem = new \Illuminate\Filesystem\Filesystem();
    $folders->each( function ( $folder ) use ( $flysystem ) {
        $directories = $flysystem->directories( $folder->name . '/Media/TV' );

        foreach ( $directories as $directory ) {
            $primary = null;

            if ( ! is_null( $series = \App\Models\Series::findByTitle( basename( $directory ) ) ) ) {
                $series = json_decode( $series );
                $primary = true;
            }
            else if ( ! is_null( $series = \App\Models\Series::findByTitle( sanitize( basename( $directory ) ) ) ) ) {
                $series = json_decode( $series );
            }
            else if ( ! is_null( $series = \App\Models\Alias::findByTitle( sanitize( basename( $directory ) ) ) ) ) {
                $series = json_decode( $series->series()->first() );
            }

            if ( ! is_null( $series ) ) {
                if ( ! $primary ) {
                    $newDir = str_replace( basename( $directory ), $series->title, $directory );

                    if ( $flysystem->isDirectory( $newDir ) && dir_size( $directory ) < 10000000 ) {
                        var_dump( $directory . ' should move to ' . $newDir );

                        $files = $flysystem->allFiles( $directory );
                        foreach ( $files as $file ) {
                            $flysystem->move( $file, $newDir . '/' . basename( $file ) );

                            if ( $flysystem->isFile( $newDir . '/' . basename( $file ) ) ) {
                                var_dump( 'Moved ' . $file . ' to ' . $newDir . '/' . basename( $file ) );
                            }
                            else {
                                var_dump( 'Failed to move ' . $file . ' to ' . $newDir . '/' . basename( $file ) );
                            }
                        }

                        if ( ! dir_size( $directory ) < 10000000 ) {
                            $flysystem->deleteDirectory( $directory );

                            if ( $flysystem->isDirectory( $directory ) ) {
                                var_dump( 'Failed to delete ' . $directory );
                            }
                            else {
                                var_dump( 'Deleted ' . $directory );
                            }
                        }
                    }
                    else if ( ! dir_size( $directory ) < 10000000 ) {
                        $flysystem->deleteDirectory( $directory );

                        if ( $flysystem->isDirectory( $directory ) ) {
                            var_dump( 'Failed to delete ' . $directory );
                        }
                        else {
                            var_dump( 'Deleted ' . $directory );
                        }
                    }
                }
            }
            else {
                var_dump( 'Deleting ' . $directory );

                $flysystem->deleteDirectory( $directory );

                if ( $flysystem->isDirectory( $directory ) ) {
                    var_dump( 'Failed to delete ' . $directory );
                }
                else {
                    var_dump( 'Deleted ' . $directory );
                }
            }
        }
    } );
} );

Route::post( 'auth', 'AuthController@authenticate' );
Route::get( 'auth/user', 'AuthController@getAuthenticatedUser' );

Route::group(
    [
        'as'         => 'api::',
        'prefix'     => 'v1',
        'middleware' => [
            //'jwt.refresh',
            //'jwt.auth',
        ],
    ],
    function () {
        Route::group( [ 'as' => 'aliases::' ], function () {
            Route::post( 'aliases', 'AliasesController@add' )->name( 'add' );
            Route::delete( 'aliases/{id}', 'AliasesController@delete' )->name( 'delete' );
        } );

        Route::group( [ 'as' => 'episodes::' ], function () {
            Route::get( 'episodes/recent/{numberOfDays?}', 'EpisodesController@getRecent' )->name( 'getRecent' );
            Route::get( 'episodes/upcoming', 'EpisodesController@getUpcoming' )->name( 'getUpcoming' );
        } );

        Route::group( [ 'as' => 'feeds::' ], function () {
            Route::get( 'feeds', 'FeedsController@getAll' )->name( 'getAll' );
            Route::post( 'feeds', 'FeedsController@add' )->name( 'add' );
            Route::get( 'feeds/{id}/refresh', 'FeedsController@refresh' )->name( 'refresh' );
            Route::get( 'feeds/refresh', 'FeedsController@refresh' )->name( 'refreshAll' );
            Route::get( 'feeds/{id}', 'FeedsController@getOne' )->name( 'getOne' );
            Route::put( 'feeds/{id}', 'FeedsController@update' )->name( 'update' );
            Route::delete( 'feeds/{id}', 'FeedsController@delete' )->name( 'delete' );
        } );

        Route::group( [ 'as' => 'files::' ], function () {
            Route::get( 'files', 'FilesController@getAll' )->name( 'getAll' );
            Route::delete( 'files/{id}', 'FilesController@delete' )->name( 'delete' );
            Route::get( 'files/episodes', 'FilesController@getEpisodes' )->name( 'getEpisodes' );
            Route::get( 'files/movies', 'FilesController@getMovies' )->name( 'getMovies' );
            Route::get( 'files/wishes', 'FilesController@getWishes' )->name( 'getWishes' );
            Route::get( 'files/subtitles', 'FilesController@getSubtitles' )->name( 'getSubtitles' );
            Route::get( 'files/uncategorized', 'FilesController@getUncategorized' )->name( 'getUncategorized' );
            Route::get( 'files/duplicates', 'FilesController@getDuplicates' )->name( 'getDuplicates' );
            Route::get( 'files/downloads', 'FilesController@getDownloads' )->name( 'getDownloads' );
            Route::get( 'files/process', 'FilesController@process' )->name( 'process' );
        } );

        Route::group( [ 'as' => 'folders::' ], function () {
            Route::get( 'folders', 'FoldersController@getAll' )->name( 'getAll' );
            Route::post( 'folders', 'FoldersController@add' )->name( 'add' );
            Route::put( 'folders/{id}', 'FoldersController@update' )->name( 'update' );
            Route::delete( 'folders/{id}', 'FoldersController@delete' )->name( 'delete' );
            Route::get( 'folders/rebuild', 'FoldersController@rebuild' )->name( 'rebuild' );
        } );

        Route::group( [ 'as' => 'logbooks::' ], function () {
            Route::get( 'logbooks', 'LogbooksController@getAll' )->name( 'getAll' );
        } );

        Route::group( [ 'as' => 'series::' ], function () {
            Route::get( 'series', 'SeriesController@getAll' )->name( 'getAll' );
            Route::post( 'series', 'SeriesController@add' )->name( 'add' );
            Route::delete( 'series/{id}', 'SeriesController@delete' )->name( 'delete' );
            Route::get( 'series/{id}/refresh', 'SeriesController@refresh' )->name( 'refresh' );
            Route::get( 'series/refresh', 'SeriesController@refresh' )->name( 'refreshAll' );
            Route::get( 'series/{id}', 'SeriesController@getOne' )->name( 'getOne' );
            Route::get( 'series/{id}/episodes', 'SeriesController@getEpisodes' )->name( 'getEpisodes' );
        } );

        Route::group( [ 'as' => 'torrents::' ], function () {
            Route::get( 'torrents/{id}/download/{type?}/{typeId?}', 'TorrentsController@download' )->name( 'download' );
            Route::get( 'torrents/search/{query}', 'TorrentsController@search' )->name( 'search' );
        } );

        Route::group( [ 'as' => 'transmission::' ], function () {
            Route::get( 'transmission', 'TransmissionController@getAll' )->name( 'getAll' );
            Route::get( 'transmission/start/all', 'TransmissionController@startAll' )->name( 'startAll' );
            Route::get( 'transmission/stop/all', 'TransmissionController@stopAll' )->name( 'stopAll' );
            Route::get( 'transmission/remove/finished', 'TransmissionController@removeAllFinished' )->name( 'removeAllFinished' );
            Route::get( 'transmission/start/{torrentId}', 'TransmissionController@start' )->name( 'start' );
            Route::get( 'transmission/stop/{torrentId}', 'TransmissionController@stop' )->name( 'stop' );
            Route::get( 'transmission/remove/{torrentId}/{removeLocalData?}', 'TransmissionController@remove' )->name( 'remove' );
            Route::get( 'transmission/badge', 'TransmissionController@getBadge' )->name( 'getBadge' );
        } );

        Route::group( [ 'as' => 'tvmaze::' ], function () {
            Route::get( 'tvmaze/search/{query}', 'TVMazeController@search' )->name( 'search' );
            Route::get( 'tvmaze/fullsearch/{query}', 'TVMazeController@fullSearch' )->name( 'fullSearch' );
            Route::get( 'tvmaze/showinfo/{id}', 'TVMazeController@showInfo' )->name( 'showInfo' );
            Route::get( 'tvmaze/fullshowinfo/{id}', 'TVMazeController@fullShowInfo' )->name( 'fullShowInfo' );
            Route::get( 'tvmaze/episodelist/{id}', 'TVMazeController@episodeList' )->name( 'episodeList' );
            Route::get( 'tvmaze/episodeinfo/{showName}/{season}x{episode}', 'TVMazeController@episodeInfo' )->name( 'episodeInfo' );
        } );

        Route::group( [ 'as' => 'users::' ], function () {
            // Index, Add, Show, Update, Delete
        } );

        Route::group( [ 'as' => 'wishes::' ], function () {
            Route::get( 'wishes', 'WishesController@getAll' )->name( 'getAll' );
            Route::post( 'wishes', 'WishesController@add' )->name( 'add' );
            Route::delete( 'wishes/{id}', 'WishesController@delete' )->name( 'delete' );
            Route::get( 'wishes/{id}/refresh', 'WishesController@refresh' )->name( 'refresh' );
            Route::get( 'wishes/refresh', 'WishesController@refresh' )->name( 'refreshAll' );
            Route::get( 'wishes/badge', 'WishesController@getBadge' )->name( 'getBadge' );
        } );
    }
);