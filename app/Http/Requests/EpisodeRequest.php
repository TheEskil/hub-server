<?php

namespace App\Http\Requests;

class EpisodeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ) {
            case 'GET':
            case 'DELETE': {
                return [ ];
            }
            case 'POST': {
                return [
                    'season_number'     => 'required',
                    'episode_number'    => 'required',
                    'production_number' => '',
                    'airdate'           => '',
                    'remote_link'       => '',
                    'title'             => 'required',
                    'rating'            => '',
                    'remote_image_uri'  => '',
                    'local_image_uri'   => '',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    //
                ];
            }
        }
    }

    public function messages()
    {
        return [
            //
        ];
    }
}