<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Response;

abstract class Request extends FormRequest
{
    public function response( array $errors )
    {
        return Response::json(
            [
                'error' => [
                    'message'     => $errors,
                    'status_code' => 400,
                ],
            ],
            400
        );
    }
}