<?php

namespace App\Http\Requests;

class FeedRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ) {
            case 'GET':
            case 'DELETE': {
                return [ ];
            }
            case 'POST': {
                return [
                    'title' => 'required',
                    'uri'   => 'required|url|unique:feeds,uri',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'title' => 'required',
                    'uri'   => 'required|url|unique:feeds,uri,' . $this->get( 'id' ),
                ];
            }
        }
    }

    public function messages()
    {
        return [
            //
        ];
    }
}