<?php

namespace App\Http\Requests;

class FolderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ) {
            case 'GET':
            case 'DELETE': {
                return [ ];
            }
            case 'POST': {
                return [
                    'name' => 'required|legal|valid_path|writable|unique:folders,name',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'name' => 'required|legal|valid_path|writable|inexistent|unique:folders,name,' . $this->get( 'id' ),
                ];
            }
        }
    }

    public function messages()
    {
        return [
            'name.legal'      => 'The :attribute contains illegal characters. Only alphanumeric characters and dashes are allowed.',
            'name.valid_path' => 'The :attribute is an invalid path.',
            'name.writable'   => 'The :attribute must be a writable location.',
            'name.inexistent' => 'A folder already exists with that name.',
        ];
    }
}