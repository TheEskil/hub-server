<?php

namespace App\Http\Requests;

class FeedSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ) {
            case 'GET':
            case 'DELETE': {
                return [ ];
            }
            case 'POST': {
                return [
                    'title' => 'required',
                    'uri'   => 'required|unique:feed_searches,uri',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    //
                ];
            }
        }
    }

    public function messages()
    {
        return [
            //
        ];
    }
}