<?php

namespace App\Http\Requests;

class WishRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ) {
            case 'GET':
            case 'DELETE': {
                return [ ];
            }
            case 'POST': {
                return [
                    'themoviedb_id' => 'sometimes|required|unique:wishes,themoviedb_id',
                    'imdb_id'       => 'sometimes|required|unique:wishes,imdb_id',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'themoviedb_id' => 'sometimes|required|unique:wishes,themoviedb_id',
                    'imdb_id'       => 'sometimes|required|unique:wishes,imdb_id',
                ];
            }
        }
    }

    public function messages()
    {
        return [
            //
        ];
    }
}