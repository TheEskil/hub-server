<?php

namespace App\Http\Requests;

class LogbookRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ) {
            case 'GET':
            case 'DELETE': {
                return [ ];
            }
            case 'POST': {
                return [
                    'event'   => 'required',
                    'type'    => 'required|in:Success,Failure,Error',
                    'message' => 'required',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'event'   => 'required',
                    'type'    => 'required|in:Success,Failure,Error',
                    'message' => 'required',
                ];
            }
        }
    }

    public function messages()
    {
        return [
            'type.in' => ':attribute must be one of the following values: Success, Failure or Error',
        ];
    }
}