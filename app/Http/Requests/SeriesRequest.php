<?php

namespace App\Http\Requests;

class SeriesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ) {
            case 'GET':
            case 'DELETE': {
                return [ ];
            }
            case 'POST': {
                return [
                    'tvmaze_id' => 'required|unique:series,tvmaze_id',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'tvmaze_id' => 'required|unique:series,tvmaze_id',
                ];
            }
        }
    }

    public function messages()
    {
        return [
            //
        ];
    }
}