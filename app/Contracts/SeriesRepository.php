<?php namespace App\Contracts;

use App\Models\Series;

/**
 * Interface SeriesRepository
 * @package App\Contracts
 */
interface SeriesRepository
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $id
     *
     * @return mixed
     */
    public function findById( $id );

    /**
     * @param $tvMazeId
     *
     * @return mixed
     */
    public function addRecord( $tvMazeId );

    /**
     * @param $id
     * @param $tvMazeId
     *
     * @return mixed
     */
    public function updateRecord( $id, $tvMazeId );

    /**
     * @param $id
     *
     * @return mixed
     */
    public function deleteRecord( $id );

    /**
     * @param \App\Models\Series $series
     *
     * @return mixed
     */
    public function downloadPoster( Series $series );
}