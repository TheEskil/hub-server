<?php namespace App\Contracts;

/**
 * Interface FeedRepository
 * @package App\Contracts
 */
interface FeedRepository
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $id
     *
     * @return mixed
     */
    public function findById( $id );

    /**
     * @param $input
     *
     * @return mixed
     */
    public function addRecord( $input );

    /**
     * @param $id
     * @param $input
     *
     * @return mixed
     */
    public function updateRecord( $id, $input );

    /**
     * @param $id
     *
     * @return mixed
     */
    public function deleteRecord( $id );
}