<?php namespace App\Contracts;

/**
 * Interface LogbookRepository
 * @package App\Contracts
 */
interface LogbookRepository
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @return mixed
     */
    public function getLastEntry();

    /**
     * @param $id
     *
     * @return mixed
     */
    public function findById( $id );

    /**
     * @param $event
     * @param $type
     * @param $message
     *
     * @return mixed
     */
    public function addRecord( $event, $type, $message );

    /**
     * @param $event
     * @param $message
     *
     * @return mixed
     */
    public function addSuccess( $event, $message );

    /**
     * @param $event
     * @param $message
     *
     * @return mixed
     */
    public function addFailure( $event, $message );

    /**
     * @param $event
     * @param $message
     *
     * @return mixed
     */
    public function addError( $event, $message );

    /**
     * @param $id
     *
     * @return mixed
     */
    public function deleteRecord( $id );
}