<?php namespace App\Contracts;

/**
 * Interface AliasRepository
 * @package App\Contracts
 */
interface AliasRepository
{
    /**
     * @param $input
     *
     * @return mixed
     */
    public function addRecord( $input );

    /**
     * @param $id
     *
     * @return mixed
     */
    public function deleteRecord( $id );
}