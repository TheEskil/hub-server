<?php namespace App\Contracts;

/**
 * Interface FileRepository
 * @package App\Contracts
 */
interface FileRepository
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @return mixed
     */
    public function getEpisodes();

    /**
     * @return mixed
     */
    public function getMovies();

    /**
     * @return mixed
     */
    public function getWishes();

    /**
     * @return mixed
     */
    public function getSubtitles();

    /**
     * @return mixed
     */
    public function getUncategorized();

    /**
     * @return mixed
     */
    public function getDuplicates();

    /**
     * @return mixed
     */
    public function getDownloads();

    /**
     * @param $id
     *
     * @return mixed
     */
    public function deleteRecord( $id );

    /**
     * @param $file
     * @param $path
     *
     * @return mixed
     */
    public function matchFile( $file, $path );
}