<?php namespace App\Contracts;

use App\Models\Wish;

/**
 * Interface WishRepository
 * @package App\Contracts
 */
interface WishRepository
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $id
     *
     * @return mixed
     */
    public function findById( $id );

    /**
     * @param $input
     *
     * @return mixed
     */
    public function addRecord( $input );

    /**
     * @param \App\Models\Wish $wish
     *
     * @return mixed
     */
    public function updateRecord( Wish $wish );

    /**
     * @param $id
     *
     * @return mixed
     */
    public function deleteRecord( $id );

    /**
     * @return mixed
     */
    public function getBadge();

    /**
     * @param \App\Models\Wish $wish
     *
     * @return mixed
     */
    public function downloadPoster( Wish $wish );
}