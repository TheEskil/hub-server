<?php namespace App\Contracts;

use App\Models\Download;
use App\Models\Torrent;

/**
 * Interface TorrentRepository
 * @package App\Contracts
 */
interface TorrentRepository
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $title
     *
     * @return mixed
     */
    public function findByTitle( $title );

    /**
     * @param $title
     *
     * @return mixed
     */
    public function findLikeTitle( $title );

    /**
     * @param $id
     *
     * @return mixed
     */
    public function findById( $id );

    /**
     * @param $id
     *
     * @return mixed
     */
    public function findByFeedId( $id );

    /**
     * @param $input
     *
     * @return mixed
     */
    public function addRecord( $input );

    /**
     * @param \App\Models\Torrent $torrent
     *
     * @return mixed
     */
    public function pairTorrent( Torrent $torrent );

    /**
     * @param \App\Models\Torrent  $torrent
     * @param \App\Models\Download $download
     *
     * @return mixed
     */
    public function download( Torrent $torrent, Download $download );

    /**
     * @param \App\Models\Torrent $torrent
     * @param                     $match
     *
     * @return mixed
     */
    public function downloadMatch( Torrent $torrent, $match );
}