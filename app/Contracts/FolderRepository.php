<?php namespace App\Contracts;

/**
 * Interface FolderRepository
 * @package App\Contracts
 */
interface FolderRepository
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $input
     *
     * @return mixed
     */
    public function addRecord( $input );

    /**
     * @param $id
     * @param $input
     *
     * @return mixed
     */
    public function updateRecord( $id, $input );

    /**
     * @param $id
     *
     * @return mixed
     */
    public function deleteRecord( $id );

    /**
     * @param $folder
     *
     * @return mixed
     */
    public function getFolderSpace( $folder );

    /**
     * @return mixed
     */
    public function getActiveFolder();

    /**
     * @param $folderSubPath
     *
     * @return mixed
     */
    public function createSubfolders( $folderSubPath );
}