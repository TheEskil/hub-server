<?php namespace App\Contracts;

/**
 * Interface EpisodeRepository
 * @package App\Contracts
 */
interface EpisodeRepository
{
    /**
     * @param int $numberOfDays
     *
     * @return mixed
     */
    public function getRecent( $numberOfDays = 5 );

    /**
     * @return mixed
     */
    public function getUpcoming();
}