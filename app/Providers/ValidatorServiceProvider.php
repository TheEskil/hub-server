<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend(
            'legal',
            function ( $attribute, $value, $parameters, $validator ) {
                if ( preg_match( '/[^A-z0-9\-]/', basename( $value ) ) ) { // Contains illegal characters
                    return false;
                }

                return true;
            }
        );

        Validator::extend(
            'valid_path',
            function ( $attribute, $value, $parameters, $validator ) {
                if ( $value[ 0 ] != '/' ) { // Does not start with the root file system
                    return false;
                }

                if ( substr_count( str_replace( basename( $value ), '', $value ), '/' ) == 1 ) { // Is a top level folder
                    return false;
                }

                return true;
            }
        );

        Validator::extend(
            'writable',
            function ( $attribute, $value, $parameters, $validator ) {
                if ( ! is_writable( str_replace( basename( $value ), '', $value ) ) ) { // Not writable location
                    return false;
                }

                return true;
            }
        );

        Validator::extend(
            'inexistent',
            function ( $attribute, $value, $parameters, $validator ) {
                if ( is_writable( str_replace( basename( $value ), '', $value ) ) ) { // Writable location
                    if ( is_dir( $value ) ) { // Folder already exists
                        return false;
                    }
                }

                return true;
            }
        );
    }

    /**
     * Register any application services.
     *
     * This service provider is a great spot to register your various container
     * bindings with the application. As you can see, we are registering our
     * "Registrar" implementation here. You can add your own bindings too!
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
