<?php namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::failing( function ( JobFailed $event ) {
            if ( array_key_exists( 'data', $event->data ) ) {
                if ( array_key_exists( 'class', $event->data[ 'data' ] ) ) {
                    echo Carbon::now() . ' [FAILED] Job: "' . $event->data[ 'data' ][ 'class' ] . '"' . "\n";
                }
                else if ( array_key_exists( 'command', $event->data[ 'data' ] ) ) {
                    echo Carbon::now() . ' [FAILED] Command: "' . $event->data[ 'data' ][ 'command' ] . '"' . "\n";
                }
            }
        } );
    }

    /**
     * Register any application services.
     *
     * This service provider is a great spot to register your various container
     * bindings with the application. As you can see, we are registering our
     * "Registrar" implementation here. You can add your own bindings too!
     *
     * @return void
     */
    public function register()
    {
        if ( $this->app->environment() == 'local' ) {
            $this->app->register( 'Laracasts\Generators\GeneratorsServiceProvider' );
        }

        $this->app->bind(
            'Illuminate\Contracts\Auth\Registrar',
            'App\Services\Registrar'
        );

        $this->app->bind(
            'App\Contracts\EpisodeRepository',
            'App\Repositories\EloquentEpisodeRepository'
        );

        $this->app->bind(
            'App\Contracts\FileRepository',
            'App\Repositories\EloquentFileRepository'
        );

        $this->app->bind(
            'App\Contracts\FeedRepository',
            'App\Repositories\EloquentFeedRepository'
        );

        $this->app->bind(
            'App\Contracts\FolderRepository',
            'App\Repositories\EloquentFolderRepository'
        );

        $this->app->bind(
            'App\Contracts\GenreRepository',
            'App\Repositories\EloquentGenreRepository'
        );

        $this->app->bind(
            'App\Contracts\LogbookRepository',
            'App\Repositories\EloquentLogbookRepository'
        );

        $this->app->bind(
            'App\Contracts\SeriesRepository',
            'App\Repositories\EloquentSeriesRepository'
        );

        $this->app->bind(
            'App\Contracts\AliasRepository',
            'App\Repositories\EloquentAliasRepository'
        );

        $this->app->bind(
            'App\Contracts\TorrentRepository',
            'App\Repositories\EloquentTorrentRepository'
        );

        $this->app->bind(
            'App\Contracts\UserRepository',
            'App\Repositories\EloquentUserRepository'
        );

        $this->app->bind(
            'App\Contracts\WishRepository',
            'App\Repositories\EloquentWishRepository'
        );
    }
}
