<?php

namespace App\Transmission;

class Transmission
{
    private $transmission;

    public function __construct()
    {
        $this->transmission = new \Transmission\Transmission( env( 'TRANSMISSION_HOST' ), env( 'TRANSMISSION_PORT' ) );
    }

    public function getAll()
    {
        $torrents = [ ];
        try {
            $torrentObjs = $this->transmission->all();

            foreach ( $torrentObjs as $torrentObj ) {
                $torrents[] = [
                    'id'   => $torrentObj->getId(),
                    'name' => $torrentObj->getName(),
                    'hash' => $torrentObj->getHash(),
                ];
            }

            return $torrents;
        }
        catch ( \RuntimeException $e ) {
            throw new \RuntimeException( $e->getMessage() );
        }
    }

    public function getSession() {
        return $this->transmission->getSession();
    }
}