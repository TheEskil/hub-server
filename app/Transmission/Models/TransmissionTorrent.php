<?php

namespace App\Transmission\Models;

class TransmissionTorrent
{
    public $name;

    public function __construct( $genre )
    {
        $this->name = ((string) $genre) ?: null;
    }
}