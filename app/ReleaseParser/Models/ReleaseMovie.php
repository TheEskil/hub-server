<?php

namespace App\ReleaseParser\Models;

class ReleaseMovie extends Release
{
    public $year;

    public function __construct( $title, $year, $quality, $group )
    {
        parent::__construct( 'Movie', $title, $quality, $group );

        $this->year = $year;
    }
}