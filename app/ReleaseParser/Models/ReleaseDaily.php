<?php

namespace App\ReleaseParser\Models;

class ReleaseDaily extends Release
{
    public $year;

    public $month;

    public $day;

    /**
     * ReleaseDaily constructor.
     *
     * @param $month
     * @param $day
     */
    public function __construct( $title, $year, $month, $day, $quality, $group )
    {
        parent::__construct( 'Daily', $title, $quality, $group );
        $this->year = trim( $year );
        $this->month = trim( $month );
        $this->day = trim( $day );
    }
}