<?php

namespace App\ReleaseParser\Models;

class ReleaseQuality
{
    public $label;

    public function __construct( $label )
    {
        $this->label = $label;
    }
}