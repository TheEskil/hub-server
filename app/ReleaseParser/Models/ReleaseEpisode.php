<?php

namespace App\ReleaseParser\Models;

class ReleaseEpisode
{
    public $season_number;

    public $episode_number;

    public function __construct( $season_number, $episode_number )
    {
        $this->season_number = $season_number;
        $this->episode_number = $episode_number;
    }
}