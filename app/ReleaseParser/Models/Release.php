<?php

namespace App\ReleaseParser\Models;

abstract class Release
{
    public $type;

    public $title;

    public $quality;

    public $group;

    public $qualityRank = 0;

    public function __construct( $type, $title, $quality, $group )
    {
        $this->type = $type;
        $this->title = trim( str_replace( '.', ' ', $title ) );
        $this->quality = $quality;
        $this->group = (strlen( $group )) ? trim( str_replace( '.', ' ', $group ) ) : null;
        $this->qualityRank = $this->calculateQualityRank();
    }

    public function calculateQualityRank()
    {
        $this->qualityRank = 0;

        if ( ! is_null( $this->quality ) ) {
            $this->quality->each( function ( $quality ) {
                switch ( strtolower( $quality->label ) ) {
                    case '1080p':
                        $this->qualityRank += 60000;
                        break;
                    case '1080i':
                        $this->qualityRank += 50000;
                        break;
                    case '810p':
                        $this->qualityRank += 40000;
                        break;
                    case '720p':
                        $this->qualityRank += 30000;
                        break;
                    case '540p':
                        $this->qualityRank += 20000;
                        break;
                    case '480p':
                        $this->qualityRank += 10000;
                        break;
                    case 'bluray':
                    case 'brrip':
                    case 'bdrip':
                        $this->qualityRank += 6000;
                        break;
                    case 'dvdrip':
                    case 'hdtv':
                    case 'pdtv':
                    case 'hdtvrip':
                        $this->qualityRank += 5000;
                        break;
                    case 'dvdscr':
                        $this->qualityRank += 3000;
                        break;
                    case 'spanish':
                    case 'korsub':
                    case 'hebsub':
                    case 'ts':
                    case 'telesync':
                    case 'hdts':
                    case 'hd-ts':
                    case 'hd-tc':
                    case 'hdcam':
                    case 'cam':
                    case 'truefrench':
                    case 'telecine':
                    case 'hd-cam':
                    case 'german':
                    case 'french':
                    case 'tc':
                    case 'hdtc':
                    case 'hdts':
                        $this->qualityRank -= 100000;
                        break;
                    case 'hc':
                        $this->qualityRank -= 1000;
                        break;
                    case 'proper':
                    case 'repack':
                        $this->qualityRank += 100;
                        break;
                    case 'truehd7':
                    case 'truehd5':
                    case 'truehd':
                        $this->qualityRank += 30;
                        break;
                    case 'dts':
                    case 'dd5':
                    case 'web-dl':
                        $this->qualityRank += 20;
                        break;
                    case 'ac3':
                        $this->qualityRank += 10;
                        break;
                    case 'x264':
                        $this->qualityRank += 20;
                        break;
                    case 'xvid':
                        $this->qualityRank += 10;
                        break;
                }
            } );
        }

        return $this->qualityRank;
    }

    public function getReleaseString()
    {
        switch ( $this->type ) {
            case 'Series':
                $release = $this->title . ' ';
                $release .= $this->buildEpisodesString() . ' ';
                $release .= $this->buildQualityString();
                $release .= $this->buildGroupString();

                return str_replace( ' ', '.', $release );
                break;
            case 'Daily':
                $release = $this->title . ' ';
                $release .= $this->year . ' ';
                $release .= $this->month . ' ';
                $release .= $this->day . ' ';

                $release .= $this->buildQualityString();
                $release .= $this->buildGroupString();

                return str_replace( ' ', '.', $release );
                break;
            case 'Movie':
                $release = $this->title . ' ';
                $release .= $this->year . ' ';
                $release .= $this->buildQualityString();
                $release .= $this->buildGroupString();

                return str_replace( ' ', '.', $release );
                break;
            default:
                return null;
        }
    }

    public function buildEpisodesString()
    {
        $release = 'S' . sprintf( '%02.d', $this->episodes->min()->season_number );
        $release .= 'E' . sprintf( '%02.d', $this->episodes->min()->episode_number );

        if ( sizeof( $this->episodes ) > 1 ) {
            $release .= '-' . sprintf( '%02.d', $this->episodes->max()->episode_number );
        }

        return $release;
    }

    public function buildQualityString()
    {
        $release = '';

        foreach ( $this->quality as $quality ) {
            $release .= $quality->label . ' ';
        }

        return trim( $release );
    }

    public function buildGroupString()
    {
        if ( is_null( $this->group ) ) {
            return;
        }

        $release = '-' . $this->group;

        return trim( $release );
    }
}
