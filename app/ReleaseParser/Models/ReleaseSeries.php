<?php

namespace App\ReleaseParser\Models;

use Illuminate\Support\Collection;

class ReleaseSeries extends Release
{
    public $episodes;

    public function __construct( $title, Collection $episodes, $quality, $group )
    {
        parent::__construct( 'Series', $title, $quality, $group );

        $this->episodes = $episodes;
    }
}