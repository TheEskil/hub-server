<?php

namespace App\ReleaseParser\Exceptions;

use Exception;

class UnableToParseException extends Exception
{
}