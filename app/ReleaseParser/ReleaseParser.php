<?php

namespace App\ReleaseParser;

use App\ReleaseParser\Exceptions\UnableToParseException;
use App\ReleaseParser\Models\ReleaseDaily;
use App\ReleaseParser\Models\ReleaseEpisode;
use App\ReleaseParser\Models\ReleaseMovie;
use App\ReleaseParser\Models\ReleaseQuality;
use App\ReleaseParser\Models\ReleaseSeries;
use Illuminate\Support\Collection;

class ReleaseParser
{
    private $seriesRegularExpression = '/(?<title>.*?)\.?(?<episodes>(?:(?:s[0-9]{1,2})?[.-]?e[0-9]{1,2}|[0-9]{1,2}x[0-9]{1,2})(?:-?(?:s?[0-9]{1,2})?[xe]?[0-9]{1,2})*)[\.| ](?<quality>[^\-]+(?:\-[^\-]+(?=[^\-]*-))*)(?:-(?<group>[^-]+)?)?$/i';

    private $alternateSeriesRegularExpression = '/(?<title>.*?)\.(?=[0-9]{3}[\.])(?<season>[0-9])(?<episode>[0-9]{2})[\.| ](?<quality>[^\-]+(?:\-[^\-]+(?=[^\-]*-))*)(?:-(?<group>[^-]+)?)?$/i';

    private $episodeRegularExpression = '/\G[.-]?(?:s?(?<season>(?!72)[0-9]{1,2}+))?[.-]?[xe]?(?<episode>(?!720)[0-9]{1,3})/i';

    private $dailyRegularExpression = '/(?<title>[A-z0-9 \&._\-:\\pL]+)(?<year>(?!1080p)[0-9]{4})[.\s](?<month>[0-9]{2})[.\s](?<day>[0-9]{2})[.\s](?<quality>[^\-]+(?:\-[^\-]+(?=[^\-]*-))*)(?:-(?<group>[^-]+)?)?$/i';

    private $movieRegularExpression = '/(?<title>[A-z0-9 \&._\-:\\pL]+)\(?(?<year>(?!1080p)[0-9]{4})\)?(?<quality>[^\-]+(?:\-[^\-]+(?=[^\-]*-))*)(?:-(?<group>[^-]+)?)?$/i';

    public function sanitize( $release )
    {
        $searchForChars = [ ' ', '_', '(', ')', '.-.', '[', ']', '{', '}', '\'' ];
        $replaceWithChars = [ '.', '.', '', '', '.', '', '', '', '', '' ];

        $fileTypes = explode( ',', env( 'FILE_TYPES' ) );
        foreach ( $fileTypes as $fileType ) {
            array_push( $searchForChars, '.' . $fileType );
            array_push( $replaceWithChars, '' );
        }

        return trim( str_replace( $searchForChars, $replaceWithChars, $release ) );
    }

    public function parseMovie( $movieMatch )
    {
        $releaseTitle = (array_key_exists( 'title', $movieMatch )) ? $this->sanitize( $movieMatch[ 'title' ] ) : null;
        $releaseYear = (array_key_exists( 'year', $movieMatch )) ? $this->sanitize( $movieMatch[ 'year' ] ) : null;

        if ( array_key_exists( 'quality', $movieMatch ) ) {
            $releaseQuality = new Collection( $this->parseQuality( $this->sanitize( $movieMatch[ 'quality' ] ) ) );
        }
        else {
            $releaseQuality = null;
        }
        if ( array_key_exists( 'group', $movieMatch ) ) {
            $releaseGroup = $this->sanitize( $movieMatch[ 'group' ] );
        }
        else {
            $releaseGroup = null;
        }

        return new ReleaseMovie( $releaseTitle, $releaseYear, $releaseQuality, $releaseGroup );
    }

    public function parseEpisodes( $seriesMatch )
    {
        $releaseTitle = (array_key_exists( 'title', $seriesMatch )) ? $this->sanitize( $seriesMatch[ 'title' ] ) : null;
        $releaseEpisodeStr = (array_key_exists( 'episodes', $seriesMatch )) ? $this->sanitize( $seriesMatch[ 'episodes' ] ) : null;
        $releaseQuality = (array_key_exists( 'quality', $seriesMatch )) ? new Collection( $this->parseQuality( $this->sanitize( $seriesMatch[ 'quality' ] ) ) ) : null;
        $releaseGroup = (array_key_exists( 'group', $seriesMatch )) ? $this->sanitize( $seriesMatch[ 'group' ] ) : null;
        $releaseEpisodes = [ ];

        if ( array_key_exists( 'season', $seriesMatch ) && array_key_exists( 'episode', $seriesMatch ) ) {
            $releaseEpisodes[] = new ReleaseEpisode( (int) $seriesMatch[ 'season' ], (int) $seriesMatch[ 'episode' ] );
        }
        else {
            preg_match_all( $this->episodeRegularExpression, $releaseEpisodeStr, $episodeMatches, PREG_SET_ORDER );

            $releaseSeason = 'NA';
            foreach ( $episodeMatches as $episodeMatch ) {
                if ( array_key_exists( 'season', $episodeMatch ) && strlen( $episodeMatch[ 'season' ] > 0 ) ) {
                    $releaseSeason = (int) $episodeMatch[ 'season' ];
                }

                $episodesArr[] = (int) $episodeMatch[ 'episode' ];
            }

            if ( sizeof( $episodesArr ) > 1 ) {
                for ( $i = min( $episodesArr ); $i <= max( $episodesArr ); $i++ ) {
                    $releaseEpisodes[] = new ReleaseEpisode( $releaseSeason, $i );
                }
            }
            else {
                $releaseEpisodes[] = new ReleaseEpisode( $releaseSeason, $episodesArr[ 0 ] );
            }
        }

        if ( ! is_null( $releaseTitle ) ) {
            return new ReleaseSeries( $releaseTitle, new Collection( $releaseEpisodes ), $releaseQuality, $releaseGroup );
        }
        else {
            throw new UnableToParseException( 'Failed to parse: ' . json_encode( $seriesMatch ) );
        }
    }

    public function parse( $release )
    {
        $release = $this->sanitize( $release );

        if ( preg_match( $this->seriesRegularExpression, $release, $seriesMatch ) || preg_match( $this->alternateSeriesRegularExpression, $release, $seriesMatch ) ) {
            try {
                return $this->parseEpisodes( $seriesMatch );
            }
            catch ( UnableToParseException $e ) {
                return null;
            }
        }
        else if ( preg_match( $this->dailyRegularExpression, $release, $dailyMatch ) ) {
            return $this->parseDaily( $dailyMatch );
        }
        else if ( preg_match( $this->movieRegularExpression, $release, $movieMatch ) ) {
            return $this->parseMovie( $movieMatch );
        }
        else {
            return null;
        }
    }

    public function parseDaily( $dailyMatch )
    {
        $releaseTitle = (array_key_exists( 'title', $dailyMatch )) ? $this->sanitize( $dailyMatch[ 'title' ] ) : null;
        $releaseYear = (array_key_exists( 'year', $dailyMatch )) ? $this->sanitize( $dailyMatch[ 'year' ] ) : null;
        $releaseMonth = (array_key_exists( 'month', $dailyMatch )) ? $this->sanitize( $dailyMatch[ 'month' ] ) : null;
        $releaseDay = (array_key_exists( 'day', $dailyMatch )) ? $this->sanitize( $dailyMatch[ 'day' ] ) : null;
        $releaseQuality = (array_key_exists( 'quality', $dailyMatch )) ? new Collection( $this->parseQuality( $this->sanitize( $dailyMatch[ 'quality' ] ) ) ) : null;
        $releaseGroup = (array_key_exists( 'group', $dailyMatch )) ? $this->sanitize( $dailyMatch[ 'group' ] ) : null;

        return new ReleaseDaily( $releaseTitle, $releaseYear, $releaseMonth, $releaseDay, $releaseQuality, $releaseGroup );
    }

    private function parseQuality( $releaseQuality )
    {
        $qualities = explode( '.', $releaseQuality );

        $qualityArr = [ ];
        foreach ( $qualities as $quality ) {
            if ( strlen( $quality ) ) {
                $qualityArr[] = new ReleaseQuality( $quality );
            }
        }

        return $qualityArr;
    }
}