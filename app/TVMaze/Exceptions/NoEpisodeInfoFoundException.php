<?php

namespace App\TVMaze\Exceptions;

class NoEpisodeInfoFoundException extends \Exception
{
}