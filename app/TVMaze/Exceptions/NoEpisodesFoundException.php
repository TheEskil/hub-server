<?php

namespace App\TVMaze\Exceptions;

class NoEpisodesFoundException extends \Exception
{
}