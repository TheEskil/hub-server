<?php

namespace App\TVMaze\Exceptions;

class NoResultsFoundException extends \Exception
{
}