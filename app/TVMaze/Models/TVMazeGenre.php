<?php

namespace App\TVMaze\Models;

class TVMazeGenre
{
    public $name;

    public function __construct( $genre )
    {
        $this->name = ((string) $genre) ?: null;
    }
}