<?php

namespace App\TVMaze\Models;

use Illuminate\Support\Collection;

class TVMazeSeries
{
    public $title;

    public $tvmaze_id;

    public $remote_link;

    public $remote_image_uri;

    public $local_image_uri;

    public $started_at;

    public $status;

    public $type;

    public $runtime;

    public $summary;

    public $rating;

    public $votes;

    public $network;

    public $airtime;

    public $airday;

    public $timezone;

    public $genres;

    public $episodes;

    public function __construct( $ApiResponse )
    {
        if ( $ApiResponse->genres ) {
            foreach ( $ApiResponse->genres as $genre ) {
                $genres[] = new TVMazeGenre( $genre );
            }

            $this->genres = new Collection( $genres );
        }

        $started_at_date = \DateTime::createFromFormat( '!Y-m-d', (string) $ApiResponse->premiered );
        $started_at_date = (is_object( $started_at_date )) ? $started_at_date->format( 'Y-m-d' ) : null;

        $this->title = ($this->normalize( (string) $ApiResponse->name )) ?: null;
        $this->tvmaze_id = ((int) $ApiResponse->id) ?: null;
        $this->remote_link = ((string) $ApiResponse->url) ?: null;
        if ( is_object( $ApiResponse->image ) && property_exists( $ApiResponse, 'image' ) ) {
            if ( property_exists( $ApiResponse->image, 'medium' ) ) {
                $this->remote_image_uri = ((string) $ApiResponse->image->medium) ?: null;
            }
            else if ( property_exists( $ApiResponse->image, 'original' ) ) {
                $this->remote_image_uri = ((string) $ApiResponse->image->original) ?: null;
            }
        }
        $this->started_at = ((string) $started_at_date) ?: null;
        $this->status = ((string) $ApiResponse->status) ?: null;
        $this->type = ((string) $ApiResponse->type) ?: null;
        $this->runtime = ((int) $ApiResponse->runtime) ?: null;
        $this->summary = ((string) strip_tags( $ApiResponse->summary )) ?: null;
        $this->rating = ((float) $ApiResponse->rating->average) ?: null;
        $this->votes = ((int) $ApiResponse->weight) ?: null;
        if ( is_object( $ApiResponse->network ) ) {
            $this->network = ((string) $ApiResponse->network->name) ?: null;
            $this->timezone = ((string) $ApiResponse->network->country->timezone) ?: null;
        }
        $this->airtime = ((string) $ApiResponse->schedule->time) ?: null;
        if ( is_array( $ApiResponse->schedule->days ) && sizeof( $ApiResponse->schedule->days ) ) {
            $this->airday = ((string) $ApiResponse->schedule->days[ 0 ]) ?: null;
        }
    }

    public function normalize( $string )
    {
        $string = str_replace( '.', '', $string );
        $string = str_replace( '\'', '', $string );
        $string = str_replace( '&', 'and', $string );
        $string = str_replace( ':', '', $string );

        return $string;
    }
}