<?php

namespace App\TVMaze\Models;

use Carbon\Carbon;

class TVMazeEpisode
{
    public $tvmaze_id;

    public $season_number;

    public $episode_number;

    public $airdate;

    public $remote_link;

    public $title;

    public $remote_image_uri;

    public $local_image_uri;

    public $summary;

    public function __construct( $episode, $series )
    {
        if ( $episode->airdate ) {
            if ( ! is_null( $series->airtime ) ) {
                $airdate = Carbon::createFromFormat( '!Y-m-d H:i', (string) $episode->airdate . ' ' . $series->airtime, $series->timezone );
            }
            else {
                $airdate = Carbon::createFromFormat( '!Y-m-d', (string) $episode->airdate, $series->timezone );
            }

            $airdate->tz( env( 'TIMEZONE' ) );
        }
        else {
            $airdate = null;
        }

        $this->tvmaze_id = ((int) $episode->id) ?: null;
        $this->season_number = ((int) $episode->season) ?: null;
        $this->episode_number = ((int) $episode->number) ?: null;
        $this->airdate = ((string) $airdate) ?: null;
        $this->remote_link = ((string) $episode->url) ?: null;
        $this->title = ((string) $episode->name) ?: null;
        if ( is_object( $episode->image ) ) {
            if ( property_exists( $episode->image, 'original' ) ) {
                $this->remote_image_uri = ((string) $episode->image->original) ?: null;
            }
        }
        $this->summary = ((string) $episode->summary) ?: null;
    }
}