<?php

namespace App\TVMaze;

use App\TVMaze\Exceptions\NoResultsFoundException;
use App\TVMaze\Exceptions\NoSeriesFoundException;
use App\TVMaze\Models\TVMazeEpisode;
use App\TVMaze\Models\TVMazeSeries;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Collection;

class TVMaze
{
    private $client;

    public function __construct()
    {
        $this->client = new Client( [ 'base_uri' => 'http://api.tvmaze.com/' ] );
    }

    public function request( $url )
    {
        $response = $this->client->get( $url );

        if ( $response->getBody() ) {
            $jsonResponse = json_decode( (string) $response->getBody() );

            return $jsonResponse;
        }

        throw new NoResultsFoundException;
    }

    public function fullSearch( $showName )
    {
        $response = $this->request( 'search/shows?q=' . $showName );

        //dd( $response );
        if ( is_array( $response ) && sizeof( $response ) ) {
            foreach ( $response as $show ) {
                $series[] = new TVMazeSeries( $show->show );
            }

            return new Collection( $series );
        }

        throw new NoSeriesFoundException;
    }

    public function singleSearch( $showName )
    {
        $response = $this->request( 'singlesearch/shows?q=' . $showName );

        dd( $response );
        if ( is_object( $response ) && property_exists( $response, 'show' ) ) {
            foreach ( $response->show as $show ) {
                $series[] = new TVMazeSeries( $show );
            }

            return new Collection( $series );
        }

        throw new NoSeriesFoundException;
    }

    public function showInfo( $showId )
    {
        try {
            $response = $this->request( 'shows/' . $showId );

            if ( is_object( $response ) && property_exists( $response, 'id' ) ) {
                $series = new TVMazeSeries( $response );
                $series->episodes = $this->episodeList( $series );

                return $series;
            }
        }
        catch ( ClientException $e ) {
            if ( $e->getMessage() == "Client error: 404" ) {
                throw new NoSeriesFoundException( 'Series ID "' . $showId . '" does not exist on TVMaze.com' );
            }

            throw new Exception( $e->getMessage() );
        }
    }

    public function episodeList( $series )
    {
        $episodes = $this->request( 'shows/' . $series->tvmaze_id . '/episodes' );
        if ( is_array( $episodes ) && sizeof( $episodes ) ) {
            foreach ( $episodes as $episode ) {
                $episodeObjects[] = new TVMazeEpisode( $episode, $series );
            }

            return new Collection( $episodeObjects );
        }

        return null;
    }
}