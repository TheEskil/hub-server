<?php

namespace App\TheMovieDB;

use App\TheMovieDB\Exceptions\NoResultsFoundException;
use App\TheMovieDB\Models\TMDBConfiguration;
use App\TheMovieDB\Models\TMDBMovie;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Cache;

class TheMovieDB
{
    public $configuration;

    private $client;

    public function __construct()
    {
        $this->client = new Client( [ 'base_uri' => 'http://api.themoviedb.org/3/' ] );

        $this->configuration = Cache::remember( 'tmdb_configuration', Carbon::now()->addHours( 24 ), function () {
            return $this->cacheConfiguration();
        } );
    }

    public function request( $url, $params = [ ] )
    {
        $params = (sizeof( $params )) ? '&' . http_build_query( $params ) : '';
        $response = $this->client->get( $url . '?api_key=' . env( 'TMDB_KEY' ) . $params );

        if ( $response->getBody() ) {
            $jsonResponse = json_decode( (string) $response->getBody() );

            return $jsonResponse;
        }

        throw new NoResultsFoundException;
    }

    public function getMovie( $movieId )
    {
        try {
            $response = $this->request( 'movie/' . $movieId );

            if ( is_object( $response ) && property_exists( $response, 'id' ) ) {
                return new TMDBMovie( $response );
            }
            else if ( is_object( $response ) && property_exists( $response, 'status_message' ) ) {
                throw new NoResultsFoundException( $response->status_message );
            }
        }
        catch ( ClientException $e ) {
            if ( $e->getMessage() == "Client error: 404" ) {
                throw new NoResultsFoundException( 'Movie ID "' . $movieId . '" does not exist on TheMovieDB.org' );
            }

            // TODO: Account for {"status_code":25,"status_message":"Your request count (88) is over the allowed limit of 40."}
            throw new Exception( $e->getMessage() );
        }
    }

    public function cacheConfiguration()
    {
        $configuration = $this->request( 'configuration' );
        if ( is_object( $configuration ) ) {
            return new TMDBConfiguration( $configuration );
        }

        throw new NoResultsFoundException;
    }
}