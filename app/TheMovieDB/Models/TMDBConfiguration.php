<?php

namespace App\TheMovieDB\Models;

class TMDBConfiguration
{
    public $images = [ ];

    public $change_keys = [ ];

    public function __construct( $configuration )
    {
        $this->images = $configuration->images;
        $this->change_keys = $configuration->change_keys;
    }
}