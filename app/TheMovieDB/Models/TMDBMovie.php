<?php

namespace App\TheMovieDB\Models;

use Carbon\Carbon;

class TMDBMovie
{
    public $adult;

    public $backdrop_path;

    public $belongs_to_collection;

    public $budget;

    public $genres;

    public $homepage;

    public $themoviedb_id;

    public $imdb_id;

    public $original_language;

    public $original_title;

    public $summary;

    public $popularity;

    public $poster_path;

    public $production_companies;

    public $production_countries;

    public $year;

    public $release;

    public $revenue;

    public $runtime;

    public $spoken_languages;

    public $status;

    public $tagline;

    public $title;

    public $video;

    public $vote_average;

    public $vote_count;

    public $country;

    public $trailers;

    /**
     * TMDBMovie constructor.
     */
    public function __construct( $movie )
    {
        $this->adult = (property_exists( $movie, 'adult' )) ? $movie->adult : null;
        $this->backdrop_path = (property_exists( $movie, 'backdrop_path' )) ? $movie->backdrop_path : null;
        $this->belongs_to_collection = (property_exists( $movie, 'belongs_to_collection' )) ? $movie->belongs_to_collection : null;
        $this->budget = (property_exists( $movie, 'budget' )) ? $movie->budget : null;
        $this->genres = (property_exists( $movie, 'genres' )) ? $movie->genres : null;
        $this->homepage = (property_exists( $movie, 'homepage' )) ? $movie->homepage : null;
        $this->themoviedb_id = (property_exists( $movie, 'id' )) ? $movie->id : null;
        $this->imdb_id = (property_exists( $movie, 'imdb_id' )) ? $movie->imdb_id : null;
        $this->original_language = (property_exists( $movie, 'original_language' )) ? $movie->original_language : null;
        $this->original_title = (strtolower( $movie->original_title ) != strtolower( $movie->title )) ? $this->normalize( $movie->original_title ) : null;
        $this->summary = (property_exists( $movie, 'overview' )) ? $movie->overview : null;
        $this->popularity = (property_exists( $movie, 'popularity' )) ? $movie->popularity : null;
        $this->remote_image_uri = (property_exists( $movie, 'poster_path' )) ? $movie->poster_path : null;
        $this->production_companies = (property_exists( $movie, 'production_companies' )) ? $movie->production_companies : null;
        $this->production_countries = (property_exists( $movie, 'production_countries' )) ? $movie->production_countries : null;
        $this->country = (sizeof( $movie->production_countries )) ? $movie->production_countries[ 0 ]->name : null;
        $this->year = (property_exists( $movie, 'release_date' )) ? Carbon::createFromFormat( 'Y-m-d', $movie->release_date )->format( 'Y' ) : null;
        $this->release = (property_exists( $movie, 'release_date' )) ? Carbon::createFromFormat( 'Y-m-d', $movie->release_date ) : null;
        $this->revenue = (property_exists( $movie, 'revenue' )) ? $movie->revenue : null;
        $this->runtime = (property_exists( $movie, 'runtime' )) ? $movie->runtime : null;
        $this->spoken_languages = (property_exists( $movie, 'spoken_languages' )) ? $movie->spoken_languages : null;
        $this->status = (property_exists( $movie, 'status' )) ? $movie->status : null;
        $this->tagline = (property_exists( $movie, 'tagline' )) ? $movie->tagline : null;
        $this->title = (property_exists( $movie, 'title' )) ? $this->normalize( $movie->title ) : null;
        $this->video = (property_exists( $movie, 'video' )) ? $movie->video : null;
        $this->vote_average = (property_exists( $movie, 'vote_average' )) ? $movie->vote_average : null;
        $this->vote_count = (property_exists( $movie, 'vote_count' )) ? $movie->vote_count : null;

        if ( property_exists( $movie, 'trailers' ) ) {
            $this->trailers = $movie->trailers;
        }
    }

    public function normalize( $string )
    {
        $string = str_replace( '.', '', $string );
        $string = str_replace( '\'', '', $string );
        $string = str_replace( '&', 'and', $string );
        $string = str_replace( ':', '', $string );
        $string = str_replace( ',', '', $string );
        $string = str_replace( '!', '', $string );
        $string = str_replace( '?', '', $string );
        $string = str_replace( ' – ', ' ', $string );
        $string = str_replace( ' - ', ' ', $string );

        return $string;
    }
}