<?php

namespace App\TheMovieDB\Exceptions;

class NoResultsFoundException extends \Exception
{
}