<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\FeedsRefresh::class,
        \App\Console\Commands\FilesRebuild::class,
        \App\Console\Commands\PairTorrents::class,
        \App\Console\Commands\CompletedFiles::class,
        \App\Console\Commands\SeriesRefresh::class,
        \App\Console\Commands\WishesRefresh::class,
        \App\Console\Commands\RemoveFinishedTorrents::class,
        \App\Console\Commands\RedownloadFailedTorrents::class,
        \App\Console\Commands\UserAdd::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule( Schedule $schedule )
    {
        if ( env( 'APP_ENV' ) == 'production' ) {
            $schedule->command( 'feeds:refresh' )->everyThirtyMinutes()->name( 'FeedsRefresh' )->withoutOverlapping();
            $schedule->command( 'files:completed' )->everyThirtyMinutes()->name( 'CompletedFiles' )->withoutOverlapping();
            $schedule->command( 'torrents:finished' )->everyThirtyMinutes()->name( 'RemoveFinishedTorrents' )->withoutOverlapping();
            $schedule->command( 'torrents:failed' )->everyThirtyMinutes()->name( 'RedownloadFailedTorrents' )->withoutOverlapping();

            $schedule->command( 'series:refresh' )->dailyAt( '01:05' )->name( 'SeriesRefresh' )->withoutOverlapping();
            $schedule->command( 'files:rebuild' )->dailyAt( '01:10' )->name( 'FilesRebuild' )->withoutOverlapping();
            $schedule->command( 'wishes:refresh' )->dailyAt( '01:15' )->name( 'WishesRefresh' )->withoutOverlapping();
        }
    }
}
