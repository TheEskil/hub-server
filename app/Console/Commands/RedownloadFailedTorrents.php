<?php

namespace App\Console\Commands;

use App\Jobs\RedownloadFailedTorrentsJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class RedownloadFailedTorrents extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'torrents:failed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tries to add torrents which failed to download in the first place';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = ( new RedownloadFailedTorrentsJob() );
        $this->dispatch( $job );
    }
}
