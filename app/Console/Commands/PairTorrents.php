<?php

namespace App\Console\Commands;

use App\Contracts\TorrentRepository;
use App\Models\Torrent;
use App\ReleaseParser\ReleaseParser;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PairTorrents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'torrents:pair';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets up the relationships between torrents, episodes and wishes';

    /**
     * @var \App\Repositories\EloquentTorrentRepository
     */
    private $torrentRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( TorrentRepository $torrentRepository )
    {
        parent::__construct();
        $this->torrentRepository = $torrentRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dateStart = Carbon::now();
        $this->info( 'Started pairing: ' . $dateStart );
        $torrentsBar = $this->output->createProgressBar();

        DB::table( 'episode_torrent' )->truncate();
        DB::table( 'torrent_wish' )->truncate();

        $parser = new ReleaseParser();

        Torrent::chunk( 1000, function ( $torrents ) use ( $torrentsBar, $parser ) {
            foreach ( $torrents as $torrent ) {
                $this->torrentRepository->pairTorrent( $torrent );

                $torrentsBar->advance();
            }
        } );

        $torrentsBar->finish();

        $this->line( '' );

        $dateFinished = Carbon::now();
        $this->info( 'Finished pairing: ' . $dateFinished );

        $this->line( '' );
        $this->info( 'Paired ' . $torrentsBar->getProgress() . ' torrents in: ' . $dateFinished->diffForHumans( $dateStart, true ) );
    }
}