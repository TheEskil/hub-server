<?php

namespace App\Console\Commands;

use App\Jobs\CompletedFilesJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class CompletedFiles extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'files:completed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processes all completed files in every folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = ( new CompletedFilesJob() );
        $this->dispatch( $job );
    }
}
