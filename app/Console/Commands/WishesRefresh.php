<?php

namespace App\Console\Commands;

use App\Contracts\WishRepository;
use App\Jobs\WishesRefreshJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class WishesRefresh extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wishes:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refreshes all wishes against TheMovieDB.org';

    /**
     * @var \App\Contracts\WishRepository
     */
    private $wishRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( WishRepository $wishRepository )
    {
        parent::__construct();
        $this->wishRepository = $wishRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $wishes = $this->wishRepository->getAll();

        foreach ( $wishes as $wish ) {
            $job = ( new WishesRefreshJob( $wish ) );
            $this->dispatch( $job );
        }
    }
}
