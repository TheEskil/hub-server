<?php

namespace App\Console\Commands;

use App\Contracts\SeriesRepository;
use App\Jobs\SeriesRefreshJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class SeriesRefresh extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'series:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var \App\Contracts\SeriesRepository
     */
    private $seriesRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( SeriesRepository $seriesRepository )
    {
        parent::__construct();
        $this->seriesRepository = $seriesRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $series = $this->seriesRepository->getAll();

        foreach ( $series as $serie ) {
            $job = ( new SeriesRefreshJob( $serie->id, $serie->tvmaze_id ) );
            $this->dispatch( $job );
        }
    }
}
