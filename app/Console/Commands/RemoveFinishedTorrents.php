<?php

namespace App\Console\Commands;

use App\Jobs\RemoveFinishedTorrentsJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class RemoveFinishedTorrents extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'torrents:finished';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes all finished torrents from Transmission';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = ( new RemoveFinishedTorrentsJob() );
        $this->dispatch( $job );
    }
}
