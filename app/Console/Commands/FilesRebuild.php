<?php

namespace App\Console\Commands;

use App\Jobs\FilesRebuildJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class FilesRebuild extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'files:rebuild';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Matches files from all folders with episodes and wishes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = ( new FilesRebuildJob() );
        $this->dispatch( $job );
    }
}
