<?php

namespace App\Console\Commands;

use App\Contracts\UserRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class UserAdd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add user';

    /**
     * @var \App\Contracts\UserRepository
     */
    private $userRepository;

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer $drip
     *
     * @return void
     */
    public function __construct( UserRepository $userRepository )
    {
        parent::__construct();

        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $first_name = $this->ask( 'What is your first name?' );
        $last_name = $this->ask( 'What is your last name?' );
        $email = $this->ask( 'What is your e-mail?' );
        $password1 = $this->secret( 'What is your password?' );
        $password2 = $this->secret( 'What is your password? [again]' );

        $tries = 0;
        while ( $password1 != $password2 && $tries++ < 3 ) {
            $this->error( 'Your passwords does not match.' );

            $password1 = $this->secret( 'What is your password?' );
            $password2 = $this->secret( 'What is your password? [again]' );
        }

        if ( $password1 != $password2 ) {
            $this->error( 'Maximum number of attempts reached. Aborting...' );
        }
        else {
            $user = $this->userRepository->addRecord( [
                                                          'first_name' => $first_name,
                                                          'last_name'  => $last_name,
                                                          'email'      => $email,
                                                          'password'   => Hash::make( $password1 ),
                                                      ] );

            if ( $user ) {
                $this->info( 'Created user' );
            }
            else {
                $this->error( 'Failed to create user' );
            }
        }
    }
}