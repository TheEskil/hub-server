<?php

namespace App\Console\Commands;

use App\Jobs\FeedsRefreshJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class FeedsRefresh extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feeds:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refreshes all feeds and dowloads new information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = ( new FeedsRefreshJob() );
        $this->dispatch( $job );
    }
}
