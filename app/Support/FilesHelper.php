<?php

namespace App\Support;

use App\Contracts\FolderRepository;
use App\Contracts\LogbookRepository;
use App\Models\Alias;
use App\Models\Series;
use App\ReleaseParser\ReleaseParser;
use Carbon\Carbon;
use ErrorException;
use FilesystemIterator;
use Illuminate\Filesystem\Filesystem as Flysystem;
use Illuminate\Support\Facades\Log;
use RarArchive;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use UnexpectedValueException;

class FilesHelper
{
    /**
     * @var \App\Contracts\FolderRepository
     */
    public $folderRepository;

    /**
     * @var \App\ReleaseParser\ReleaseParser
     */
    private $parser;

    /**
     * @var \App\Contracts\LogbookRepository
     */
    private $logbookRepository;

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    private $flysystem;

    /**
     * @var \Carbon\Carbon
     */
    private $carbon;

    public function __construct( FolderRepository $folderRepository, ReleaseParser $parser, LogbookRepository $logbookRepository, Flysystem $flysystem, Carbon $carbon )
    {
        $this->folderRepository = $folderRepository;
        $this->parser = $parser;
        $this->logbookRepository = $logbookRepository;
        $this->flysystem = $flysystem;
        $this->carbon = $carbon;
    }

    public function getCompletedFiles( $searchFolder = 'Completed' )
    {
        $folders = $this->folderRepository->getAll();
        $files = [ ];

        foreach ( $folders as $folder ) {
            $completedFolder = $folder->name . '/' . $searchFolder;

            $recursiveIterator = [];
            try {
                $directoryIterator = new RecursiveDirectoryIterator( $completedFolder, FilesystemIterator::SKIP_DOTS );
                $recursiveIterator = new RecursiveIteratorIterator( $directoryIterator, RecursiveIteratorIterator::SELF_FIRST );
            }
            catch ( UnexpectedValueException $e ) {
                if ( strstr( $e->getMessage(), 'Host is down' ) ) {
                    continue;
                }
                else {
                    Log::error( $e->getMessage() );
                }
            }

            foreach ( $recursiveIterator as $file ) {
                if ( $file->isFile() && ! preg_match( '/' . str_replace( ',', '|', env( 'FILE_IGNORE' ) . '/i' ), $file->getFileName() ) ) {
                    $newLocation = null;

                    if ( $file->getExtension() == 'rar' ) {
                        $fileAction = 'extract';

                        try {
                            $rarFile = RarArchive::open( $file->getPathname() );

                            if ( $rarFile !== false ) {
                                try {
                                    foreach ( $rarFile->getEntries() AS $rarFileEntry ) {
                                        $newLocation = $this->getBestLocation( $folder, $searchFolder, new SplFileInfo( $rarFileEntry->getName() ) );
                                    }
                                }
                                catch ( ErrorException $e ) {
                                    continue; // Probably missing one or more RAR files in the archive
                                }

                                $rarFile->close();
                            }
                        }
                        catch ( ErrorException $e ) {
                            $this->logbookRepository->addError( 'File/Process', $e->getMessage() . ' when processing "' . $file->getPathname() . '"' );
                            echo $this->carbon->now() . ' [ERROR] ProcessFileJob: ' . $e->getMessage() . ' when processing "' . $file->getPathname() . '"' . "\n";

                            if ( strstr( $e->getMessage(), 'ERAR_BAD_ARCHIVE' ) ) {
                                $parentDirectoryPath = dirname( $file->getPathname() );
                                $parentDirectoryName = basename( $parentDirectoryPath );

                                if ( $parentDirectoryName != 'Completed' ) {
                                    $this->flysystem->deleteDirectory( $parentDirectoryPath );

                                    if ( $this->flysystem->isDirectory( $parentDirectoryPath ) ) {
                                        $this->logbookRepository->addFailure( 'File/Process', 'Failed to delete "' . $parentDirectoryPath . '"' );
                                    }
                                    else {
                                        $this->logbookRepository->addSuccess( 'File/Process', 'Deleted "' . $parentDirectoryPath . '"' );
                                    }
                                }
                                else {
                                    $this->flysystem->delete( $file->getPathname() );

                                    if ( $this->flysystem->isFile( $file->getPathname() ) ) {
                                        $this->logbookRepository->addFailure( 'File/Process', 'Failed to delete "' . $file->getPathname() . '"' );
                                    }
                                    else {
                                        $this->logbookRepository->addSuccess( 'File/Process', 'Deleted "' . $file->getPathname() . '"' );
                                    }
                                }
                            }
                        }
                    }
                    else if ( in_array( $file->getExtension(), explode( ',', env( 'FILE_TYPES' ) ) ) ) {
                        if ( $file->getSize() >= (1024 * 1024 * 150) ) {
                            $fileAction = 'move';
                            $newLocation = $this->getBestLocation( $folder, $searchFolder, $file );
                        }
                    }
                    else {
                        continue; // Not a RAR file or any of the valid file types
                    }

                    if ( $newLocation ) {
                        $files[] = [
                            'action' => $fileAction,
                            'from'   => $file->getPathname(),
                            'to'     => $newLocation,
                            'folder' => $folder->name,
                            'file'   => $file->getFilename(),
                        ];
                    }
                }
            }
        }

        return collect( $files )->sortByDesc( 'action' )->values()->toArray();
    }

    public function getBestLocation( $folder, $searchFolder, SplFileInfo $file )
    {
        $fileExtension = '.' . $file->getExtension();

        // Try to parse filename first
        $parsed = $this->parser->parse( basename( str_replace( $fileExtension, '', $file->getPathname() ) ) );

        if ( ! $parsed && basename( dirname( $file->getPathname() ) ) != $searchFolder ) {
            // Otherwise try the parent folder
            $parsed = $this->parser->parse( basename( dirname( str_replace( $fileExtension, '', $file->getPathname() ) ) ) );
        }

        if ( ! $parsed ) {
            // If there's still no match, traverse the folder hierarchy looking for a match
            $matchFound = false;
            $path = $file->getPathname();
            while ( ! $matchFound && is_dir( $path ) ) {
                $parsed = $this->parser->parse( basename( str_replace( $fileExtension, '', $path ) ) );

                if ( basename( $file ) == $searchFolder || $parsed ) {
                    $matchFound = true;
                }

                $path = str_replace( '/' . basename( $path ), '', $path );
            }
        }

        if ( $parsed ) {
            switch ( $parsed->type ) {
                case 'Daily':
                case 'Series':
                    if ( ! is_null( $series = Series::findByTitle( $parsed->title ) ) ) {
                        $series = json_decode( $series );
                    }
                    else if ( ! is_null( $series = Alias::findByTitle( $parsed->title ) ) ) {
                        $series = json_decode( $series->series()->first() );
                        $parsed->title = $series->title;
                    }

                    if ( $series && is_dir( $folder->name . '/Media/TV/' . $series->title ) ) {
                        return $folder->name . '/Media/TV/' . $series->title . '/' . $parsed->getReleaseString() . $fileExtension;
                    }
                    else if ( is_dir( $folder->name . '/Media/TV/' . $parsed->title ) ) {
                        return $folder->name . '/Media/TV/' . $parsed->title . '/' . $parsed->getReleaseString() . $fileExtension;
                    }
                    else {
                        return $folder->name . '/Unsorted/' . $parsed->getReleaseString() . $fileExtension;
                    }
                    break;
                case 'Movie':
                    return $folder->name . '/Media/Movies/' . $parsed->getReleaseString() . $fileExtension;
                    break;
                default:
                    return $folder->name . '/Unsorted/' . $parsed->getReleaseString() . $fileExtension;
            }
        }

        return $folder->name . '/Unsorted/' . $file->getFilename();
    }
}