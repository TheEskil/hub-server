<?php

function setActive( $path, $active = 'active' )
{
    if ( Request::is( $path ) || Request::is( str_replace( '/*', '', $path ) ) ) {
        return $active;
    }
}

function formatFileSize( $size )
{
    $units = [ ' B', ' KB', ' MB', ' GB', ' TB' ];
    for ( $i = 0; $size > 1024; $i++ ) {
        $size /= 1024;
    }

    return round( $size, 2 ) . $units[ $i ];
}

function glob_recursive( $pattern, $flags = 0 )
{
    $files = glob( $pattern, $flags );

    foreach ( glob( dirname( $pattern ) . '/*', GLOB_ONLYDIR | GLOB_NOSORT ) as $dir ) {
        $files = array_merge( $files, glob_recursive( $dir . '/' . basename( $pattern ), $flags ) );
    }

    return $files;
}

function d( $arr )
{
    echo '<pre>';
    print_r( $arr );
    echo '</pre>';
    die;
}